"""

This set of functions and classes are used to plot results  from Pettine, W. W., Louie, K.,
Murray, J. D, Wang, X. J., "Hierarchical Network Model Excitatory-Inhibitory Tone Shapes Alternative Strategies for
Different Degrees of Uncertainty in Multi-Attribute Decisions." PLoS Computational Biology, 2021

If you have questions, email warren.pettine@gmail.com.

"""

from dynamicalModel import *
from fixedPointAnalysis import *
import numpy as np
import scipy as sc
from scipy.signal import find_peaks
from scipy.optimize import curve_fit
from colorsys import hls_to_rgb
import matplotlib.pyplot as plt
import matplotlib.backends.backend_pdf
from mpl_toolkits.axes_grid1 import make_axes_locatable
from skimage import feature
from itertools import product, combinations, compress
import brian2  # Used for unit-checking

brian2.prefs.codegen.target = 'numpy'  # Only need this option for debugging in pycharm


def calcDprime(mu1,mu2,var):
    """
    Calculates the d'prime of distributions
    :param mu1: Mean of distribution 1
    :param mu2: Mean of distribution 2
    :param var: Variance
    :return: dprime
    """
    dprime = (mu1 - mu2) / (var + 0.0000001)
    return dprime


def calcAB(bundles):
    """
    Calculates bundles A and B from bundles matrix
    :param bundles: Matrix of bundles
    :return: A, B
    """
    A = np.sum(bundles[:, [0, 2]], axis=1)
    B = np.sum(bundles[:, [1, 3]], axis=1)
    return A, B


def findModes(array, height=10):
    """Identify peaks in a distribution"""
    array = array[np.isnan(array) < 1]
    if len(array) < height:
        return np.nan
    hst = np.histogram(array)
    hst2 = np.pad(hst[0], (1, 1), mode='constant', constant_values=0)
    peaks, _ = find_peaks(hst2, height=height)
    return hst[1][peaks - 1]


def sigmoid(x, x0, k):
    '''Basic sigmoid function with two parameters'''
    y = 1 / (1 + np.exp(-k * (x - x0)))
    return y


def sigmoidFourParam(x, x0, k, a, c):
    '''Basic sigmoid function with two parameters'''
    y = a / (1 + np.exp(-k * (x - x0))) + c
    return y


def eiModelFitComparison(X, y):
    """
    Performs the model comparison between divisive and linear, using F statistic. Also calculates Rsquared
    :param X: Tuple with vectors for excitatory and inhibitory weights
    :param y:
    :return: p, f_ratio, r_sq_linear, r_sq_div
    """
    # Clean the data
    if any(np.isnan(y)):
        X[0], X[1] = X[0][np.isnan(y) < 1], X[1][np.isnan(y) < 1]
        y = y[np.isnan(y) < 1]
    # Fit the linear and divisive models
    popt, pcov = sc.optimize.curve_fit(linearFit, X, y)
    y_ft_linear = linearFit(X, popt[0], popt[1], popt[2])
    popt, pcov = sc.optimize.curve_fit(divisiveFit, X, y)
    y_ft_div = divisiveFit(X, popt[0], popt[1])
    # Calculate r_squared
    r_sq_linear = calcRsquared(y, y_ft_linear)
    r_sq_div = calcRsquared(y, y_ft_div)
    # Do the F-test
    ssq0 = ((y_ft_linear - y) ** 2).sum()
    ssq1 = ((y_ft_div - y) ** 2).sum()
    df = len(y) - 3
    f_ratio = (ssq1 - ssq0) / (ssq0 / df)
    p = 1 - sc.stats.f.cdf(f_ratio, 1, df)
    return p, f_ratio, r_sq_linear, r_sq_div


def linearFit(X, a, b, c):
    x1, x2 = X
    y = a * x1 + b * x2 + c
    return y


def ellipse(x, a):
    y_out = (1 - x ** a) ** (1 / a)
    return y_out


def divisiveFit(X, a, b):
    x1, x2 = X
    y = a * (x1 / (x2 - .1)) + b
    return y


def fitFixedPointEllipse(x, y):
    # Prepare data
    x_hat, y_hat = (x - x.min()).astype(float), (y - y.min()).astype(float)
    x_hat, y_hat = x_hat / x_hat.max(), y_hat / y_hat.max()
    popt, pcov = curve_fit(ellipse, x_hat, y_hat)
    return popt


def calcRsquared(y, y_fit):
    ss_tot = np.dot(y - np.mean(y), y - np.mean(y));
    ss_res = np.dot(y - y_fit, y - y_fit)
    r_squared = 1 - (ss_res / ss_tot)
    return r_squared



def getFixedPointCurvature(params,traj_fp,ref=20,lims=np.array([0, 40]),output='FP',nonlinear_sv=False,sv_exp=.5):
    """
    Gets the curvature of all weight combinations in the data set produced by PyDStool. Used for plotting the convexity.
    :param params: Matrix of inputs and weights used during simulation
    :param traj_fp: The trajectory of the fixed points
    :param ref: float. Reference value for convexity calculations (default=20)
    :param lims: 2x1 vector. Limites for inputs (default=np.array([0, 40]))
    :param output: 'FR', 'FP'. Determines y_label variable.
    :param nonlinear_sv: True/False. Whether to make subjective values nonlinear prior to input to network (default=False)
    :param sv_exp: flots. If nonlinear_sv=True, this exponent determines the nonlinearity
    :return: grid_vals. Matrix that contains convexity values for each E/I tone.
    """
    """Gets the curvature of all weight combinations in the data set. Used for plotting the convexity"""
    if nonlinear_sv:
        params[:,:2], ref = convertInputSV(params[:,:2], exponent=sv_exp,ref=ref)
        print(f'Using nonlinear subjective values, ref={ref}')
    excit_range, inhib_range = np.unique(params[:, 2]), np.unique(params[:, 3])
    grid_vals = np.zeros((len(excit_range), len(inhib_range)))
    for i in range(len(inhib_range)):
        for e in range(len(excit_range)):
            x, y = getFixedPointIndifferenceXY(params,traj_fp,E=excit_range[e],I=inhib_range[i],\
                                     ref=ref,lims=lims,output=output)
            grid_vals[e, i] = fitFixedPointEllipse(x, y)
    grid_vals = np.flipud(grid_vals)
    return grid_vals


def getFixedPointIndifferenceXY(params,traj_fp,E = 0.33,I = 0.0,ref=20,lims=np.array([0, 40]),output='FP'):
    """
    Obtains indifference curve using the fixed point values generated using PyDStool.
    :param params: Matrix of inputs and weights used during simulation
    :param traj_fp: The trajectory of the fixed points
    :param E: float. Excitatory weight (default=0.33)
    :param I: float. Inhibitory weight (default=0.0)
    :param ref: float. Reference value for convexity calculations (default=20)
    :param lims: 2x1 vector. Limites for inputs (default=np.array([0, 40]))
    :param output: 'FR', 'FP'. Determines y_label variable.
    :return: x, y. Coordinates of the indifference curve.
    """
    X_A, X_B, inpt_X, Y_A, Y_B, inpt_Y, _ = \
        cullFixedPointData(params=np.round(params, 4), traj_fp=traj_fp, E=E, I=I, ref=ref, lims=lims, output=output)
    indx_X, indx_Y = np.arange(0, sum(inpt_X >= lims[0])), np.arange(0, sum(inpt_Y >= lims[0]))
    grid_vals = np.zeros((len(np.unique(indx_X)), len(np.unique(indx_Y))))
    for i in range(len(indx_X)):
        for j in range(len(indx_Y)):
            grid_vals[indx_Y[j], indx_X[i]] = \
                (X_A[i] + Y_A[j]) - (X_B[i] + Y_B[j])
    # grid_vals = np.flipud(grid_vals)
    grid_vals[grid_vals < -0.001] = -1
    grid_vals[grid_vals > 0.001] = 1
    edges = feature.canny(grid_vals)
    value = edges.flatten()
    y, x = np.indices(edges.shape).reshape(-1, len(value))
    y, x = y[value > 0], x[value > 0]
    return x, y


def cullFixedPointData(params,traj_fp,E = 0.33,I = 0.0,ref=20,lims=np.array([0, 40]),output='FP'):
    """
    Gets the data associated with a specific weight configuration and reference value
    :param params: Matrix of inputs and weights used during simulation
    :param traj_fp: The trajectory of the fixed points
    :param E: float. Excitatory weight (default=0.33)
    :param I: float. Inhibitory weight (default=0.0)
    :param ref: float. Reference value for indexing (default=20)
    :param lims: 2x1 vector. Limites for inputs (default=np.array([0, 40]))
    :param output: 'FR', 'FP'. Determines y_label variable.
    :return: X_A, X_B, inpt_X, Y_A, Y_B, inpt_Y, y_label
    """
    # Collect and clean the data
    I_w = (abs(params[:, 2] - E) < 0.001) * (abs(params[:, 3] - I) < 0.001) * (params[:, 0] >= lims[0]) * \
          (params[:, 1] >= lims[0]) * (params[:, 0] <= lims[1]) * (params[:, 1] <= lims[1])
    params_w, traj_fp_s1_w, traj_fp_s2_w = params[I_w, :], traj_fp['s1'][I_w], traj_fp['s2'][I_w]
    # Clean the data
    I_x, I_y = (abs(params_w[:, 1] - ref) < 0.001), abs(params_w[:, 0] - ref) < 0.001
    X_A, X_B, input_X = traj_fp_s1_w[I_x], traj_fp_s2_w[I_x], params_w[I_x, 0:2]
    Y_B, Y_A, input_Y = traj_fp_s1_w[I_y], traj_fp_s2_w[I_y], params_w[I_y, 0:2]
    if output == 'FR':
        S = np.concatenate((X_A.reshape(len(X_A), 1), X_B.reshape(len(X_A), 1)), axis=1)
        r = getFR(S, input_X, w=np.array([[E, -1 * I], [-1 * I, E]]))
        X_A, X_B = r[0, :], r[1, :]
        S = np.concatenate((Y_A.reshape(len(X_A), 1), Y_B.reshape(len(X_A), 1)), axis=1)
        r = getFR(S, input_Y, w=np.array([[E, -1 * I], [-1 * I, E]]))
        Y_A, Y_B = r[0, :], r[1, :]
        y_label = 'Firing Rate'
    elif output == 'FP':
        y_label = 'Fixed Point Value'
    else:
        raise ValueError("output must be 'FR' or 'FP")
    X_A, X_B, inpt_X = sortInput(X_A, X_B, input_X[:, 0] - min(input_X[:, 0]))
    Y_A, Y_B, inpt_Y = sortInput(Y_A, Y_B, input_Y[:, 1] - min(input_Y[:, 1]))
    return X_A, X_B, inpt_X, Y_A, Y_B, inpt_Y, y_label


def getFR(S, mu, w=np.array([[0.3725, -0.1137], [-0.1137, 0.3725]]), I_0=0.3297, d=0.154,
          g_ext=0.0011, a=270.0, b=108.0):
    """

    :param S: Matrix. Synaptic drive
    :param mu: Vector. Input firing rates
    :param w: Matrix. Weights of populations (default=np.array([[0.3725, -0.1137], [-0.1137, 0.3725]]))
    :param I_0: Float. Background current. (default=0.3297)
    :param d: unitless parameter for F-I curve (default=0.154)
    :param g_ext: Float. Coupling parameter converting FR to I. (default=0.0011)
    :param a: Hz/nAmp, parameter for F-I curve (default=270.0)
    :param b: Hz, parameter for F-I curve (default=108.0)
    :return: r. Firing Rate
    """
    try:
        wS = np.dot(w, S)
    except:
        wS = np.dot(w, S.transpose())
    try:
        I = (wS + g_ext * mu + I_0)
    except:
        I = (wS + g_ext * mu.transpose() + I_0)
    r = (a * I - b) / (1 - np.exp(-d * (a * I - b)))
    return r


def sortInput(A, B, inpt):
    """
    Sorts bundles by input value
    :param A: Vector. Bundle A
    :param B: Vector. Bundle B
    :param inpt: Vector. Used to sort
    :return: A, B, inpt. All sorted
    """
    I = np.argsort(inpt)
    return A[I].copy(), B[I].copy(), inpt[I].copy()


def colors(n):
    '''Create distinct colors for plotting'''
    colors = []
    for i in np.arange(0., 360., 360. / n):
        h = i / 360.
        l = (50 + np.random.rand() * 10) / 100.
        s = (90 + np.random.rand() * 10) / 100.
        colors.append(hls_to_rgb(h, l, s))
    return colors


class plotVaryParent():
    """ Contains shared methods to inherit by classes that plot parameter varying data (weights, noise/uncertainty)"""
    def __init__(self, vary_EI):
        """
        Initalization method
        :param vary_EI: varyEI() class object. Allows it to load relevant data.
        """
        self.P = vary_EI.P.copy()

    def getGridVals(self,vals=None,rng_Y=[],rng_X=[]):
        """
        Produces the matrix used in plotting.
        :param vals: Vector. Values to place in the matrix.
        :param rng_Y: Vector. Range of values for Y axis (default=[])
        :param rng_X: Vector. Range of values for X axis (default=[])
        :return: grid_vals, rng_Y, rng_X, coord
        """
        if vals is None: raise ValueError('Variable "vals" is empty')
        if len(rng_Y) == 0: rng_Y = np.arange(0, self.P['excit_range'].shape[0])
        if len(rng_X) == 0: rng_X = np.arange(0, self.P['inhib_range'].shape[0])
        coord = np.array(list(product(rng_Y, rng_X, repeat=1)))
        # Create the matrix for plotting
        grid_vals = np.zeros((rng_X.shape[0], rng_Y.shape[0]))
        # Place values in the plotting matrix
        for v in range(len(coord)):
            grid_vals[list(coord[v])[1], list(coord[v])[0]] = vals[v]
        grid_vals = np.flipud(grid_vals.transpose())
        return grid_vals, rng_Y, rng_X, coord

    def plotHeatmap(self, ax=None, cmap='plasma', vals=None, vmin=[], vmax=[], c_bar_lab='',ttl=[],fontsize=5.5, \
                    excit_range=[], inhib_range=[], mu=[], annotate=True):
        """
        Produces heatmap of values
        :param ax: matplotlib axis object. If None, one is created (default=None)
        :param cmap: colormap for heatmap (default='plasma')
        :param vals: Values to plot.
        :param vmin: float/int. Minimum value of heatmap (default=[])
        :param vmax: float/int. Maximum value of heatmap (default=[])
        :param c_bar_lab: String. Colorbar label (default='')
        :param ttl: String. Figure title (default='')
        :param fontsize: float/int. (default=5.5)
        :param excit_range: Vector. Excitatory values for plot
        :param inhib_range: Vector. Inhibitory values for plot.
        :param mu: Vector. Input values for heatmap
        :param annotate: True/False. Whether to annotate the heatmap with values (default=True)
        :return:
        """
        if ax is None: fig, ax = plt.subplots()
        if len(excit_range) == 0:  excit_range=self.excit_range
        if len(inhib_range) == 0:  inhib_range = self.inhib_range
        if len(mu) == 0:  mu = self.P['mu']
        grid_vals, rng_E, rng_I, coord = self.getGridVals(vals=vals)
        # Plot the values and colorbar
        im = ax.imshow(grid_vals,cmap=plt.get_cmap(cmap),vmin=vmin, vmax=vmax)
        if annotate:
            # Loop over data dimensions and create text annotations.
            for i in range(len(rng_I)):  # CHANGED I TO E, NEED TO CHECK IF RIGHT
                for j in range(len(rng_E)):
                    plt.text(i, j, np.round(grid_vals[j, i], 2), ha="center", va="center", color="w", fontsize=fontsize)
        divider = make_axes_locatable(ax)
        cax = divider.append_axes("right", size="5%", pad=0.05)
        cbar = ax.figure.colorbar(im, cax=cax)
        # cbar.set_clim(vmin=vmin, vmax=vmax)
        cbar.ax.tick_params(labelsize=fontsize+3.5)
        cbar.draw_all()
        # Label the axes, etc.
        ax.set_yticks(np.flip(rng_E))
        ax.set_yticklabels(np.round(excit_range, 2), rotation=45, fontsize=fontsize+3.5)
        ax.set_xticks(rng_I)
        ax.set_xticklabels(np.round(inhib_range, 2), fontsize=fontsize+3.5,rotation=45)
        ax.set_ylabel('Excitatory Weight',fontsize=fontsize+5.5)
        ax.set_xlabel('Inhibitory Weight',fontsize=fontsize+5.5)
        if len(ttl) == 0:
            ax.set_title(
                'A = (%.d, %d), B = (%d, %d)' % (mu[0], mu[2], mu[1], mu[3]))
        else:
            ax.set_title(ttl)
        cbar.ax.set_ylabel(c_bar_lab, rotation=-90, va="bottom",fontsize=fontsize+5.5)

    def plotContour(self, ax=None, vals=None, levels=[], cmap='Spectral',ttl='',fontsize=10, \
                    excit_range=[], inhib_range=[], mu=[]):
        """
        Produces a contour map from values
        :param ax: matplotlib axis object. If None, one is created (default=None)
        :param cmap: colormap for heatmap (default='plasma')
        :param vals: Values to plot. (default=None)
        :param levels: Vector. Contour levels. (default=[])
        :param ttl: String. Figure title (default='')
        :param fontsize: float/int. (default=5.5)
        :param excit_range: Vector. Excitatory values for plot
        :param inhib_range: Vector. Inhibitory values for plot.
        :param mu: Vector. Input values for heatmap
        :return:
        """
        if ax is None: fig, ax = plt.subplots()
        if len(excit_range) == 0:  excit_range=self.excit_range
        if len(inhib_range) == 0:  inhib_range = self.inhib_range
        if len(mu) == 0:  mu = self.P['mu']
        if len(levels)== 0: levels=np.linspace(np.nanmin(vals[:]),np.nanmax(vals[:]),10)
        grid_vals, rng_E, rng_I, coord = self.getGridVals(vals=vals)
        scale_term = abs(np.diff(excit_range)[0] / np.diff(inhib_range)[0])
        X, Y = np.meshgrid(inhib_range * scale_term, excit_range)
        CS = ax.contour(np.fliplr(X), Y, np.flipud(np.round(grid_vals, 2)), levels, cmap=cmap)
        #Make axis labels
        excit_labels, inhib_labels = np.unique(np.round(excit_range,2)), np.unique(np.round(inhib_range,2))
        ax.axis('square')
        ax.set_yticks(excit_labels)
        ax.set_yticklabels(np.round(excit_labels,2), fontsize=fontsize+3.5,rotation=45)
        ax.set_xticks(inhib_labels * scale_term)
        ax.set_xticklabels(np.flip(np.round(inhib_labels, 3)), fontsize=fontsize+3.5, rotation=45)
        ax.set_ylabel('Recurrent Excitatory Weight', fontsize=(fontsize + 5.5))
        ax.set_xlabel('Inhibitory Weight', fontsize=(fontsize + 5.5))
        # ax.clabel(CS, inline=1, fontsize=fontsize)
        if len(ttl) == 0:
            ax.set_title(
                'A = (%.d, %d), B = (%d, %d)' % (mu[0], mu[2], mu[1], mu[3]),fontsize=(fontsize+4))
        else:
            ax.set_title(ttl)


class plotGrid():
    """
    Plots results of the gridVarBund() class.
    """
    def __init__(self, grid):
        """
        Initialization method.
        :param grid: gridVarBund() class object
        """
        self.P = grid.P.copy()
        if not 'grid_vary' in self.P:
            self.P['grid_vary'] = 'alt'
        self.p_A = grid.p_A.copy()
        self.decision_time = grid.decision_time.copy()
        self.ref_bund = grid.ref_bund.copy()
        self.var_bund = grid.var_bund.copy()

    def plotHeatmap(self, ax=None, vals='p_A', fontsize=5.5, cmap='bwr', annotate=True):
        """
        Produces a heatmap of the gridVarBund() data.
        :param ax: matplotlib axis object. (default=None)
        :param vals: 'p_A', 'decision_time'. Values used in the heatmap. (default='p_A')
        :param fontsize: float/int. (default=5.5)
        :param cmap: colormap for heatmap (default='bwr')
        :param annotate: True/False. Whether to annotate heatmap with values.
        :return:
        """
        if ax is None: fig, ax = plt.subplots()
        # Figure out what values to plot
        if vals == 'p_A':
            vals = np.round(self.p_A, 2)
            if self.P['grid_vary'] == 'alt':
                c_bar_lab = 'P(Var Bund Chosen)'
            elif self.P['grid_vary'] == 'att':
                c_bar_lab = 'P(Bund A Chosen)'
            else:
                raise ValueError("Invalid value for P['grid_vary']")
            vmin, vmax = 0, 1
        elif vals == 'decision_time':
            vals = np.round(np.nanmean(self.decision_time, axis=1), 2)
            c_bar_lab = 'Decision Time (s)'
            vmin, vmax = 0, 1
        # Axis labels
        if self.P['grid_vary'] == 'alt':
            xlab, ylab, ttl = 'Variable Bundle, Attribute X', 'Variable Bundle, Attribute Y', 'Reference Bundle = (1,1)'  # + str(self.ref_bund)
        elif self.P['grid_vary'] == 'att':
            xlab, ylab, ttl = 'Attribute X, Bundle A', 'Attribute X, Bundle B', 'Attribute Y (A,B) = (1,1)'  # + str(self.ref_bund)
        else:
            raise ValueError("Invalid value for P['grid_vary']")
        # Get the coordinates
        rng = np.arange(0, self.P['input_range'].shape[0])
        coord = list(product(rng, repeat=2))
        # Create the matrix for plotting
        grid_vals = np.zeros((rng.shape[0], rng.shape[0]))
        # Place values in the plotting matrix
        for v in range(len(coord)):
            grid_vals[list(coord[v])[1], list(coord[v])[0]] = vals[v]
        grid_vals = np.flipud(grid_vals)
        # Plot the values and colorbar
        im = ax.imshow(grid_vals, cmap=plt.get_cmap(cmap), vmin=vmin, vmax=vmax)
        cbar = ax.figure.colorbar(im)
        cbar.ax.tick_params(labelsize=fontsize)
        cbar.draw_all()
        # Label the axes, etc.
        axis_interval_vals = rng[np.arange(0, int(np.round((len(rng)))), 2)]  # Make the labels sparse for visiblity
        x_labels = np.round(self.P['input_range'] / self.ref_bund[0], 2)[::2]
        y_labels = np.round(self.P['input_range'] / self.ref_bund[1], 2)[::2]
        ax.set_xticks(axis_interval_vals)
        try:
            ax.set_xticklabels(x_labels, rotation=45, fontsize=fontsize)
        except:
            print('here')
        ax.set_yticks(np.flip(axis_interval_vals))
        ax.set_yticklabels(y_labels, fontsize=fontsize)
        ax.set_xlabel(xlab, fontsize=fontsize)
        ax.set_ylabel(ylab, fontsize=fontsize)
        ax.set_title(ttl, fontsize=fontsize + 2)
        cbar.ax.set_ylabel(c_bar_lab, rotation=-90, va="bottom", fontsize=fontsize)
        if annotate:
            # Loop over data dimensions and create text annotations.
            for i in range(len(rng)):
                for j in range(len(rng)):
                    plt.text(j, i, np.round(grid_vals[i, j], 2), ha="center", va="center", color="w", fontsize=fontsize)

    def saveHeatmap(self, ax=None, fig_name='bundles_heatmap', vals='p_A', fontsize=5.5, fmt='png', cmap='bwr',
                    annotate=True):
        """
        Saves Heatmap.
        :param ax: matplotlib axis object. (default=None)
        :param vals: 'p_A', 'decision_time'. Values used in the heatmap. (default='p_A')
        :param fontsize: float/int. (default=5.5)
        :param cmap: colormap for heatmap (default='bwr')
        :param annotate: True/False. Whether to annotate heatmap with values.
        :param fmt: save format (default='png')
        :return:
        """
        if ax is None: fig, ax = plt.subplots()
        self.plotHeatmap(ax=ax, vals=vals, fontsize=fontsize, cmap=cmap, annotate=annotate)
        plt.tight_layout()
        plt.savefig(fig_name + '.' + fmt, format=fmt, dpi=300)


class plotVaryIE(plotVaryParent):
    """
    Plots results from the varyEI() class object.
    """
    def __init__(self,vary_EI):
        """
        Initialization method
        :param vary_EI: Object produced by varyEI()
        """
        plotVaryParent.__init__(self,vary_EI)
        self.P = vary_EI.P.copy()
        self.weight_range = vary_EI.weight_range.copy()
        self.p_A = vary_EI.p_A.copy()
        self.decision_time = np.nanmean(vary_EI.decision_time,axis=1)
        self.excit_range = vary_EI.P['excit_range']
        self.inhib_range = vary_EI.P['inhib_range']
        try:
            if vary_EI.vary_noise_sigma: self.p_A_slope = vary_EI.p_A_slope
        except: print('vary_noise_sigma not found')
        try: self.poly_fit = vary_EI.poly_fit # For convexity plotting
        except: print('no convexity fitting found')

    def saveHeatmap(self, ax=None, fig_name='vary_EI_heatmap',vals='p_A',fontsize=5.5,format='png',annotate=True):
        """
        Produces heatmap from simulation values.
        :param ax: matplotlib axis object. If None, one is created (default=None)
        :param vals: 'p_A', 'decision_time', 'vary_noise', 'convexity'. Values to plot.
        :param fig_name: string. Path and name of figure (default='vary_EI_heatmap')
        :param fontsize: float/int. (default=5.5)
        :param format: string. File format (default='png')
        :param annotate: True/False. Whether to annotate the heatmap with values (default=True)
        :return:
        """
        if ax is None: fig, ax = plt.subplots()
        if vals == 'p_A':
            vals = np.round(self.p_A,2)
            c_bar_lab, ttl, cmap = '%A chosen', 'Percent A Chosen', 'viridis'
            vmin, vmax = 0, 1
        elif vals == 'decision_time':
            vals = np.round(self.decision_time,2)
            c_bar_lab, ttl, cmap = 'Decision Time (s)', "Mean Decision Time Per Trial", 'viridis'
            vmin, vmax = np.nanmin(vals.flatten()), np.nanmax(vals.flatten())
        elif vals == 'vary_noise':
            # vals = self.p_A_slope.astype(int)
            vals = self.p_A_slope
            c_bar_lab, ttl, cmap = 'Slope', 'Noise Slope', 'viridis'
            vmin, vmax = np.nanmin(vals.flatten()), np.nanmax(vals.flatten())
        elif vals == 'convexity':
            vals = self.poly_fit[0,:]
            c_bar_lab, ttl, cmap = 'Curvature', "Indifference Curvature", 'seismic'
            lm = max(abs(vals))
            vmin, vmax = -lm, lm
        else:
            raise ValueError("Values specified must be 'p_A', 'decision_time', or 'vary_noise'")
        self.plotHeatmap(ax=ax,vals=vals,vmin=vmin,vmax=vmax,c_bar_lab=c_bar_lab,fontsize=fontsize,ttl=ttl,cmap=cmap,
                         annotate=annotate)
        plt.tight_layout()
        plt.savefig((fig_name + '.' + format), format=format, dpi=300)

    def saveContour(self, ax=None, fig_name='vary_EI_contour',vals='p_A',fontsize=5.5,format='png',levels=[]):
        """
        Produces contour map from simulation values.
        :param ax: matplotlib axis object. If None, one is created (default=None)
        :param vals: 'p_A', 'decision_time', 'vary_noise', 'convexity'. Values to plot.
        :param fig_name: string. Path and name of figure (default='vary_EI_heatmap')
        :param fontsize: float/int. (default=5.5)
        :param format: string. File format (default='png')
        :param levels: vector. Used to determine levels of heatmap
        :return:
        """
        if ax is None: fig, ax = plt.subplots()
        if vals == 'p_A':
            vals = np.round(self.p_A,2)
            c_bar_lab, ttl, cmap = '%A chosen', 'Percent A Chosen', 'viridis'
            vmin, vmax = 0, 1
        elif vals == 'decision_time':
            vals = np.round(self.decision_time,2)
            c_bar_lab, ttl, cmap = 'Decision Time (s)', "Mean Decision Time Per Trial", 'viridis'
            vmin, vmax = np.nanmin(vals.flatten()), np.nanmax(vals.flatten())
        elif vals == 'vary_noise':
            # vals = self.p_A_slope.astype(int)
            vals = self.p_A_slope
            c_bar_lab, ttl, cmap = 'Slope', 'Noise Slope', 'viridis'
            vmin, vmax = np.nanmin(vals.flatten()), np.nanmax(vals.flatten())
        elif vals == 'convexity':
            vals = self.poly_fit[0,:]
            c_bar_lab, ttl, cmap = 'Curvature', "Indifference Curvature", 'seismic'
            lm = max(abs(vals))
            vmin, vmax = -lm, lm
        else:
            raise ValueError("Values specified must be 'p_A', 'decision_time', or 'vary_noise'")
        self.plotContour(ax=ax,vals=vals,fontsize=fontsize,ttl=ttl,levels=levels)
        plt.tight_layout()
        plt.savefig((fig_name + '.' + format), format=format, dpi=300)


def plotPsychometricCurve(input_range,ref_val,p_A,color='b',lbl='',ax=None,fontsize=12):
    """
    Plots psychometric curve produced by simulations
    :param ax: matplotlib subplot axis object
    :param input_range: Vector. X axis input values
    :param ref_val: float/int, Reference value for centering inputs
    :param p_A: Vector. Proportion of choices of A. Used to create curve
    :param color: Color of plot. (default='b')
    :param lbl: Label for what is plotted. (default='')
    :param fontsize: float/int. (default=12)
    :return:
    """
    if ax is None:
        fig, ax = plt.subplots()
    x = input_range - ref_val
    x = x/x.max() + 1
    ft, _ = curve_fit(sigmoid,x,p_A)
    x_fit = np.arange(x.min(),x.max(),0.01)
    y_fit = sigmoid(x_fit,ft[0],ft[1])
    ax.axvline(x=1, color='k',linestyle=':')
    ax.plot([0,2],[.5,.5],color='k',linestyle=':')
    ax.scatter(x,p_A,label=lbl,color=color)
    ax.plot(x_fit,y_fit,color=color,linewidth=4)
    ax.spines['right'].set_visible(False)
    ax.spines['top'].set_visible(False)
    ax.set_ylabel('P(A)',fontsize=fontsize+2)
    ax.set_xlabel('$\\alpha$',fontsize=fontsize+2)
    ax.tick_params(labelsize=fontsize)
    ax.set_box_aspect(1)
    ax.set_ylim([-0.05, 1.05])
    ax.set_xlim([0,2])
    ax.axvline(x=1, color='k',linestyle=':')


def plotFixedPointIndiffCurvatureContour(params, traj_fp, ax=None, lims=np.array([0, 40]), output='FP', save_dir='',
                               fmt='png',fontsize=10,save_bool=True,fig_name_base='fp_curvature_contour',
                               cmap='PRGn_r',levels=[.9,1.1],ttl='',nonlinear_sv=False,sv_exp=.5):
    """
    Plots the convexity contour map by weights using fixed point data produced by PyDStool
    :param params: Matrix of inputs and weights used during simulation
    :param traj_fp: The trajectory of the fixed points
    :param ax: matplotlib axis object. If None, one is created (default=None)
    :param lims: 2x1 vector. Limites for inputs (default=np.array([0, 40]))
    :param output: 'FR', 'FP'. Determines y_label variable.
    :param save_dir: String. Director to save the figure. (default='convexity_plots/')
    :param fmt: String. Format for file (default='png')
    :param fontsize: Float/int. Size of font (default=10)
    :param save_bool: True/False. Whether to save figure (default=True)
    :param fig_name_base: String. Base name of the figure (default=fp_curvature_contour)
    :param cmap: Colormap used in contour. (default='PRGn_r')
    :param levels: List. Determines levels of the contour. (default=[])
    :param ttl: Figure title (default='')
    :param nonlinear_sv: True/False. Whether to make subjective values nonlinear prior to input to network (default=False)
    :param sv_exp: flots. If nonlinear_sv=True, this exponent determines the nonlinearity
    :return:
    """
    if ax is None: fig, ax = plt.subplots()
    excit_range, inhib_range = np.unique(params[:, 2]), np.unique(params[:, 3])
    curvatures = getFixedPointCurvature(params, traj_fp, lims=lims, output=output,nonlinear_sv=nonlinear_sv,sv_exp=sv_exp) - 1
    plotContour(curvatures,excit_range, inhib_range, ax=ax, levels=levels,
                fontsize=fontsize, ttl=ttl, cmap=cmap)
    fig.tight_layout()
    if save_bool:
        plt.savefig(((save_dir + fig_name_base + '_contour') + '.' + fmt), format=fmt, dpi=300)


def plotFixedPointIndiffCurvatureHeatmap(params, traj_fp, ax=None, lims=np.array([0, 40]), output='FP', save_dir='',
                               fmt='png',fontsize=10,save_bool=True,fig_name_base='fp_optimal_reward_by_noise',
                               cmap='seismic',c_bar_lab='Curvature',ttl='',nonlinear_sv=False,sv_exp=.5,
                                         vmin=None,vmax=None):
    """
    Plots the convexity heatmap by weights using fixed point data produced by PyDStool
    :param params: Matrix of inputs and weights used during simulation
    :param traj_fp: The trajectory of the fixed points
    :param ax: matplotlib axis object. If None, one is created (default=None)
    :param lims: 2x1 vector. Limites for inputs (default=np.array([0, 40]))
    :param output: 'FR', 'FP'. Determines y_label variable.
    :param save_dir: String. Director to save the figure. (default='convexity_plots/')
    :param fmt: String. Format for file (default='png')
    :param fontsize: Float/int. Size of font (default=10)
    :param save_bool: True/False. Whether to save figure (default=True)
    :param fig_name_base: String. Base name of the figure (default=fp_curvature_contour)
    :param cmap: Colormap for curvature. (default='seismic')
    :param c_bar_lab: String. Label of heatmap colorbar (default='')
    :param ttl: Figure title (default='')
    :param nonlinear_sv: True/False. Whether to make subjective values nonlinear prior to input to network (default=False)
    :param sv_exp: flots. If nonlinear_sv=True, this exponent determines the nonlinearity
    :return:
    """
    if ax is None: fig, ax = plt.subplots()
    excit_range, inhib_range = np.unique(params[:, 2]), np.unique(params[:, 3])
    curvatures = getFixedPointCurvature(params, traj_fp, lims=lims, output=output,nonlinear_sv=nonlinear_sv,sv_exp=sv_exp) - 1
    #Get the limits for the colorbar
    if vmin is None or vmax is None:
        lm = max(abs(curvatures.flatten()))
        vmin, vmax = -lm, lm
    plotHeatmap(curvatures,excit_range, inhib_range, ax=ax, vmin=vmin, vmax=vmax, c_bar_lab=c_bar_lab,
                fontsize=fontsize, ttl=ttl, cmap=cmap)
    fig.tight_layout()
    if save_bool:
        plt.savefig(((save_dir + fig_name_base + '_heatmap') + '.' + fmt), format=fmt, dpi=300)


def plotHeatmap(grid_vals,excit_range,inhib_range,ax=None, cmap='seismic', vmin=[], vmax=[], c_bar_lab='',
                fontsize=12, ttl=[]):
    """

    :param grid_vals: Matrix of values determining convexity
    :param excit_range: Vector. Excitatory values used in simulations.
    :param inhib_range: Vector. Excitatory values used in simulations.
    :param ax: matplotlib axis object. If None, one is created (default=None)
    :param cmap: Colormap for curvature. (default='seismic')
    :param vmin: float. Minimum value of heatmap (default=[])
    :param vmax: float. Maximum value of heatmap (default=[])
    :param c_bar_lab: String. Label of heatmap colorbar (default='')
    :param fontsize: Size of font. (default=12)
    :param ttl: Figure title (default='')
    :return:
    """
    if ax == None: fig, ax = plt.subplots()
    plt.rcParams.update({'font.size': fontsize + 3.5})
    # Plot the values and colorbar
    im = ax.imshow(grid_vals, cmap=plt.get_cmap(cmap), vmin=vmin, vmax=vmax)
    divider = make_axes_locatable(ax)
    cax = divider.append_axes("right", size="5%", pad=0.05)
    cbar = ax.figure.colorbar(im, cax=cax)
    cbar.draw_all()
    cbar.ax.set_ylabel(c_bar_lab, rotation=-90, va="bottom", fontsize=fontsize + 3.5)
    # # Make axis labels
    excit_labels, inhib_labels = np.unique(np.round(excit_range, 2)), np.unique(np.round(inhib_range, 2))
    # # Label the axes, etc.
    ax.set_yticks(np.flipud(np.arange(0, len(excit_range))))
    ax.set_yticklabels(np.round(excit_labels, 2), rotation=45, fontsize=fontsize + 3.5)
    ax.set_xticks(np.arange(0, len(inhib_range)))
    ax.set_xticklabels(-1 * np.round(inhib_labels, 2), fontsize=fontsize + 3.5, rotation=45)
    ax.set_ylabel('Excitatory Weight', fontsize=fontsize + 5.5)
    ax.set_xlabel('Inhibitory Weight', fontsize=fontsize + 5.5)
    if len(ttl) == 0:
        ax.set_title('Curvature by Weight')
    else:
        ax.set_title(ttl)


def plotContour(grid_vals,excit_range,inhib_range,ax=None, cmap='PRGn_r', levels=[], fontsize=12, ttl=''):
    """
    Plots a contour map, customized for portraying convexity output for E/I tone.
    :param grid_vals: Matrix of values determining convexity
    :param excit_range: Vector. Excitatory values used in simulations.
    :param inhib_range: Vector. Excitatory values used in simulations.
    :param ax: matplotlib axis object. If None, one is created (default=None)
    :param cmap: Colormap for curvature. (default='PRGn_r')
    :param levels: List. Determines levels of the contour. (default=[])
    :param fontsize: Size of font. (default=12)
    :param ttl: Figure title (default='')
    :return:
    """
    plt.rcParams.update({'font.size': fontsize + 3.5})
    scale_term = abs(np.diff(excit_range)[0] / np.diff(inhib_range)[0])
    X, Y = np.meshgrid(inhib_range * scale_term, excit_range)
    CS = ax.contour(X, np.flipud(Y), np.round(grid_vals, 2), levels, cmap=cmap)
    # Make axis labels
    excit_labels, inhib_labels = np.unique(np.round(excit_range, 2)), np.unique(np.round(inhib_range, 2))
    ax.axis('square')
    ax.set_yticks(excit_labels)
    ax.set_yticklabels(np.round(excit_labels, 2), fontsize=fontsize + 3.5, rotation=45)
    ax.set_xticks(inhib_labels * scale_term)
    ax.set_xticklabels(-1 * np.round(inhib_labels, 3), fontsize=fontsize + 3.5, rotation=45)
    ax.set_ylabel('Recurrent Excitatory Weight', fontsize=(fontsize + 5.5))
    ax.set_xlabel('Inhibitory Weight', fontsize=(fontsize + 5.5))
    if len(ttl) == 0:
        ax.set_title('Curvature by Weight')
    else:
        ax.set_title(ttl)


def plotFpIO(params,traj_fp,E = 0.33,I = 0.0,lims=np.array([0, 40]),ref=20,fontsize=18,
         fmt='png',f_dir='',output='FP',fig_size=(6,5)):
    """
    Plots the I/O relationship using fixed points produced by PyDSTool.
    :param params: Matrix of inputs and weights used during simulation
    :param traj_fp: The trajectory of the fixed points
    :param E: float. Excitatory weight (default=0.33)
    :param I: float. Inhibitory weight (default=0.0)
    :param ref: float. Reference value for indexing (default=20)
    :param lims: 2x1 vector. Limites for inputs (default=np.array([0, 40]))
    :param fontsize: float/int. (default=18)
    :param fmt: str. Plot file format (default='png')
    :param f_dir: str. Location to store plot (default='')
    :param output: 'FR', 'FP'. Determines y_label variable. (default='FP')
    :param fig_size: 2x1 array. (default=(6,5))
    :return:
    """
    # Make the title for the figure
    ttl = output + ' - IO - E=%.2f, I=%.2f, ref=%d, lims=(%d, %d)' % (E, I, ref, lims[0], lims[-1])
    #Collect the data
    X_A, X_B, inpt_X, _, _, _, _ = cullFixedPointData(params, traj_fp, E=E, I=I, ref=ref, lims=lims, output=output)
    inpt = (inpt_X + lims.min() - ref)/ref
    #Plot and save
    fig, ax = plt.subplots(figsize=fig_size)
    ax.scatter(inpt, X_A, color='r', marker='.', s=60, label='XA')
    ax.scatter(inpt, X_B, color='b', marker='.', s=60, label='XB')
    ax.set_xlabel('Input', fontsize=fontsize)
    ax.set_ylabel(output, fontsize=fontsize)
    ax.set_xlim([inpt.min(), inpt.max()])
    ax.set_ylim([0,max(X_A.max(), X_B.max())*1.08])
    ax.spines['right'].set_visible(False)
    ax.spines['top'].set_visible(False)
    ax.tick_params(labelsize=fontsize)
    # ax.grid()
    ax.legend(fontsize=fontsize)
    fig.suptitle(ttl, y=1.01, fontsize=16)
    plt.tight_layout()
    fig.savefig((f_dir + ttl + '.' + fmt), format=fmt, dpi=300)
    print('done')


class plotRewardRate(plotVaryParent):
    """
    Produces figures utilizing results from the rewardRates() class.
    """

    def __init__(self,reward_rate):
        """
        Initialization method.
        :param reward_rate: Object produced by the rewardRates() class that will be used to generate figures.
        """
        plotVaryParent.__init__(self, reward_rate)
        # self.P = reward_rate
        self.reward_observed = reward_rate.reward_observed
        self.reward_max = reward_rate.reward_max
        self.reward_min = reward_rate.reward_min
        self.p_correct = reward_rate.p_correct
        self.reward_observed_slope = reward_rate.reward_observed_slope
        self.reward_max_slope = reward_rate.reward_max_slope
        self.reward_min_slope = reward_rate.reward_min_slope
        self.p_correct_slope = reward_rate.p_correct_slope
        self.decision_time_slope = reward_rate.decision_time_slope
        self.intermed_agree = reward_rate.intermed_agree
        self.intermed_time_gain = reward_rate.intermed_time_gain
        self.intermed_p_correct = reward_rate.intermed_p_correct
        if np.isnan(self.decision_time_slope).any():
            _, _, _, _, self.decision_time_slope = reward_rate.calcNoiseSlope()
        self.vary_noise_sigma = reward_rate.vary_noise_sigma
        self.stability_time_mean = reward_rate.stability_time_mean
        self.decision_time_mean = np.nanmean(reward_rate.decision_time_mean,axis=0)
        self.n_trials_stable = reward_rate.n_trials_stable
        self.p_A = reward_rate.p_A
        self.dim_key = reward_rate.dim_key
        self.bundles = reward_rate.bundles
        self.excit_range = reward_rate.P['excit_range']
        self.inhib_range = reward_rate.P['inhib_range']
        # Add newer variables (allows for backward compatability)
        if hasattr(reward_rate, 'intermed_agree'):
            self.intermed_agree = reward_rate.intermed_agree
            self.intermed_time_gain = reward_rate.intermed_time_gain
            self.p_intermed_decision = reward_rate.p_intermed_decision
        if hasattr(reward_rate,'p_failed'):
            self.p_failed = reward_rate.p_failed
        if hasattr(reward_rate,'intermed_att_p_correct'):
            self.intermed_att_p_correct = reward_rate.intermed_att_p_correct
            self.intermed_att_agree = reward_rate.intermed_att_agree
            self.intermed_att_time_gain = reward_rate.intermed_att_time_gain
            self.p_intermed_att_decision = reward_rate.p_intermed_att_decision

    def saveHeatmap(self, ax=None, fig_name_base='reward_rate_plot',vals='p_A',fontsize=5.5,format='png',cmap='plasma',
                    annotate=False,vmin=None, vmax=None):
        """
        Primary method called by user to generate figures (name ain't intuitive, sorry).
        :param ax: matlab subplot ax object (default=None).
        :param fig_name_base: str. basename of figure (default='reward_rate_plot')
        :param vals: 'reward_observed', 'reward_max', 'reward_min', 'reward_observed_minus_min', 'reward_normalized',
                      'p_correct', 'reward_observed_slope', 'reward_max_slope', 'reward_min_slope', 'p_correct_slope',
                      'stability_time_mean', 'stability_time_var', 'p_stable', 'p_completed_bundles', 'decision_time',
                      'reaction_time', 'decision_time_slope', 'reward_per_second', 'intermed_p_correct',
                      'intermed_att_p_correct', 'intermed_v_decision_area_p_correct', 'intermed_agree',
                      'intermed_att_agree', 'intermed_time_gain', 'intermed_reaction_time', 'p_intermed_decision',
                      'p_intermed_att_decision', 'p_failed', Specifies what is plotted.
        :param fontsize: loat/int (default=5.5)
        :param format: str. Save format for output (default='png')
        :param cmap: str. Colormap for heatmaps (default='plasma')
        :param annotate: True/False. Whether to annotate heatmaps with the values (default=False)
        :param vmin: float/int. Minimum value on heatmap (default=None)
        :param vmax: float/int. Maximum value on heatmap (default=None)
        :return:
        """
        if vals == 'reward_observed':
            vals, loop_noise, mult_atts = np.round(self.reward_observed,2), True, False
            c_bar_lab, ttl_base = 'Mean Reward Per Trial', 'Observed Reward'
            # vmin, vmax = np.min(self.reward_observed), np.max(self.reward_max)
            if vmin is None: vmin = (np.min(self.reward_min)+np.min(self.reward_max))/2
            if vmax is None:  vmax = np.max(self.reward_max)
        elif vals == 'reward_max':
            vals, loop_noise, mult_atts = np.round(self.reward_max,2), True, False
            c_bar_lab, ttl_base = 'Mean Reward Per Trial', "Max Reward"
            if vmin is None: vmin = np.min(self.reward_min)
            if vmax is None: vmax = np.max(self.reward_max)
        elif vals == 'reward_min':
            vals, loop_noise, mult_atts = np.round(self.reward_min,2), True, False
            c_bar_lab, ttl_base = 'Mean Reward Per Trial', 'Min Reward'
            if vmin is None: vmin = np.min(self.reward_min)
            if vmax is None: vmax = np.max(self.reward_max)
        elif vals == 'reward_observed_minus_min':
            reward_norm = (self.reward_observed - self.reward_min)
            vals, loop_noise, mult_atts = np.round(reward_norm, 2), True, False
            c_bar_lab, ttl_base = 'Reward Per Trial', 'Reward Minus Min'
            if vmin is None: vmin = np.min(reward_norm)
            if vmax is None: vmax = np.max(reward_norm)
        elif vals == 'reward_normalized':
            reward_norm = (self.reward_observed-self.reward_min)/(self.reward_max-self.reward_min)
            vals, loop_noise, mult_atts = np.round(reward_norm, 2), True, False
            c_bar_lab, ttl_base = 'Proportion of Max Reward Per Trial', 'Normalized Reward'
            if vmin is None: vmin = np.min(reward_norm)
            if vmax is None: vmax = np.max(reward_norm)
        elif vals == 'p_correct':
            vals, loop_noise, mult_atts = np.round(self.p_correct, 2), True, False
            c_bar_lab, ttl_base = 'P(Larger Chosen)', 'Percent Correct'
            if vmin is None: vmin = 0
            if vmax is None: vmax = 1
        elif vals == 'reward_observed_slope':
            vals, loop_noise, mult_atts = np.round(self.reward_observed_slope, 2), False, False
            c_bar_lab, ttl_base = 'Noise Slope', 'Observed Reward Slope'
            if vmin is None: vmin = np.min(self.reward_observed_slope)
            if vmax is None: vmax = np.max(self.reward_observed_slope)
        elif vals == 'reward_max_slope':
            vals, loop_noise, mult_atts = np.round(self.reward_max_slope, 2), False, False
            c_bar_lab, ttl_base = 'Noise Slope', 'Max Reward Slope'
            if vmin is None: vmin = np.min(self.reward_max_slope)
            if vmax is None: vmax = np.max(self.reward_max_slope)
        elif vals == 'reward_min_slope':
            vals, loop_noise, mult_atts = np.round(self.reward_min_slope, 2), False, False
            c_bar_lab, ttl_base = 'Noise Slope', 'Min Reward Slope'
            if vmin is None: vmin = np.min(self.reward_min_slope)
            if vmax is None: vmax = np.max(self.reward_min_slope)
        elif vals == 'p_correct_slope':
            vals, loop_noise, mult_atts = np.round(self.p_correct_slope, 2), False, False
            c_bar_lab, ttl_base = 'Noise Slope', 'P(Larger Chosen) Slope'
            if vmin is None: vmin = np.min(self.p_correct_slope)
            if vmax is None: vmax = np.max(self.p_correct_slope)
        elif vals == 'stability_time_mean':
            vals, loop_noise, mult_atts = np.round(np.nanmean(self.stability_time_mean,axis=0),2), False, True
            c_bar_lab, ttl_base = 'Seconds', 'FP Stability Time Mean'
            if vmin is None: vmin = 0
            if vmax is None: vmax = max(2, vals.max())
        elif vals == 'stability_time_var':
            vals, loop_noise, mult_atts = np.round(np.nanvar(self.stability_time_mean, axis=0), 2), False, True
            c_bar_lab, ttl_base = 'Seconds', 'FP Stability Time Var'
            if vmin is None: vmin = 0
            if vmax is None: vmax = vals.max()
        elif vals == 'p_stable':
            vals, loop_noise, mult_atts = np.round(np.nanmean(self.n_trials_stable, axis=0), 2) / self.P['n_trials'], False, True
            c_bar_lab, ttl_base = 'Percent', 'Percent of Trials Achieving Stability'
            if vmin is None: vmin = 0
            if vmax is None: vmax = 1
        elif vals == 'p_completed_bundles':
            vals, loop_noise, mult_atts = 1-(np.isnan(self.p_A).sum(axis=0))/self.p_A.shape[0], True, False
            c_bar_lab, ttl_base = 'Percent', 'Percent of Bundles with Decision'
            if vmin is None: vmin = 0
            if vmax is None: vmax = 1
        elif vals == 'decision_time':
            vals, loop_noise, mult_atts = np.round(self.decision_time_mean,2), True, False
            c_bar_lab, ttl_base = 'Seconds', 'Mean Decision Time'
            if vmin is None: vmin = 0.75
            if vmax is None: vmax = 1.8
        elif vals == 'reaction_time':
            vals, loop_noise, mult_atts = np.round(self.decision_time_mean,2)-brian2.asarray(self.P['stim_on']), True, False
            c_bar_lab, ttl_base = 'Seconds', 'Mean Reaction Time'
            if vmin is None: vmin = np.min(vals)
            if vmax is None: vmax = np.max(vals)
        elif vals == 'decision_time_slope':
            vals, loop_noise, mult_atts = np.round(self.decision_time_slope,2), False, False
            c_bar_lab, ttl_base = 'Noise Slope', 'Decision Time Slope'
            if vmin is None: vmin = np.min(self.decision_time_slope)
            if vmax is None: vmax = np.max(self.decision_time_slope)
        elif vals == 'reward_per_second':
            vals, loop_noise, mult_atts = np.round(np.mean(self.calcRewardPerSecond(),axis=0),2), True, False
            c_bar_lab, ttl_base = 'Reward/Second', 'Mean Reward Per Second'
            if vmin is None: vmin = np.min(vals)
            if vmax is None: vmax = np.max(vals)
        elif vals == 'intermed_p_correct':
            vals, loop_noise, mult_atts = np.round(self.intermed_p_correct, 2), True, False
            c_bar_lab, ttl_base = 'P(Larger Chosen)', 'Intermediate Percent Correct'
            if vmin is None: vmin = 0
            if vmax is None: vmax = 1
        elif vals == 'intermed_att_p_correct':
            vals, loop_noise, mult_atts = np.round(self.intermed_att_p_correct, 2), True, False
            c_bar_lab, ttl_base = 'P(Larger Chosen)', 'Intermediate Att Percent Correct'
            if vmin is None: vmin = 0
            if vmax is None: vmax = 1
        elif vals == 'intermed_v_decision_area_p_correct':
            vals, loop_noise, mult_atts = np.round(self.intermed_p_correct-self.p_correct, 2), True, False
            c_bar_lab, ttl_base = 'TA-DA P(Larger Chosen)', 'Intermediate - Decision Area P(Larger Chosen)'
            if vmin is None: vmin = -1*np.max(abs(vals))
            if vmax is None: vmax = np.max(abs(vals))
        elif vals == 'intermed_att_v_decision_area_p_correct':
            vals, loop_noise, mult_atts = np.round(self.intermed_att_p_correct-self.p_correct, 2), True, False
            c_bar_lab, ttl_base = 'TA-DA P(Larger Chosen)', 'Intermediate Att - Decision Area P(Larger Chosen)'
            if vmin is None: vmin = -1*np.max(abs(vals))
            if vmax is None: vmax = np.max(abs(vals))
        elif vals == 'intermed_agree':
            vals, loop_noise, mult_atts = np.round(np.nanmean(self.intermed_agree, axis=0), 3), True, False
            c_bar_lab, ttl_base = 'P(Trials Readout Agrees)', 'Intermed Readout Agrees'
            if vmin is None: vmin = 0
            if vmax is None: vmax = 1
        elif vals == 'intermed_att_agree':
            vals, loop_noise, mult_atts = np.round(np.nanmean(self.intermed_att_agree, axis=0), 3), True, False
            c_bar_lab, ttl_base = 'P(Trials Readout Agrees)', 'Intermed Att Readout Agrees'
            if vmin is None: vmin = 0
            if vmax is None: vmax = 1
        elif vals == 'intermed_time_gain':
            vals, loop_noise, mult_atts = np.round(np.nanmean(self.intermed_time_gain, axis=0), 3), True, False
            c_bar_lab, ttl_base = 'Seconds', 'Intermed Readout Time-Gain'
            if vmin is None: vmin = 0
            if vmax is None: vmax = np.nanmax(vals)
        elif vals == 'intermed_att_time_gain':
            vals, loop_noise, mult_atts = np.round(np.nanmean(self.intermed_att_time_gain, axis=0), 3), True, False
            c_bar_lab, ttl_base = 'Seconds', 'Intermed Att Readout Time-Gain'
            if vmin is None: vmin = 0
            if vmax is None: vmax = np.nanmax(vals)
        elif vals == 'intermed_reaction_time':
            vals = self.decision_time_mean - brian2.asarray(self.P['stim_on']) - self.intermed_time_gain
            # vals[np.isnan(vals)] = 0
            vals, loop_noise, mult_atts = np.round(np.nanmean(vals, axis=0), 3), True, False
            c_bar_lab, ttl_base = 'Seconds', 'Intermed Reaction Time'
            if vmin is None: vmin = 0
            if vmax is None: vmax = np.nanmax(vals)
        elif vals == 'p_intermed_decision':
            vals = self.p_intermed_decision.copy()
            # vals[np.isnan(vals)] = 0
            vals, loop_noise, mult_atts = np.round(np.mean(vals, axis=0), 3), True, False
            c_bar_lab, ttl_base = 'P(Trials Readout Successful)', 'P(Intermed Readout Success)'
            if vmin is None: vmin = 0
            if vmax is None: vmax = np.nanmax(vals)
        elif vals == 'p_intermed_att_decision':
            vals = self.p_intermed_att_decision.copy()
            # vals[np.isnan(vals)] = 0
            vals, loop_noise, mult_atts = np.round(np.mean(vals, axis=0), 3), True, False
            c_bar_lab, ttl_base = 'P(Trials Readout Successful)', 'P(Intermed Att Readout Success)'
            if vmin is None: vmin = 0
            if vmax is None: vmax = np.nanmax(vals)
        elif vals == 'p_failed':
            vals, loop_noise, mult_atts = np.round(np.mean(self.p_failed,axis=0), 2), True, False
            c_bar_lab, ttl_base = 'P(Failed)', 'P(Bundles No Decision)'
            if vmin is None: vmin = np.min(vals)
            if vmax is None: vmax = np.max(vals)
        else:
            raise ValueError("Values specified must be 'reward_observed', 'reward_max', 'reward_min', 'reward_normalized', \
                      'p_correct', 'reward_observed_slope', 'reward_max_slope', 'reward_min_slope', 'p_correct_slope', \
                      'stability_time_mean', 'stability_time_var', 'p_stable', 'p_completed_bundles', 'decision_time', \
                      'reaction_time', 'decision_time_slope', 'reward_per_second', 'intermed_p_correct', \
                      'intermed_att_p_correct', 'intermed_v_decision_area_p_correct', 'intermed_agree', \
                      'intermed_att_agree', 'intermed_time_gain', 'intermed_reaction_time', 'p_intermed_decision', \
                      'p_intermed_att_decision', 'p_failed', 'reward_observed_minus_min'")
        # if loop_noise:
        if mult_atts:
            for n in range(vals.shape[1]):
                fig, ax = plt.subplots()
                ttl = ttl_base + ', Att - ' + str(n+1)
                self.plotHeatmap(ax=ax, vals=vals[:, n], vmin=vmin, vmax=vmax, c_bar_lab=c_bar_lab, ttl=ttl,
                                 fontsize=fontsize,cmap=cmap,annotate=annotate)
                plt.tight_layout()
                plt.savefig(fig_name_base + ttl + '.' + format, format=format, dpi=300)
        if loop_noise:
            for n in range(len(self.P['noise_sigma_range'])):
                fig, ax = plt.subplots()
                ttl = ttl_base + ', Noise - ' + str(round(self.P['noise_sigma_range'][n],2))
                self.plotHeatmap(ax=ax, vals=vals[:,n], vmin=vmin, vmax=vmax, c_bar_lab=c_bar_lab, ttl=ttl,
                                 fontsize=fontsize,cmap=cmap,annotate=annotate)
                plt.tight_layout()
                plt.savefig(fig_name_base + ttl + '.' + format, format=format, dpi=300)
        if (mult_atts<1) and (loop_noise<1):
            if ax is None: fig, ax = plt.subplots()
            self.plotHeatmap(ax=ax, vals=vals, vmin=vmin, vmax=vmax, c_bar_lab=c_bar_lab, ttl=ttl_base,
                             fontsize=fontsize,cmap=cmap,annotate=annotate)
            plt.tight_layout()
            plt.savefig(fig_name_base + ttl_base + '.' + format, format=format, dpi=300)

    def calcRewardPerSecond(self):
        """
        Calculates the rewards per second
        :return: reward_per_sec
        """

        A = np.sum(self.bundles[:, [0, 2]], axis=1)
        B = np.sum(self.bundles[:, [1, 3]], axis=1)
        reward = np.zeros((self.p_A.shape))
        for w in range(self.p_A.shape[1]):
            for n in range(self.p_A.shape[2]):
                reward[:, w, n] = A * self.p_A[:, w, n] + B * (1 - self.p_A[:, w, n])
        reward_per_sec = reward / self.decision_time_mean
        return reward_per_sec


class plotHNrelLN():
    """
    Produces comparison of results between the hierarchical and linear networks.
    """
    def __init__(self,hn_reward_rate,ln_reward_rate):
        """
        Initialization method.
        :param hn_reward_rate: rewardRate() object for hierarchical network simulations
        :param ln_reward_rate: rewardRate() object for linear network simulations
        """
        self.hn_rr = hn_reward_rate
        self.ln_rr = ln_reward_rate
        #Align noise
        self.ln_rr, self.hn_rr = self.alignNoiseSigma(self.ln_rr,self.hn_rr)
        #Sort the bundles
        inds = self.sortBundles(self.hn_rr.bundles, self.ln_rr.bundles)
        self.ln_rr.bundles, self.ln_rr.p_A = self.ln_rr.bundles[inds, :], self.ln_rr.p_A[inds, :]

    def getRelPerf(self):
        """
        Compare the relative performance of the models
        :return: perc_greater, ln_reward, hn_reward
        """
        #Calculate bundles and create variables
        A = np.sum(self.hn_rr.bundles[:, [0, 2]], axis=1)
        B = np.sum(self.hn_rr.bundles[:, [1, 3]], axis=1)
        hn_reward, ln_reward = self.hn_rr.p_A * 0, self.ln_rr.p_A * 0
        perc_greater = self.hn_rr.p_A[0, :, :] * 0
        #Loop through to calc values
        for n in range(self.hn_rr.p_A.shape[2]):
            ln_reward[:, n] = A * self.ln_rr.p_A[:, n] + B * (1 - self.ln_rr.p_A[:, n])
            for w in range(self.hn_rr.p_A.shape[1]):
                hn_reward[:, w, n] = A * self.hn_rr.p_A[:, w, n] + B * (1 - self.hn_rr.p_A[:, w, n])
                perc_greater[w, n] = np.nanmean(hn_reward[:, w, n] > ln_reward[:, n])
        return perc_greater, ln_reward, hn_reward

    def sortBundles(self,bundles1, bundles2):
        """
        Makes sure that the bundles from the two models are aligned
        :param bundles1: First set of bundles
        :param bundles2: Second set of bundles
        :return: vector for second bundle to make it match the first
        """
        if bundles1.shape[0] != bundles2.shape[0]:
            raise ValueError('Bundles count is not equal ')
        inds = np.zeros(bundles1.shape[0])
        for i in range(bundles1.shape[0]):
            inds[i] = (np.flatnonzero((bundles1[i, :] == bundles2).all(1))).astype(int)
        return inds.astype(int)

    def alignNoiseSigma(self,ln_rr,hn_rr):
        """
        If the two are varied by noise, it makes sure those are aligned
        :param ln_rr: rewardRate() object for linear network simulations
        :param hn_rr: rewardRate() object for hierarchical network simulations
        :return:
        """
        vl, I_hn, I_ln = np.intersect1d(hn_rr.P['noise_sigma_range'], \
                                        ln_rr.P['noise_sigma_range'], return_indices=True)
        hn_rr.p_A = hn_rr.p_A[:, :, I_hn]
        ln_rr.p_A = ln_rr.p_A[:, I_ln]
        return ln_rr, hn_rr

    def saveHeatmap(self,fig_name_base='',ttl_base='Bundles Relative LN Heatmap',c_bar_lab='P(HN>LN)',fontsize=5.5,
                    format='png',cmap='plasma',annotate=False):
        """
        Saves a heatmap of results
        :param fig_name_base: string. Base specifying path of file (default='')
        :param ttl_base: string. Title of figure and name of file (default='Bundles Relative LN Heatmap')
        :param c_bar_lab: string. Label of the colorbar (default='P(HN>LN)')
        :param fontsize: float/int. (default=5.5)
        :param format: str. Figure format (default='png')
        :param cmap: Colormap for the figure (default='plasma')
        :param annotate: True/False. Whether to annotate heatmap with actual values (default=False)
        :return:
        """
        perc_greater, _, _ = self.getRelPerf()
        vmin, vmax = np.min(perc_greater), np.max(perc_greater)
        plot_vary = plotVaryParent(self.hn_rr)
        plot_vary.excit_range = self.hn_rr.P['excit_range']
        plot_vary.inhib_range = self.hn_rr.P['inhib_range']
        for n in range(len(plot_vary.P['noise_sigma_range'])):
            fig, ax = plt.subplots()
            ttl = ttl_base + ', Noise - ' + str(round(plot_vary.P['noise_sigma_range'][n], 2))
            plot_vary.plotHeatmap(ax=ax, vals=perc_greater[:, n], vmin=vmin, vmax=vmax, c_bar_lab=c_bar_lab, ttl=ttl,
                             fontsize=fontsize, cmap=cmap, annotate=annotate)
            plt.tight_layout()
            plt.savefig(fig_name_base + ttl + '.' + format, format=format, dpi=300)

    def saveScatter(self,fig_name_base='',ttl='Bundles Relative SD Scatter',fontsize=14,format='png'):
        """
        Saves a summary scatter plot of performance
        :param fig_name_base: str. Path for figure file save (default='')
        :param ttl: str. Title of figure and the output file (default='Bundles Relative SD Scatter')
        :param fontsize: float/int (default=14)
        :param format: str. File format (default='png')
        :return:
        """
        perc_greater, _, _ = self.getRelPerf()
        mn = np.mean(perc_greater, axis=0)
        mx = np.max(perc_greater, axis=0)
        x = self.hn_rr.P['noise_sigma_range']
        fix, ax = plt.subplots()
        ax.scatter(x, mn, color='k', marker='x', label='Mean of HN Weight Configurations')
        ax.scatter(x, mx, color='k', label='Best HD Weight Configuration')
        ax.set_xlabel('Uncertainty',fontsize=fontsize)
        ax.set_ylabel('P(HN > LN)',fontsize=fontsize)
        ax.tick_params(axis='both', which='major', labelsize=fontsize)
        ax.legend(fontsize=fontsize - 3)
        ax.spines['right'].set_visible(False)
        ax.spines['top'].set_visible(False)
        ax.set_title('Proportion of Bundles',fontsize=fontsize)
        plt.tight_layout()
        plt.savefig(fig_name_base + ttl + '.' + format, format=format, dpi=300)


def plotOptimalWeightsConvexByNoise(f_dir='pickles/',save_dir='convexity_plots/',fmt='png',font_size=14,
         reward_rate_f_name='hn_reward_rate_vals_0_2_20_all_signal_noise_0_v_2_intrnl_noise_0.003.pickle',
         save_bool=True,fig_name_base='ww_optimal_reward_by_noise',vals='p_correct',
         grid_f_name='grid_vary_0_40_2_E_.3_.4_I_0_.1_ellipseFit.pickle',line_width=3,clrs=['m','c'],val_min=0,
         bounds=([-10, -10, -10, -10], [10, 10, 10, 10])):
    """
    Function used to plot the optimal convexity as a function of noise for different weight values
    :param f_dir: str. Path to where data is stored (default='pickles/')
    :param save_dir: str. Path of where to save the figure (default='convexity_plots/')
    :param fmt: str. Figure format (default='png')
    :param font_size: float/int (default=14)
    :param reward_rate_f_name: name of rewardRate() object
    :param save_bool: True/False. Whether to save the results (default=True)
    :param fig_name_base: str. Name of figure that's saved
    :param vals: 'p_correct', 'reward_observed_minus_min', 'reward_observed'. Values to be plotted
    :param grid_f_name: Name of gridVarBund() object
    :param line_width: Width of lines on the figure (default=3)
    :param clrs: list. Colors for the models (default=['m','c'])
    :param val_min: float/int. Mimimum value for bundles (default=0).
    :param bounds: tuple. Limits for fitting (default=([-10, -10, -10, -10], [10, 10, 10, 10]))
    :return:
    """
    #Load the data
    reward_rate = rewardRate()
    reward_rate.loadRewardRate(file_name=(f_dir + reward_rate_f_name))
    grid = gridVarBund()
    grid.loadGridData(file_name=(f_dir + grid_f_name))
    #Trim the bundle (yeah, we're getting hacky)
    reward_rate = trimBundleMin(reward_rate, val_min=val_min)
    #Create a vector of convexities corresponding to reward rate weights
    convexity = np.zeros(reward_rate.weight_range.shape[0])
    for w in range(reward_rate.weight_range.shape[0]):
        I = (abs(reward_rate.weight_range[w, 0] - grid.weight_range[:, 0]) < 0.001) * \
            (abs(reward_rate.weight_range[w, 1] - grid.weight_range[:, 1]) < 0.001)
        convexity[w] = grid.poly_fit[0, I]
    #Index for the best weights/convexity
    if vals == 'p_correct':
        I = np.argmax(reward_rate.p_correct, axis=0)
    elif vals == 'reward_observed_minus_min':
        I = np.argmax((reward_rate.reward_observed - reward_rate.reward_min), axis=0)
    elif vals == 'reward_observed':
        I = np.argmax((reward_rate.reward_observed), axis=0)
    else:
        raise ValueError("Invalid vals specified")
    # Fit the sigmoids
    excit, inhib = reward_rate.weight_range[I, 0], reward_rate.weight_range[I, 1]
    noise_sig, cnvxty = reward_rate.P['noise_sigma_range'], convexity[I]
    x = np.arange(np.min(noise_sig), np.max(noise_sig), 0.0001)
    popt_E, pcov_E = sc.optimize.curve_fit(sigmoidFourParam, noise_sig, excit, bounds=bounds)
    popt_I, pcov_I = sc.optimize.curve_fit(sigmoidFourParam, noise_sig, inhib, bounds=bounds)
    popt_C, pcov_C = sc.optimize.curve_fit(sigmoidFourParam, noise_sig, cnvxty, bounds=bounds)
    y_E = sigmoidFourParam(x, popt_E[0], popt_E[1], popt_E[2], popt_E[3])
    y_I = sigmoidFourParam(x, popt_I[0], popt_I[1], popt_I[2], popt_I[3])
    y_C = sigmoidFourParam(x, popt_C[0], popt_C[1], popt_C[2], popt_C[3])
    #Plot the optimal convexity
    plt.rcParams.update({'font.size': font_size})
    fig, ax = plt.subplots()
    ax.scatter(noise_sig, cnvxty, c='k',linewidth=line_width)
    ax.plot(x, y_C, color='k', linewidth=line_width)
    ax.set_xlabel('Environmental Uncertainty')
    ax.set_ylabel('Curvature')
    ax.set_title('Optimal Curvature')
    ax.spines['right'].set_visible(False)
    ax.spines['top'].set_visible(False)
    plt.tight_layout()
    if save_bool:
        plt.savefig((save_dir + fig_name_base + '_convexity' + '.' + fmt), format=fmt, dpi=300)
    #Plot the optimal weights
    fig, ax1 = plt.subplots()
    # ax1.plot(noise_sig, excit, color=clrs[0], linewidth=3, label='Excit Weight')
    ax1.scatter(noise_sig, excit, c=clrs[0], linewidth=3)
    ax1.plot(x, y_E, color=clrs[0], linewidth=3, label='Excit Weight')
    ax1.set_xlabel('Environmental Uncertainty')
    ax1.set_ylabel('Excitatory Weight', color=clrs[0])
    ax1.set_ylim([reward_rate.weight_range[:, 0].min()-0.005, reward_rate.weight_range[:, 0].max()+0.005])
    ax1.tick_params(axis='y', labelcolor=clrs[0])
    ax1.spines['top'].set_visible(False)
    ax1.set_title('Optimal Weights')
    ax2 = ax1.twinx()
    # ax2.plot(noise_sig, inhib, color=clrs[1], linewidth=3, label='Inhib Weight')
    ax2.scatter(noise_sig, inhib, c=clrs[1], linewidth=3)
    ax2.plot(x, y_I, color=clrs[1], linewidth=3, label='Inhib Weight')
    ax2.set_ylabel('Inhibitory Weight', color=clrs[1])
    ax2.set_ylim([reward_rate.weight_range[:, 1].max()+0.005, reward_rate.weight_range[:, 1].min()-0.005])
    ax2.set_xlim([noise_sig.min(), noise_sig.max()])
    ax2.tick_params(axis='y', labelcolor=clrs[1])
    ax2.spines['top'].set_visible(False)
    fig.tight_layout()
    if save_bool:
        plt.savefig((save_dir + fig_name_base + '_weights' + '.' + fmt), format=fmt, dpi=300)

def trimBundleMin(reward_rate,val_min=10):
    """
    Gets rid of values below a threshold set by val_min
    :param reward_rate: rewardRate() object
    :param val_min: Mimimum value for bundles
    :return: reward_rate
    """
    I = np.sum(reward_rate.bundles >= val_min, axis=1) > 3
    reward_rate.bundles = reward_rate.bundles[I, :]
    reward_rate.p_A = reward_rate.p_A[I, :, :]
    reward_rate.reward_observed, reward_rate.reward_max, reward_rate.reward_min, \
    reward_rate.p_correct = reward_rate.calcRewardRates(failed_random=False)
    reward_rate.reward_observed_slope, reward_rate.reward_max_slope, reward_rate.reward_min_slope, \
        reward_rate.p_correct_slope, reward_rate.decision_time_slope = reward_rate.calcNoiseSlope()
    return reward_rate


def plotHNvsLNOfferDistributionSeparability(rel_perf,ax=None,weights=np.array([0.33, -0.1]),fontsize=13,linewidth=3,
                                            fig_dir='',fig_name='Max-Example_better_offers',fmt='eps',save_bool=False):
    """
    Plots the d' of the separation between offer distributions for both models as a function of uncertainty
    :param rel_perf: plotHNrelLN() object containing data for both models
    :param ax: matplotlib subplot axis object (default=None)
    :param weights: 2x1 vec. Weight value to plot (default=np.array([0.33, -0.1]))
    :param fontsize: float/int. (default=13)
    :param linewidth: float/int (default=3)
    :param fig_dir: str. Where to save the figure (default='')
    :param fig_name: str. name of the figure (default='Max-Example_better_offers')
    :param fmt: str. Figure format (default='eps')
    :param save_bool: True/False. Whether to save (default=False)
    :return:
    """
    if ax is None:
        fig, ax = plt.subplots()
    # Find Index of desired weights
    indx_w = (rel_perf.hn_rr.weight_range[:, 0] == weights[0]) * (rel_perf.hn_rr.weight_range[:, 1] == weights[1])
    # Get summary performance data
    perc_greater, ln_reward, hn_reward = rel_perf.getRelPerf()
    # Get the indices of performance
    bund_indxs = np.zeros((ln_reward.shape[0], ln_reward.shape[1] - 1))
    for n in range(bund_indxs.shape[1]):
        bund_indxs[:, n] = hn_reward[:, indx_w, n].flatten() > ln_reward[:, n]
    bund_not_indxs = bund_indxs < 1
    for n in np.flip(np.arange(1, bund_indxs.shape[1])):
        bund_indxs[bund_indxs[:, n] == bund_indxs[:, n - 1], n] = 0
    bund_indxs = bund_indxs.astype(bool)
    # Look at average differences of the better
    A, B = calcAB(rel_perf.hn_rr.bundles)
    indx_filter = (A != B)
    bundles, A, B = rel_perf.hn_rr.bundles[indx_filter], A[indx_filter], B[indx_filter]
    hn_bundles = bundles.copy()
    for b in range(bundles.shape[0]):
        hn_bundles[b, np.argmin(bundles[b, :2])] = 0
        hn_bundles[b, 2 + np.argmin(bundles[b, 2:])] = 0
    A_hn, B_hn = calcAB(hn_bundles)
    # Remove bundles that have the same value
    bund_indxs = bund_indxs[indx_filter, :]
    bundles_sorted = np.sort(bundles)
    argmax_dif = np.abs(np.max(bundles[:, [0, 1]], axis=1) - np.max(bundles[:, [2, 3]], axis=1))
    argmax_dif[argmax_dif == 0] = np.nan
    #Get ready to caluculate a whole lot of dprime info
    df_alt_mn = np.zeros(bund_indxs.shape[1])
    dprime_alt_mn, dprime_alt_std = df_alt_mn.copy(), df_alt_mn.copy()
    dprime_att_mn, dprime_att_std = df_alt_mn.copy(), df_alt_mn.copy()
    #Loop through to calculate and store the dprime information
    for n in range(bund_indxs.shape[1]):
        # Calculate the d prime
        dprime_alt = np.abs(
            calcDprime(A[bund_indxs[:, n]], B[bund_indxs[:, n]], np.sqrt(rel_perf.hn_rr.P['noise_sigma_range'][n])))
        dprime_att = np.abs(calcDprime(A_hn[bund_indxs[:, n]], B_hn[bund_indxs[:, n]],
                                       np.sqrt(rel_perf.hn_rr.P['noise_sigma_range'][n])))
        # Store the d prime
        dprime_alt_mn[n] = np.mean(np.abs(dprime_alt))
        dprime_att_mn[n] = np.mean(np.abs(dprime_att))
        dprime_alt_std[n] = np.std(np.abs(dprime_alt)) / np.sqrt(sum(bund_indxs[:, n]))
        dprime_att_std[n] = np.std(np.abs(dprime_att)) / np.sqrt(sum(bund_indxs[:, n]))
    #Plot it all
    x = rel_perf.hn_rr.P['noise_sigma_range'][:-1]
    ind_x = x>0.00001
    ax.errorbar(x[ind_x], dprime_alt_mn[ind_x], yerr=dprime_alt_std[ind_x], label='Linear DV Distributions', linewidth=linewidth)
    ax.errorbar(x[ind_x], dprime_att_mn[ind_x], yerr=dprime_att_std[ind_x], label='Max DV Distributions', linewidth=linewidth)
    ax.spines['right'].set_visible(False)
    ax.spines['top'].set_visible(False)
    ax.tick_params(labelsize=fontsize)
    ax.set_xlabel(r'$\sigma_{\eta_I}^2$', fontsize=fontsize + 2)
    ax.set_ylabel("d'", fontsize=fontsize + 2)
    ax.set_title('Offers HN>LN', fontsize=fontsize + 2)
    ax.legend(fontsize=fontsize)
    plt.tight_layout()
    if save_bool:
        plt.savefig(fig_dir + fig_name + '.' + fmt, format=fmt)
    else:
        return ax

def maxLikeDeviationsThreshCrossDifference(max_deviations,ax=None,fig_name='threshold_attainment_diff',thresh=0.7,
                                           fig_dir='',fontsize=12,fmt='eps',save_bool=False):
    """
    Plots histograms of the timing of threshold crossings for the HN.
    :param max_deviations: maxLikeDeviations() object
    :param ax: matplotlib subplot axis object (default=None)
    :param fig_name: str. Name of saved figure (default='threshold_attainment_diff')
    :param thresh: float. Performance difference threshold (default=0.7)
    :param fig_dir: str. Where to save the fig (default='')
    :param fontsize: float/int. (default=12)
    :param fmt: str. (default='eps')
    :param save_bool: True/False. Whether to save the figure (default=False)
    :return:
    """
    if ax is None:
        fig, ax = plt.subplots()
    A_true = np.sum(max_deviations.bundles[:, [0, 2]], axis=1)
    B_true = np.sum(max_deviations.bundles[:, [1, 3]], axis=1)

    ind_A = A_true > B_true
    ind_better = (ind_A * (max_deviations.p_A >= thresh)) + ((ind_A < 1) * (max_deviations.p_A <= (1 - thresh)))
    p_failed = max_deviations.intermed_att_discrim_pFailed.copy()
    ind_good = np.sum(p_failed > 0.05, axis=1) < 1

    ind_expect = ind_better < 1
    ind_better *= ind_good
    ind_expect *= ind_good

    bttr = abs(max_deviations.intermed_att_discrim_time_mean[ind_better, 0] - \
               max_deviations.intermed_att_discrim_time_mean[ind_better, 1]) / \
           np.sum(max_deviations.intermed_att_discrim_time_mean[ind_better, :], axis=1)

    expd = abs(max_deviations.intermed_att_discrim_time_mean[ind_expect, 0] - \
               max_deviations.intermed_att_discrim_time_mean[ind_expect, 1]) / \
           np.sum(max_deviations.intermed_att_discrim_time_mean[ind_expect, :], axis=1)

    ax.hist(bttr, label='Better Than Max-Like Performance', density=True, fc=(.4, 0, .4, 1))
    ax.hist(expd, label='Max-Like Performance', rwidth=1, density=True, fc=(0.3, 1, 0, 0.5))
    ylim = ax.get_ylim()
    ax.axvline(np.median(bttr), ymin=ylim[0], ymax=ylim[1], color=(.4, 0, .4), linestyle=':')
    ax.axvline(np.median(expd), ymin=ylim[0], ymax=ylim[1], color=(.3, 1, 0), linestyle=':')
    ax.set_xlabel('Threshold Attainment Difference (Seconds)', fontsize=fontsize + 2)
    ax.set_ylabel('Density', fontsize=fontsize + 2)
    ax.tick_params(labelsize=fontsize)
    ax.legend(fontsize=fontsize)
    if save_bool:
        plt.savefig(f"{fig_dir}{fig_name}.{fmt}", format=fmt)
    else:
        return ax


if __name__ == '__main__':

    """
    For code examples, see the jupyter lab notebook "figures.ipynb"
    """