"""

This set of functions and classes are used to simulate the hierachical and linear models from Pettine, W. W., Louie, K., 
Murray, J. D, Wang, X. J., "Hierarchical Network Model Excitatory-Inhibitory Tone Shapes Alternative Strategies for 
Different Degrees of Uncertainty in Multi-Attribute Decisions." PLoS Computational Biology, 2021

If you have questions, email warren.pettine@gmail.com.

"""

import numpy as np
import scipy as sc
from scipy.signal import find_peaks
from colorsys import hls_to_rgb
import pickle
import os
import multiprocessing as mp
import matplotlib.pyplot as plt
import matplotlib.backends.backend_pdf
import itertools
from skimage import feature
from sklearn.utils import shuffle
import copy
from glob import glob
from itertools import product, combinations, compress
import brian2  # Used for unit-checking
from fixedPointAnalysis import loadFixedPointData

brian2.prefs.codegen.target = 'numpy'  # Only need this option for debugging in pycharm


def hnParameters(w=np.array([[0.3725, -0.1137], [-0.1137, 0.3725]]), mu_0=np.array([20, 20, 0]), mu=np.array([]),
                       inhib_range=np.array([]), excit_range=np.array([]), noise_sigma_range=np.array([]),
                       w_trans=0.025, n_trials=100, sample_rate=1, bundle_vals=[], input_noise=0, int_time_shift=0.100,
                       fixed_point_fname='simulation_results/fp_table.pickle', internal_noise_sigma=0.009, truncate_trial=False,
                       discrimination_thresh=12, input_range=np.arange(0, 20, 5)):
    """
    Creates Parameter Dictionary for running the hierarchical model
    :param w: Matrix of weights for the intermediate layer (default=np.array([[0.3725, -0.1137], [-0.1137, 0.3725]]))
    :param mu_0: When varying coherence, the base level of input to each population (default=np.array([20, 20, 0]))
    :param mu: Vector specifying the exact input. If empty, model uses mu_0. (default=np.array([]))
    :param inhib_range: A vector of inhibitory weights to cycle through (default=np.array([]))
    :param excit_range: A vector of excitatory weights to cycle through (default=np.array([]))
    :param noise_sigma_range: A vector of input_noise (uncertainty) levels to cycle through (default=np.array([]))
    :param w_trans: Weight of transmission from intermediate layer population to final layer (default=w_trans)
    :param n_trials: Number of trials to simulate per run (default=100)
    :param sample_rate: Rate at which one records the data, which influences file size (default=1)
    :param bundle_vals: Vector allowing one to specify values from which the bundles will be created (default=[])
    :param input_noise: Specifies an input noise/uncertainty level (default=0)
    :param fixed_point_fname: When comparing the fixed points with function, allows one to specify the file (default=fp_table.pickle)
    :param internal_noise_sigma: The noise intrinsic to the network (default=0.009)
    :param int_time_shift:
    :param truncate_trial: Whether to end trial when decision is reached. Influences runtime (default=False)
    :param discrimination_thresh: When using the two-population deviation for decisions, Hz difference threshold (default=12)
    :param input_range: Range of inputs to simulate (default=np.arange(0, 20, 5))
    :return: P, a dictionary of parameters
    """
    P = {
        # F-I Parameters (for Abbott-Chance
        'a': 270.0 * brian2.Hz / brian2.nA,  # Hz/nAmp, parameter for F-I curve
        'b': 108.0 * brian2.Hz,  # Hz, parameter for F-I curve
        'd': 0.154 * brian2.second,  # parameter for F-I curve
        # Synaptic drive parameters
        'gamma': 0.641,  # unitless, parameter for synaptic drive equation
        'psi': 1.0,  # unitless, parameter for synaptic drive equation
        # 's_init': 0,  # Initial value of synaptic drive for all populations
        's_init': 0.1,  # Initial value of synaptic drive for all populations
        'tau_s': 60 * brian2.ms,  # mseconds, synaptic time constant
        # Weight matrix of conductances. Colums are source, rows are target. Order is 1A, 1B, 2A, 2B...
        # 'w': np.array([[ 0.3725, -0.1137,   0.025,  -0.025,       0,       0],
        #                [-0.1137,  0.3725,  -0.025,   0.025,       0,       0],
        #                [  0.025,  -0.025,  0.3725, -0.1137,       0,       0],
        #                [ -0.025,   0.025, -0.1137,  0.3725,       0,       0],
        #                [  0.025,       0,   0.025,       0,  0.3725, -0.1137],
        #                [      0,   0.025,       0,   0.025, -0.1137,  0.3725]]) * brian2.nA,
        'w': np.array([[0.3725, -0.1137, 0.0, 0.0, 0, 0],
                       [-0.1137, 0.3725, 0.0, 0.0, 0, 0],
                       [0.0, 0.0, 0.3725, -0.1137, 0, 0],
                       [0.0, 0.0, -0.1137, 0.3725, 0, 0],
                       [0.025, 0, 0.025, 0, 0.3725, -0.1137],
                       [0, 0.025, 0, 0.025, -0.1137, 0.3725]]) * brian2.nA,
        'g_ext': 0.0011 * brian2.nA / brian2.Hz,  # nAmp/Hz, synaptic coupling strength for stimulus
        'I_0': 0.3297 * brian2.nA,  # nAmp, Background input mean current
        'tau_0': 2.0 * brian2.ms,  # mseconds, Filter time constant for background noise
        'sigma': internal_noise_sigma * brian2.nA,  # nAmp, Noise amplitude
        'mu': mu * brian2.Hz,  # Hz , mu matrix used for input
        'mu_0': mu_0 * brian2.Hz,  # Hz , Stimulus input FR: area 1, area 2, area 3
        'input_noise': input_noise,  # standard deviation of noise to add to value representation
        'input_range': input_range,  # Hz, Range of input levels
        'inhib_range': inhib_range,  # When sweeping excitation and inhibition, the range of inhibitory values
        'excit_range': excit_range,  # When sweeping excitation and inhibition, the range of excitatory values
        'noise_sigma_range': noise_sigma_range,  # When varying the noise, provides the range of values.
        'stim_on': 500.0 * brian2.ms,  # mseconds, Turn stimuli on
        'stim_dur': 1000.0 * brian2.ms,  # mseconds, duration target is on screen
        'truncate_trial': truncate_trial,  # Whether to end trial when all winners have been decided
        'int_time_shift': int_time_shift * brian2.second,
        # When integrating the input, how much of a shift from decision
        'coherence': np.array([0.50, -0.50, None]),  # Coherence level of input for each population, for single session
        'coherence_range': np.array([0.15, 0.25, 0.50, 1.0]),  # When sweeping a range of coherences, specify the range
        'coh_set_type': 'full',  # 'full', 'match'. Whether to do the entire range, or only the matched values
        'f': 0.4,  # Gain term in coherence equation
        'time_step': 0.1 * brian2.ms,  # mseconds, Simulation timestep
        'sample_rate': sample_rate,  # How many time steps to sample from
        'duration': 1750.0 * brian2.ms,  # mseconds, Simulation duration
        'fr_threshold': 35 * brian2.Hz,  # Hz, the threshold for decision
        'discrimination_thresh': discrimination_thresh * brian2.Hz,  # Intermed areas, diff in FR for readout
        'n_trials': n_trials,  # Number of trials to run
        'area_latency': np.array([[0, 0, 0],  # ms delay (no units). Projecting areas in columns, receiving in rows
                                  [0, 0, 0],
                                  # If latency given for area 1/2 -> area 1/2, it will be introduced in input
                                  [0, 0, 0]]),
        'choice_method': 'action_area',  # 'threshcross', 'action_area'. Method for determining choice
        'grid_vary': 'alt',
        # 'att', 'alt', vary a single attribute across bundles, or one bundle versus another  # 'threshcross', 'action_area'. Method for determining choice
        'bundle_vals': bundle_vals,  # Gets converted to Hz. Values to use when constructing bundles.
        'fixed_point_fname': fixed_point_fname
    }
    P['w'][0:2, 0:2], P['w'][2:4, 2:4] = w * brian2.nA, w * brian2.nA
    P['w'][4, 0], P['w'][5, 1], P['w'][4, 2], P['w'][
        5, 1] = w_trans * brian2.nA, w_trans * brian2.nA, w_trans * brian2.nA, w_trans * brian2.nA
    P['area_latency'] = (P['area_latency'] / P['time_step'] * brian2.ms).astype(int)  # Convert to index
    P['n_areas'] = int(P['w'].shape[0] / 2)
    return P


def lnParameters(w=np.array([[0.3725, -0.1137], [-0.1137, 0.3725]]), mu_0=np.array([20, 20, 0]), mu=np.array([]),
                       inhib_range=np.array([]), excit_range=np.array([]), noise_sigma_range=np.array([]), n_trials=100,
                       sample_rate=1, bundle_vals=[], input_noise=0, fixed_point_fname='fp_table.pickle',
                       internal_noise_sigma=0.009, truncate_trial=False, discrimination_thresh=12,
                       input_range=np.arange(0, 20, 5)):
    """

    :param w: Matrix of network weights (default=np.array([[0.3725, -0.1137], [-0.1137, 0.3725]]))
    :param mu_0: When varying coherence, the base level of input to each population (default=np.array([20, 20, 0]))
    :param mu: Vector specifying the exact input. If empty, model uses mu_0. (default=np.array([]))
    :param inhib_range: A vector of inhibitory weights to cycle through (default=np.array([]))
    :param excit_range: A vector of excitatory weights to cycle through (default=np.array([]))
    :param noise_sigma_range: A vector of input_noise (uncertainty) levels to cycle through (default=np.array([]))
    :param n_trials: Number of trials to simulate per run (default=100)
    :param sample_rate: Rate at which one records the data, which influences file size (default=1)
    :param bundle_vals: Vector allowing one to specify values from which the bundles will be created (default=[])
    :param input_noise: Specifies an input noise/uncertainty level (default=0)
    :param fixed_point_fname: When comparing the fixed points with function, allows one to specify the file (default=fp_table.pickle)
    :param internal_noise_sigma: The noise intrinsic to the network (default=0.009)
    :param truncate_trial: Whether to end trial when decision is reached. Influences runtime (default=False)
    :param discrimination_thresh: When using the two-population deviation for decisions, Hz difference threshold (default=12)
    :param input_range: Range of inputs to simulate (default=np.arange(0, 20, 5))
    :return:
    """
    P = {
        # F-I Parameters (for Abbott-Chance
        'a': 270.0 * brian2.Hz / brian2.nA,  # Hz/nAmp, parameter for F-I curve
        'b': 108.0 * brian2.Hz,  # Hz, parameter for F-I curve
        'd': 0.154 * brian2.second,  # parameter for F-I curve
        # Synaptic drive parameters
        'gamma': 0.641,  # unitless, parameter for synaptic drive equation
        'psi': 1.0,  # unitless, parameter for synaptic drive equation
        's_init': 0.1,  # Initial value of synaptic drive for all populations
        'tau_s': 60 * brian2.ms,  # mseconds, synaptic time constant
        # Weight matrix of conductances. Colums are source, rows are target. Order is 1A, 1B, 2A, 2B...
        'w': w * brian2.nA,
        'g_ext': 0.0011 * brian2.nA / brian2.Hz,  # nAmp/Hz, synaptic coupling strength for stimulus
        'I_0': 0.3297 * brian2.nA,  # nAmp, Background input mean current
        'tau_0': 2.0 * brian2.ms,  # mseconds, Filter time constant for background noise
        'sigma': internal_noise_sigma * brian2.nA,  # nAmp, Noise amplitude
        'mu': mu * brian2.Hz,  # Hz , mu matrix used for input
        'mu_0': mu_0 * brian2.Hz,  # Hz , Stimulus input FR: input 1, input 2...
        'input_noise': input_noise,  # standard deviation of noise to add to value representation
        'input_range': input_range,  # Hz, Range of input levels
        'inhib_range': inhib_range,  # When sweeping excitation and inhibition, the range of inhibitory values
        'excit_range': excit_range,  # When sweeping excitation and inhibition, the range of excitatory values
        'noise_sigma_range': noise_sigma_range,  # When varying the noise, provides the range of values.
        'sing_area_int_method': 'linear',  # 'linear', 'power'. Specifies how to integrate the two inputs
        'stim_on': 500.0 * brian2.ms,  # mseconds, Turn stimuli on
        'stim_dur': 1000.0 * brian2.ms,  # mseconds, duration target is on screen
        'truncate_trial': truncate_trial,  # Whether to end trial when all winners have been decided
        'coherence': np.array([0.50, -0.50, None]),  # Coherence level of input for each population, for single session
        'coherence_range': np.array([0.15, 0.25, 0.50, 1.0]),  # When sweeping a range of coherences, specify the range
        'coh_set_type': 'full',  # 'full', 'match'. Whether to do the entire range, or only the matched values
        'f': 0.4,  # Gain term in coherence equation
        'time_step': 0.1 * brian2.ms,  # mseconds, Simulation timestep
        'sample_rate': sample_rate,  # How many time steps to sample from
        'duration': 2000.0 * brian2.ms,  # mseconds, Simulation duration
        'fr_threshold': 35 * brian2.Hz,  # Hz, the threshold for decision
        'discrimination_thresh': discrimination_thresh * brian2.Hz,  # Intermed areas, diff in FR for readout
        'n_trials': n_trials,  # Number of trials to run
        'area_latency': np.array([0, 0, 0]),
        # latency of stimulus input from each area, and decision back to integrator
        'choice_method': 'action_area',  # 'threshcross', 'action_area'. Method for determining choice
        'grid_vary': 'alt',
        # 'att', 'alt', vary a single attribute across bundles, or one bundle versus another  # 'threshcross', 'action_area'. Method for determining choice
        'bundle_vals': bundle_vals,  # Gets converted to Hz. Values to use when constructing bundles.
        'fixed_point_fname': fixed_point_fname
    }
    P['area_latency'] = np.round(P['area_latency'] / P['time_step'] * brian2.ms)  # Convert to index
    P['n_areas'] = int(P['w'].shape[0] / 2)
    return P


##################
# HELPER FUNCTIONS
##################

def findModes(array, height=10):
    """
    Identifies peaks in a distribution
    :param array: A vector of values from the distribution
    :param height: The height of a peak (default=10)
    :return: Vector with the peaks
    """
    """"""
    array = array[np.isnan(array) < 1]
    if len(array) < height:
        return np.nan
    hst = np.histogram(array)
    hst2 = np.pad(hst[0], (1, 1), mode='constant', constant_values=0)
    peaks, _ = find_peaks(hst2, height=height)
    return hst[1][peaks - 1]


def sigmoid(x, x0, k):
    """
    Basic sigmoid function with two parameters
    :param x: Input values
    :param x0: Sigmoid mean
    :param k: Sigmoid slope
    :return: vector of sigmoid values
    """
    y = 1 / (1 + np.exp(-k * (x - x0)))
    return y


def sigmoidFourParam(x, x0, k, a, c):
    """
    Basic sigmoid function with four parameters
    :param x: Input values
    :param x0: Sigmoid mean
    :param k: Sigmoid slope
    :param a: Asymptote
    :param c: Baseline
    :return: vector of sigmoid values
    """
    y = a / (1 + np.exp(-k * (x - x0))) + c
    return y


def eiModelFitComparison(X, y):
    """
    Performs the model comparison between divisive and linear, using F statistic. Also calculates Rsquared
    :param X: Tuple with vectors for excitatory and inhibitory weights
    :param y: Output values to which models are fit
    :return: p, f_ratio, r_sq_linear, r_sq_div
    """
    # Clean the data
    if any(np.isnan(y)):
        X[0], X[1] = X[0][np.isnan(y) < 1], X[1][np.isnan(y) < 1]
        y = y[np.isnan(y) < 1]
    # Fit the linear and divisive models
    popt, pcov = sc.optimize.curve_fit(linearFit, X, y)
    y_ft_linear = linearFit(X, popt[0], popt[1], popt[2])
    popt, pcov = sc.optimize.curve_fit(divisiveFit, X, y)
    y_ft_div = divisiveFit(X, popt[0], popt[1])
    # Calculate r_squared
    r_sq_linear = calcRsquared(y, y_ft_linear)
    r_sq_div = calcRsquared(y, y_ft_div)
    # Do the F-test
    ssq0 = ((y_ft_linear - y) ** 2).sum()
    ssq1 = ((y_ft_div - y) ** 2).sum()
    df = len(y) - 3
    f_ratio = (ssq1 - ssq0) / (ssq0 / df)
    p = 1 - sc.stats.f.cdf(f_ratio, 1, df)
    return p, f_ratio, r_sq_linear, r_sq_div


def linearFit(X, a, b, c):
    """
    Eauation for fitting a linear model to two-attribute data
    :param X: Two-dimensional vector of Input values, such that x1, x2 = X
    :param a: Contribution of attribute x1
    :param b: Contribution of attribute x2
    :param c: Intercept
    :return: Vector of function output
    """
    x1, x2 = X
    y = a * x1 + b * x2 + c
    return y


def ellipse(x, a):
    """
    Produces a unit ellipse
    :param x: Vector of input values
    :param a: Shape parameter
    :return: Vector of function output
    """
    y_out = (1 - x ** a) ** (1 / a)
    return y_out


def divisiveFit(X, a, b):
    """
    Simplified divisive model of two-attribute data
    :param X: Two-dimensional vector of Input values, such that x1, x2 = X
    :param a: Gain parameter
    :param b: Offset parameter
    :return: Vector of function output
    """
    x1, x2 = X
    y = a * (x1 / (x2 - .1)) + b
    return y


def calcRsquared(y, y_fit):
    """
    Calculates the R^2 value between observed values and fitted values
    :param y: Vector of observations
    :param y_fit: Vector of fit values
    :return:
    """
    ss_tot = np.dot(y - np.mean(y), y - np.mean(y))
    ss_res = np.dot(y - y_fit, y - y_fit)
    r_squared = 1 - (ss_res / ss_tot)
    return r_squared


def saveData(file_name, save_dict):
    """
    Save method compatible with large datasets. To load the data, use the loadData() function
    :param file_name: Location and name of file to be saved
    :param save_dict: The data to be saved. Does not have to be a dictionary, though that's how it's used
    :return: 0 if successful, 1 if not
    """
    ''''Save method used in the classes'''
    # Add extension, if necessary
    if not file_name.endswith('.pickle'):
        file_name = (file_name + '.pickle')

    # Break data up and save it
    max_bytes = 2 ** 31 - 1
    bytes_out = pickle.dumps(save_dict)
    try:
        with open(file_name, 'wb') as f_out:
            for idx in range(0, len(bytes_out), max_bytes):
              f_out.write(bytes_out[idx:idx + max_bytes])
    except:
        return 1
    return 0


def loadData(file_name):
    """
    Load data saved using saveData(). Useful for large datasets. Designed for use with dictionaries
    :param file_name: The file path and name
    :return: The loaded dictionary
    """
    '''Load method used in the classes'''
    # Add extension, if necessary
    if not file_name.endswith('.pickle'):
        file_name = (file_name + '.pickle')

    # Check to see if file exists
    if not os.path.isfile(file_name):
        raise ValueError('file ' + file_name + ' does not exist in workspace')

    # Rebuild it
    max_bytes = 2 ** 31 - 1
    bytes_in = bytearray(0)
    input_size = os.path.getsize(file_name)
    with open(file_name, 'rb') as f_in:
        for _ in range(0, input_size, max_bytes):
            bytes_in += f_in.read(max_bytes)
    save_dict = pickle.loads(bytes_in)
    return save_dict


def convertInputSV(vals, exponent=1):
    """
    Converts a linear scale to an exponential with the same minimum and maximum value as the original
    :param vals: Vector of input values
    :param exponent: Exponent parameter to which one raises the data (default=1)
    :return: vector of transformed values
    """
    """
    """
    convert = np.linspace(0, 1, len(vals)) ** exponent
    vals_nonlin = convert * (np.max(vals) - np.min(vals)) + np.min(vals)
    return vals_nonlin


def makeBundles(vals, hard=False, df=4):
    """
    Creates bundles based on all combinations of values. Can be modulated to only include similar bundles.
    :param vals: Vector of values from which to draw combinations
    :param hard: whther to restrict to bundles that are close (default=False)
    :param df: If hard=True, the difference in bundle values to restrict (default=4)
    :return: Bundles matrix. Rows are trials. Columns are (A1, A2, B1, B2), where A/B are attributes and 1/2 alternatives
    """
    """Create bundles from a given set of values"""
    bundles = np.array(list(product(vals, repeat=2)))
    indx = np.arange(0, len(bundles), 1)
    indx_2 = np.array(list(combinations(indx, 2)))
    bundles = np.concatenate((bundles[indx_2[:, 0], :], bundles[indx_2[:, 1], :]), axis=1)
    if hard:
        bundle_val = np.concatenate((np.sum(bundles[:, [0, 2]], axis=1).reshape(len(bundles), 1),
                                     np.sum(bundles[:, [1, 3]], axis=1).reshape(len(bundles), 1)), axis=1)
        I_bund = abs(bundle_val[:, 0] - bundle_val[:, 1]) <= df
        I_att = (abs(bundles[:, 0] - bundles[:, 2]) <= df) * (abs(bundles[:, 1] - bundles[:, 3]) <= df)
        I_neq = abs(bundle_val[:, 0] - bundle_val[:, 1]) > 0.0001
        I = I_bund * I_att * I_neq
        bundles = bundles[I, :]
    return bundles


def quadrantBundles(A=[], B=[], step=0.5, ref=15):
    """
    Creates bundles, but restricts them to a single quadrant of the indifference curve
    :param A: Vector of values for attribute A (default=[])
    :param B: Vector of values for attribute B (default=[])
    :param step: If no values specified, the step between values created (default=0.5)
    :param ref: The reference value for the indifference curve (default=15)
    :return: Bundles matrix. Rows are trials. Columns are (A1, A2, B1, B2), where A/B are attributes and 1/2 alternatives
    """
    if len(A) == 0: A = np.arange(ref, 20, step)
    if len(B) == 0: B = np.arange(10, ref, step)
    bunds = np.array(list(product(A, B)))
    # Shuffle A and B
    bunds2 = bunds.copy()
    I = np.random.rand(bunds.shape[0]) < .5
    bunds2[I, 0] = bunds[I, 1]
    bunds2[I, 1] = bunds[I, 0]
    # Shuffle side for reference
    I = np.random.rand(bunds.shape[0]) < .5
    bundles = np.ones((bunds.shape[0], 4)) * ref
    bundles[I, 0], bundles[I, 2] = bunds2[I, 0], bunds2[I, 1]
    bundles[I < 1, 1], bundles[I < 1, 3] = bunds2[I < 1, 0], bunds2[I < 1, 1]
    # Remove equal values
    I = np.sum(bundles[:, [0, 2]], axis=1) != np.sum(bundles[:, [1, 3]], axis=1)
    bundles = bundles[I, :]
    # One last shuffle of order
    I = shuffle(np.arange(0, bundles.shape[0]))
    bundles = bundles[I, :]
    return bundles


def maxDeviationBundles(vals=None):
    """
    Creates a set of bundles where the linear integration model and max model would choose differently
    :param vals: Vector of values from which to create the bundles (default=None)
    :return:
    """
    if vals is None:
        vals = np.arange(10, 21, 1)
    # Make the bundles
    bundles = makeBundles(vals=vals)
    A_true = np.sum(bundles[:, [0, 2]], axis=1)
    B_true = np.sum(bundles[:, [1, 3]], axis=1)
    # Determine linear choices and max choices
    A_correct = A_true > B_true
    max_decisions = np.argmax(bundles, axis=1)
    max_decisions[max_decisions == 2] = 0
    max_decisions[max_decisions == 3] = 1
    # Index where they conflict
    cnflt_ind = (A_correct == max_decisions)
    equal_ind = A_true != B_true
    select_ind = (cnflt_ind * equal_ind) > 0
    bundles = bundles[select_ind, :]
    # Give back to the people
    return bundles


def rotate(x, y, theta=np.pi / 4):
    """
    Rotates a set of two dimensional coordinates around the axis.
    :param x: Original vector of x coordinates
    :param y: Original vector of y coordinates
    :param theta: Amount to rotate (default=np.pi/4)
    :return: x, y rotated
    """
    R = np.array([[np.cos(theta), -1 * np.sin(theta)], [np.sin(theta), np.cos(theta)]])
    M = np.concatenate((x.flatten().reshape(1, len(x)), y.flatten().reshape(1, len(y))), axis=0)
    M_prime = np.dot(R, M)
    return M_prime[0, :].flatten(), M_prime[1, :].flatten()


def argSortMatRows(mat):
    """
    Creates a standard way of sorting bundle matrices based on how they were initially created
    :param mat: The matrix of bundles
    :return: array, sorting index
    """
    # Sort using collective value of rows
    indx = np.zeros(mat.shape[0])
    for c in range(mat.shape[1]):
        indx = ((((indx + mat[:, c - 1]) * mat[:, c]) - mat[:, c - 2]) * mat[:, c - 3]) + mat[:, c - 4]
    indx_bunds = np.argsort(indx)
    return indx_bunds


def getPool():
    """ Produces the parallel pool, both on cluster running Slurm and laptop """
    on_cluster = os.getenv("SLURM_JOB_ID")
    if on_cluster:
        print('Obtaining number of cores using os.getenv')
        num_workers = int(os.getenv("SLURM_CPUS_PER_TASK"))
    else:
        print('Obtaining number of cores using mp.cpu_count')
        num_workers = mp.cpu_count()
    pool = mp.Pool(num_workers)
    return pool


def colors(n):
    """
    Create distinct colors for plotting
    :param n: parameter for the number of colors needed.
    :return: matrix of color values
    """
    colors = []
    for i in np.arange(0., 360., 360. / n):
        h = i / 360.
        l = (50 + np.random.rand() * 10) / 100.
        s = (90 + np.random.rand() * 10) / 100.
        colors.append(hls_to_rgb(h, l, s))
    return colors


def getCoherenceSets(obj, type='full'):
    """
    Create the pairs of coherence values that cover all range of combinations.
    :param obj: Model Object
    :param type: 'full' or 'match'. Specifies if full range of combination, or only matched coherence
    :return: Matrix of coherence levels
    """
    if type == 'full':
        A = list(itertools.combinations_with_replacement(obj.P['coherence_range'], 2))
        B = list(itertools.combinations(obj.P['coherence_range'], 2))
        A, B = np.array(A), np.array(B)
        B[:, 0], B[:, 1] = B[:, 1].copy(), B[:, 0].copy()
        coherence_sets = np.vstack((A, B))
    elif type == 'match':
        coherence_sets = np.array([obj.P['coherence_range'], obj.P['coherence_range']]).transpose()
    else:
        raise ValueError('Invalid value for "type." Must be "full" or "matched"')
    return coherence_sets


def getPA(winners, n_trials, decision_time):
    """
    Computes the proportion of trials where "A" was chosen. Corrects for no decisions and other contingencies.
    :param winners: Array indicating the winning population
    :param n_trials: Total number of trials.
    :param decision_time: Times at which a decision was reached (makes sure there was one)
    :return: p_A. The proportion of trials "A" chosen
    """
    num = (winners[np.invert(np.isnan(winners))] > 0).sum()  # Get rid of warning by dropping nans
    denom = (n_trials - sum(np.isnan(decision_time.flatten())))
    if denom < 0.01:
        p_A = np.nan
    else:
        try:
            p_A = num / denom
        except:
            p_A = np.nan
    return p_A


def findWinning(trials):
    """
    Convenience function that produces a number of summary variables for the hierarchical model
    :param trials: Simulation object for hierarchical model
    :return: p_A, winning_pop, winners, decision_time
    """
    if trials.P['choice_method'] == 'threshcross':
        decision_time = np.nanmin(trials.thresh_cross_time, axis=0)
        winning_pop = np.nanargmin(trials.thresh_cross_time, axis=0)
        winners = np.array([trials.winners[0], trials.winners[2], trials.winners[4]])
        w_ind = (tuple(winning_pop), tuple(range(winning_pop.shape[0])))
        winners = winners[w_ind]  # Using this method of extracting winners, 0=A and 1=B
    elif trials.P['choice_method'] == 'action_area':
        decision_time = trials.thresh_cross_time[2, :]
        # winners = (trials.winners[4][np.isnan(trials.winners[4]) < 1] < 1).astype(int) #Winners on completed trials
        winners = (trials.winners[4] < 1).astype(float)
        winners[np.isnan(trials.winners[4])] = np.nan
        winning_pop = None
    else:
        raise ValueError('Invalid choice_method specified')
    if (trials.P['n_trials'] - sum(np.isnan(decision_time.flatten()))) < 5:
        p_A = np.nan
    else:
        p_A = np.nanmean(winners < 1)
    return p_A, winning_pop, winners, decision_time


def calcIntermedReadout(trial):
    """
    Coverts data from the trial simulation object to aid in readout from intermediate layer areas.
    :param trial: Simulation object
    :return: intermed_readout
    """
    intermed_readout = trial.discrimination_pop.copy()
    intermed_readout[intermed_readout == 3] = 1
    intermed_readout[intermed_readout == 2] = 0
    return intermed_readout


def calcIntermedLayerGains(trial, winners=None):
    """
    Calculates gains by intermediate layers (compared to final layer), and decodability. Compares ALL POPULATIONS
    :param trial: Trial object
    :param winners: Vector of winners (to save computation time). Can be obtained from Trial object.
    :return: intermed_time_gain, intermed_agree, dt_intermed_readout, p_intermed_decision, intermed_pA
    """
    """"""
    if winners is None:
        _, _, winners, _ = findWinning(trial)
    n_complete_trials = sum(np.isnan(trial.thresh_cross_time[2, :]) < 1)
    # Determine intermediate-layer with highest value at time of decision
    dt_intermed_readout = np.argmax(trial.decision_snapshot[:4, 1, :], axis=0)
    dt_intermed_readout[dt_intermed_readout == 2] = 0
    dt_intermed_readout[dt_intermed_readout == 3] = 1
    dt_intermed_readout = np.sum(winners == dt_intermed_readout) / n_complete_trials
    # Determine time gained through reading out from intermediate layer area
    intermed_time_gain = trial.thresh_cross_time[-1, :] - trial.discrimination_time
    intermed_time_gain = intermed_time_gain[np.isnan(intermed_time_gain) < 1]
    intermed_time_gain = np.nanmean(intermed_time_gain[intermed_time_gain > 0])
    # Readout info
    intermed_readout = calcIntermedReadout(trial)
    intermed_agree = np.sum(winners == intermed_readout) / n_complete_trials
    intermed_pA = np.mean(intermed_readout[np.isnan(intermed_readout) < 1] < 1)
    p_intermed_decision = np.mean(np.isnan(trial.discrimination_time) < 1)
    return intermed_time_gain, intermed_agree, dt_intermed_readout, p_intermed_decision, intermed_pA


def calcIntermediateAttWinners(trial):
    """
    Calculates winners from intermediate layers, reading out from the attributes separately
    :param trial: Trial simulation object
    :return: winners
    """
    winners = np.zeros(trial.P['n_trials'])
    nan_inds = np.sum(np.isnan(trial.att_discrimination_time), axis=0) > 1
    winners[nan_inds] = np.nan
    if sum(nan_inds) == trial.P['n_trials']:
        return winners
    inds_winners = np.nanargmin(trial.att_discrimination_time[:, nan_inds < 1], axis=0)
    winners[nan_inds < 1] = trial.att_discrimination_pop[:, nan_inds < 1][inds_winners,
                                  np.arange(trial.att_discrimination_pop[:,nan_inds < 1].shape[1])]
    return winners


def calcIntermediateAttStats(trial, winners=None):
    """
    Calculates gains by intermediate layers (compared to final layer), and decodability. Compares WITHIN ATTRIBUTES
    :param trial: Trial object
    :param winners: Vector of winners (to save computation time). Can be obtained from Trial object.
    :return: intermed_att_time_gain, intermed_att_agree, intermed_att_pA, p_intermed_att_decision
    """
    if winners is None:
        _, _, winners, _ = findWinning(trial)
    n_complete_trials = sum(np.isnan(trial.thresh_cross_time[2, :]) < 1)
    # Determine time gained through reading out from intermediate layer area
    intermed_att_time_gain = trial.thresh_cross_time[-1, :] - np.nanmin(trial.att_discrimination_time, axis=0)
    intermed_att_time_gain = intermed_att_time_gain[np.isnan(intermed_att_time_gain) < 1]
    intermed_att_time_gain = np.nanmean(intermed_att_time_gain[intermed_att_time_gain > 0])
    # Readout info
    intermed_att_readout = calcIntermediateAttWinners(trial)
    intermed_att_agree = np.sum(winners == intermed_att_readout) / n_complete_trials
    intermed_att_pA = np.mean(intermed_att_readout[np.isnan(intermed_att_readout) < 1] < 1)
    p_intermed_att_decision = 1 - np.mean(np.isnan(intermed_att_readout))
    return intermed_att_time_gain, intermed_att_agree, intermed_att_pA, p_intermed_att_decision


class checkStability():
    """
    Class to check parameter weight matrix for stability
    """
    def __init__(self, P, rq=15):
        """
        Initiation method
        :param P: Parameter dictionary
        :param rq: Threshold for instability (default=15)
        """
        self.P = P
        self.N = self.P['n_areas'] * 2
        self.s = np.zeros((self.N))
        t_end = 6
        self.s10 = self.P['tau_s'] * self.P['gamma'] * rq / ((1 * brian2.ms) + self.P['tau_s'] * self.P['gamma'] * rq)
        self.Ntstop = int(t_end / self.P['time_step'])

    def test(self, noise=False):
        """
        Main call to run test.
        :param noise: True/False. Whether noise is included (default=False)
        :return: 1 (unstable), 0 (stable)
        """
        s = 0. * np.ones(self.N)
        I_ext = self.P['I_0'] * np.ones(self.N)
        I_noise = 0. * np.ones(self.N) * brian2.nA
        s_noise = np.exp(-self.P['time_step'] / self.P['tau_0'])
        d_noise = np.sqrt(0.5 * (1. - s_noise * s_noise))

        # Loop through timesteps
        for it in range(self.Ntstop):
            if noise == True:
                I_noise = self.noise(I_noise, s_noise, d_noise)
            s, fr = self.timestep(s, I_noise + I_ext)
        self.s = s
        key = ['no noise', 'noise']
        if any(s > self.s10):
            print("Resting state unstable, " + key[noise])
            return 1
        else:
            print("Stable, " + key[noise])
            return 0

    def noise(self, In, s, d):
        """
        Adds noise to current
        :param In: Existing noise current
        :param s: Timestep
        :param d: Integration constant
        :return: z. New noise current
        """
        z = In * s + d * self.P['sigma'] * np.random.standard_normal(In.shape)
        return z

    def timestep(self, x, In):
        """
        Incrementing function called at each timestep.
        :param x: S variable
        :param In: Current
        :return: z (s variable), r (FR)
        """
        Is = In + np.dot(self.P['w'], x)
        r = self.inputOutput(Is)
        z = x + (self.P['time_step'] / self.P['tau_s']) * (-x + self.P['tau_s'] * self.P['gamma'] * r * (1. - x))
        return z, r

    def inputOutput(self, I):
        """
        Transfer function for firing rate
        :param I: Current
        :return: r (firing rate)
        """
        r = (self.P['a'] * I - self.P['b']) / (1 - np.exp(-self.P['d'] * (self.P['a'] * I - self.P['b'])))
        return r


#########################
# MAIN SIMULATION CLASSES
#########################

class decisionTrials():
    """
    This is the primary class for running simulations. It is the core of the entire model.
    """
    """ Primary class to run the simulation """

    def __init__(self, P):
        """
        Initialiazation method
        :param P: Parameter dictionary produced by either the hnParameters() or lnParameters() functions
        """
        self.P = P
        if len(self.P['mu']) == 0:
            self.P['mu'] = self.makeMuArray()
        # Simulation variables
        self.mu_noise = np.random.normal(0, self.P['input_noise'],
                                         (self.P['n_areas'] * 2, self.P['n_trials'])) * brian2.Hz
        self.nSteps = int(P['duration'] / P['time_step'])
        n_samples = max(1,
                        round(self.nSteps / P['sample_rate']))  # If sample rate is lower than nsteps, just sample once
        self.R = np.zeros((self.P['n_areas'] * 2, n_samples, self.P['n_trials'])) * brian2.Hz  # Record of firing rates
        self.S = np.ones((self.P['n_areas'] * 2, n_samples, self.P['n_trials'])) * P[
            's_init']  # Record of synaptic currents
        self.I = np.zeros(
            (self.P['n_areas'] * 2, n_samples, self.P['n_trials'])) * brian2.nA  # Record of overall current
        self.Ib = np.zeros(
            (self.P['n_areas'] * 2, n_samples, self.P['n_trials'])) * brian2.nA  # Record of noise terms current
        self.stim_inputs = np.zeros(
            (self.P['n_areas'] * 2, n_samples, self.P['n_trials'])) * brian2.Hz  # Record of stimulus currents
        self.thresh_cross_time = np.zeros(
            (self.P['n_areas'], self.P['n_trials'])) * np.nan  # reaction time relative to stimulus
        self.winners = np.zeros((self.P['n_areas'] * 2, self.P['n_trials'])) * np.nan  # record of winners
        self.discrimination_time = np.zeros(self.P[
                                                'n_trials']) * np.nan  # Time at which one could readout from the intermediate layer, based on discrimination
        self.discrimination_pop = np.zeros(
            self.P['n_trials']) * np.nan  # Intemediate layer population that passes threshold
        self.att_discrimination_time = np.zeros((self.P['n_areas'] - 1, self.P[
            'n_trials'])) * np.nan  # Time the comp is resolved for each att using difference
        self.att_discrimination_pop = np.zeros(
            (self.P['n_areas'] - 1, self.P['n_trials'])) * np.nan  # Alternative for each att that wins using difference
        self.decision_snapshot = np.full([self.P['n_areas'] * 2, 2, self.P['n_trials']],
                                         np.nan)  # (I,S) record of activity when decision is made
        # Determine if a latency will be introduced in the projections
        self.projection_latency = False
        if self.P['n_areas'] == 3:
            if sum(self.P['area_latency'][2, :2]) > 0: self.projection_latency = True
            self.stability_time = np.full([self.P['n_areas'] - 1, self.P['n_trials']], np.nan)
            fp_X, fp_Y = self.getFP()
            self.fp = {'X': fp_X, 'Y': fp_Y}
            if len(fp_X) < 1: self.stability_time[0, :] = -1
            if len(fp_Y) < 1: self.stability_time[1, :] = -1
        else:
            self.stability_time = np.full([self.P['n_trials']], np.nan)
            self.fp = self.getFP()
            if len(self.fp) < 1: self.stability_time[:] = -1

    def getFP(self):
        """
        Loads possible fixed points using a pre-generated file whose location is specified in P['fixed_point_fname']
        :return: fp (list of fixed points per area)
        """
        """ obtain the possible fixed points for the system """
        try:
            params, fixed_points, stability, n_steps, traj_fp = loadFixedPointData(f_name=self.P['fixed_point_fname'])
        except:
            params, fixed_points, stability = pickle.load(open(self.P['fixed_point_fname'], "rb"), encoding='latin1')
        I_w = (np.abs((params[:, 2] * brian2.nA) - self.P['w'][0, 0]) < (0.000001 * brian2.nA)) * \
              (np.abs((params[:, 3] * brian2.nA) - np.abs(self.P['w'][1, 0])) < (0.000001 * brian2.nA))
        if self.P['n_areas'] == 3:
            I_mu_X = (np.abs(params[:, 0] - brian2.asarray(self.P['mu'][0])) < 0.01) * \
                     (np.abs(params[:, 1] - brian2.asarray(self.P['mu'][1])) < 0.01)
            I_mu_Y = (np.abs(params[:, 0] - brian2.asarray(self.P['mu'][2])) < 0.01) * \
                     (np.abs(params[:, 1] - brian2.asarray(self.P['mu'][3])) < 0.01)
            fp_X = self.findFP(I_mu=I_mu_X, I_w=I_w, fixed_points=fixed_points, stability=stability)
            fp_Y = self.findFP(I_mu=I_mu_Y, I_w=I_w, fixed_points=fixed_points, stability=stability)
            return fp_X, fp_Y
        else:
            I_mu = (np.abs(params[:, 0] - np.mean(brian2.asarray(self.P['mu'][[0, 2]]))) < 0.01) * \
                   (np.abs(params[:, 1] - np.mean(brian2.asarray(self.P['mu'][[1, 3]]))) < 0.01)
            fp = self.findFP(I_mu=I_mu, I_w=I_w, fixed_points=fixed_points, stability=stability)
            return fp

    def findFP(self, I_mu, I_w, fixed_points, stability):
        """
        Helper function that extracts fixed points
        :param I_mu: Input currents
        :param I_w: Recurrent weights
        :param fixed_points: list of fixed points
        :param stability: list classifying fixed point stability
        :return: fp (list of fixed points)
        """
        I = (I_mu * I_w) * np.arange(0, len(I_w), 1)
        if sum(I) < 1: return np.array([])
        stable_fp_I = [i for i in range(len(stability[max(I)])) if stability[max(I)][i] == 's']
        fp_list = [fixed_points[max(I)][i] for i in stable_fp_I]
        fp = np.zeros((len(fp_list), 2))
        for i in range(len(fp_list)): fp[i, 0], fp[i, 1] = fp_list[i]['s1'], fp_list[i]['s2']
        return fp

    def makeMuArray(self):
        """
        Creates a mu array, using coherence levels and mu_0
        :return: mu
        """
        mu = np.array([[]])
        for a in range(self.P['coherence'].shape[0]):
            if self.P['coherence'][a] is None:
                mu = np.append(mu, np.array([0, 0]))
            else:
                mu = np.append(mu, np.array([self.P['mu_0'][a] * (1 + self.P['f'] * self.P['coherence'][a]),
                                             self.P['mu_0'][a] * (1 - self.P['f'] * self.P['coherence'][a])]))
        mu = mu.reshape(self.P['coherence'].shape[0] * 2, 1) * brian2.Hz
        return mu

    def makeSdriveLatencyMatrix(self, t):
        """
        Creates a new synaptic drive matrix to use when latencies are introduced
        :param t: latency (ms)
        :return: S
        """
        S = np.zeros((self.P['n_areas'] * 2, self.P['n_trials'], self.P['n_areas'] * 2))
        a1 = 0
        # Loop through all the projecting populations/areas
        for j in range(self.P['n_areas'] * 2):
            a2 = 0
            # Loop through the target populations/areas
            for k in range(self.P['n_areas'] * 2):
                # Set the S variable to the appropriate time offset value
                S[j, :, k] = self.S[j, t - 1 - self.P['area_latency'][a2, a1], :]
                a2 += k % 2
            a1 += j % 2
        return S

    def applySdriveLatencyMatrix(self, S):
        """
        Applies the weight matrix to the S matrix in each area.
        :param S: S matrix
        :return: wS
        """
        """ Helper for the current function """
        wS = np.zeros((self.P['n_areas'] * 2, self.P['n_trials']))
        # Loop through the projecting areas
        for a in range(self.P['n_areas']):
            # Calculate the wS and apply it in the appropriate location
            temp = np.dot(self.P['w'], S[:, :, a * 2])
            wS[a * 2:a * 2 + 2, :] = temp[a * 2:a * 2 + 2, :]
        return wS * brian2.amp  # Cleanup the units

    def inputOutput(self, I):
        """
        Return firing rate as a function of the input current using Abbot-Chance
        :param I: Input current
        :return: r
        """
        r = (self.P['a'] * I - self.P['b']) / (1 - np.exp(-self.P['d'] * (self.P['a'] * I - self.P['b'])))
        return r

    def synapticDrive(self, I, s):
        """
        Calculates synaptic drive based on current
        :param I: Current
        :param s: synaptic drive at last time step
        :return: s
        """
        ds = self.P['psi'] * (self.inputOutput(I) * self.P['gamma'] * (1 - s) - s / self.P['tau_s'])
        s = s + ds * self.P['time_step']
        return s

    # Return current for input
    def current(self, Ib, S, mu):
        """
        Computes the current at that timestep
        :param Ib: Background noise current
        :param S: Synaptic drive
        :param mu: input current
        :return: I
        """
        if self.projection_latency:
            wS = self.applySdriveLatencyMatrix(S)
        else:
            wS = np.dot(self.P['w'], S)
        mu = np.tile(mu, (1, self.P['n_trials'])) + self.mu_noise
        I = wS + Ib + self.P['g_ext'] * mu
        return I

    def noiseComponent(self, Ib):
        """
        Return noise component, as described by the Ornstein-Uhlenbeck process
        :param Ib: Noise component at prior timestep
        :return: Ib
        """
        dIb = (self.P['time_step'] * (-(Ib - self.P['I_0'])) + (np.sqrt(self.P['time_step']) *
                    np.random.normal(size=[self.P['n_areas'] * 2,self.P['n_trials']]) * np.sqrt(
                    self.P['tau_0'] * (self.P['sigma'] ** 2)))) / self.P['tau_0']
        Ib = Ib + dIb
        return Ib

    def integratorUnit(self, mu_in):
        """
        Used in the single area network to integrate the input (either linear or with power law)
        :param mu_in: Input bundles
        :return: integrated input
        """
        mu = np.zeros((self.P['n_areas'] * 2, 1))
        if self.P['sing_area_int_method'] == 'linear':
            # Average together the inputs
            mu[0], mu[1] = mu_in[0::2].sum() / (len(mu_in) / 2), mu_in[1::2].sum() / (len(mu_in) / 2)
        elif self.P['sing_area_int_method'] == 'power':
            mu[0] = (np.sqrt(mu_in[0]) / 2 + np.sqrt(mu_in[2]) / 2) ** 2
            mu[1] = (np.sqrt(mu_in[1]) / 2 + np.sqrt(mu_in[3]) / 2) ** 2
        else:
            raise ValueError('"linear", and "power" are  presently the only input integration methods')
        return mu * brian2.Hz

    def updateMu(self, t, s_on, s_off):
        """
        Updates the input array based on latency
        :param t: current time step
        :param s_on: timestep where input is on
        :param s_off: timestep where input is off
        :return: mu
        """
        if self.P['n_areas'] == 1:
            lat_1, lat_2 = self.P['area_latency'][0], self.P['area_latency'][1]
        else:
            lat_1, lat_2 = self.P['area_latency'][0][0], self.P['area_latency'][0][1]
        mu = np.zeros((self.P['mu'].shape[0], 1)) * brian2.Hz
        # Check each input independently, taking into account latency
        if (t - lat_1 >= s_on) & (t - lat_1 <= s_off):  # Input latency to area 1
            mu[:2] = self.P['mu'][:2]
        if (t - lat_2 >= s_on) & (t - lat_2 <= s_off):  # Input latency to areas 2 and 3
            mu[2:] = self.P['mu'][2:]
        if self.P['n_areas'] == 1:
            if self.P['choice_method'] == 'action_area': mu = mu[:4]  # Kludge to correct for the averaging
            mu = self.integratorUnit(mu)  # Send it to the integrator unit
        return mu

    def runTrials(self):
        """
        Primary function for simulating trials
        :return:
        """
        S = np.ones((self.P['n_areas'] * 2, self.P['n_trials'])) * self.P['s_init']
        Ib = np.ones((self.P['n_areas'] * 2, self.P['n_trials'])) * self.P['I_0']
        s_on = int(self.P['stim_on'] / self.P['time_step'])  # time the stimuli turn on
        s_off = s_on + int(self.P['stim_dur'] / self.P['time_step'])
        # Tracking variables
        decision_time = 0
        sample = 0
        # Loop through time steps
        for t in range(0, self.nSteps):

            # update decision time
            decision_time += self.P['time_step']

            # Turn stimuli on or off (delay implemented here for single area)
            mu = self.updateMu(t=t, s_on=s_on, s_off=s_off)

            # Check if latency needs to be added to synaptic drive for three area model (single area is implemented in mu)
            if self.projection_latency:
                S_input = self.makeSdriveLatencyMatrix(t=t)
            else:
                S_input = S

            # Calculate new values for the trial
            Ib = self.noiseComponent(Ib)
            I = self.current(Ib, S_input, mu)
            S = self.synapticDrive(I, S)
            R = self.inputOutput(I)

            # Check for winner and stability after stimulus turns on
            if (t > s_on) and (t < s_off):
                self.checkWinnerRT(t, I, S, R)
                # self.checkStableFP(t, S)  # TOGGLE BASED ON IF YOU WANT IT TO STOP WHEN FP REACHED
                if self.P['n_areas'] > 1:
                    # self.checkDiscrimination(t, R) TOGGLE BASED ON IF YOU WANT IT TO STOP WHEN THRESHOLD REACHED
                    self.checkAttComp(t, R)

            # Record  values
            if np.mod(t + 1, self.P['sample_rate']) == 0:
                self.stim_inputs[:, sample, :] = mu  # Stimulus currents
                self.R[:, sample, :] = R  # Population firing rates
                self.I[:, sample, :] = I  # Input currents
                self.S[:, sample, :] = S  # Synaptic drive
                self.Ib[:, sample, :] = Ib  # Noise term
                sample += 1

            # End loop if winner has been reached for all trials
            if self.P['truncate_trial']:
                if (np.isnan(self.winners[-1, :]) < 1).all():
                    break

    def checkDiscrimination(self, t, R):
        """
        Determines which/when an intermediate layer population passes the discrimination threshold
        :param t: timestep
        :param R: Firing rate matrix
        :return:
        """
        check_inds_bool = np.isnan(self.discrimination_pop)
        if check_inds_bool.any() < 1:  # If none are left, get out of here
            return
        n_alts = self.P['n_areas'] - 1
        pop_mx_R = np.zeros((n_alts, sum(check_inds_bool)))
        pop_mx_id = pop_mx_R.copy()
        pops = np.arange(0, len(self.R[:-2, 0, 0]))  # Popopulations to index
        for a in range(n_alts):
            pop_mx_R[a, :] = np.max(R[:-2, check_inds_bool][a::n_alts, :], axis=0)
            pop_mx_id[a, :] = pops[a::n_alts][np.argmax(R[:-2, check_inds_bool][a::n_alts, :], axis=0)]
        df = np.max(pop_mx_R, axis=0) - pop_mx_R
        thrsh = (df * brian2.Hz >= self.P['discrimination_thresh'])
        cross_inds = np.sum(thrsh, axis=0) >= (n_alts - 1)
        if sum(cross_inds) > 0:
            cross_pops = np.argmin(thrsh[:, cross_inds], axis=0)
            I = np.arange(0, self.P['n_trials'])[check_inds_bool][cross_inds].astype(int)
            self.discrimination_pop[I] = pop_mx_id[:, cross_inds][cross_pops, np.arange(len(I))]  # Store the population
            self.discrimination_time[I] = t * self.P['time_step']  # Store the threshold crossing time

    def checkAttComp(self, t, R):
        """
        Determines within-attribute winner using discrimination threshold
        :param t: Timestep
        :param R: Firing rate matrix
        :return:
        """
        check_inds_bool = np.isnan(self.att_discrimination_pop)
        if check_inds_bool.any() < 1:  # If none are left, get out of here
            return
        pop_inds = np.arange(0, (self.P['n_areas'] - 1) * 2, 2)
        for p in range(self.P['n_areas'] - 1):
            cross_inds = np.abs(R[pop_inds[p], check_inds_bool[p, :]] - R[pop_inds[p] + 1, check_inds_bool[p, :]]) >= \
                         self.P['discrimination_thresh']
            if sum(cross_inds) > 0:  # Only keep going if something has crossed, get out of here
                I = np.arange(0, self.P['n_trials'])[check_inds_bool[p, :]][cross_inds].astype(int)
                self.att_discrimination_pop[p, I] = np.argmax((R[pop_inds[p], I], R[pop_inds[p] + 1, I]), axis=0)
                self.att_discrimination_time[p, I] = t * self.P['time_step']  # Store the threshold crossing time

    def checkWinnerRT(self, t, I, S, R):
        """
        Checks for the winner and RT on each timestep
        :param t: timestep
        :param I: Current
        :param S: Synaptic drive
        :param R: Firing Rate
        :return:
        """
        # Only look at values that haven't crossed yet (fun with indices!)
        check_inds_bool = np.isnan(self.thresh_cross_time)
        if check_inds_bool.any() < 1:  # If none are left, get out of here
            return
        pop_inds = np.arange(0, self.P['n_areas'] * 2, 2)
        for p in range(self.P['n_areas']):
            update_inds = np.arange(0, self.P['n_trials'], 1)[check_inds_bool[p, :]]
            passed_thresh_inds = R[pop_inds[p]:pop_inds[p] + 2, update_inds] > self.P['fr_threshold']
            update_inds = update_inds[passed_thresh_inds.sum(axis=0) > 0]
            # Take care of the decision snapshot indices
            self.thresh_cross_time[p, update_inds] = t * self.P['time_step']  # Store the threshold crossing time
            winners = np.tile(np.array([1, 2]).reshape(2, 1), (1, passed_thresh_inds.shape[1])) * passed_thresh_inds
            winners = winners[:, passed_thresh_inds.sum(axis=0) > 0]
            self.winners[pop_inds[p]:pop_inds[p] + 2, update_inds] = winners  # Store the winners
        # update record
        self.decision_snapshot[:, 0, update_inds] = I[:, update_inds]
        self.decision_snapshot[:, 1, update_inds] = S[:, update_inds]

    def checkStableFP(self, t, S):
        """
        Determine when the system reaches the stable fixed points
        :param t: timestep
        :param S: Synaptic drive matrix
        :return:
        """
        check_inds_bool = np.isnan(self.stability_time)
        err = 0.005
        if self.P['n_areas'] == 3:
            check_inds_int = np.tile(np.arange(0, self.P['n_trials'], 1),
                                     (self.stability_time.shape[0], 1)) * check_inds_bool
            pop_inds, dic_indx = np.arange(0, 4, 2), ['X', 'Y']
            for p in range(self.stability_time.shape[0]):
                update_inds, stable_inds = check_inds_int[p, :], np.array([])
                for s in range(self.fp[dic_indx[p]].shape[0]):
                    I_A_bool = abs(S[pop_inds[p], update_inds] - self.fp[dic_indx[p]][s, 0]) < err
                    I_B_bool = abs(S[pop_inds[p] + 1, update_inds] - self.fp[dic_indx[p]][s, 1]) < err
                    stable_inds = np.concatenate((stable_inds, update_inds[I_A_bool * I_B_bool]))
                self.stability_time[pop_inds[p]:pop_inds[p] + 2, stable_inds.astype(int)] = t * self.P[
                    'time_step']  # Store the time stability reached
        else:
            if len(self.fp) == 0: return
            check_inds_int = np.arange(0, self.P['n_trials'], 1) * check_inds_bool
            stable_inds = np.array([])
            for s in range(self.fp.shape[0]):
                I_A_bool = abs(S[0, check_inds_int] - self.fp[s, 0]) < err
                I_B_bool = abs(S[1, check_inds_int] - self.fp[s, 1]) < err
                stable_inds = np.concatenate((stable_inds, check_inds_int[I_A_bool * I_B_bool]))
            self.stability_time[stable_inds.astype(int)] = t * self.P['time_step']  # Store the time stability reached

    def checkWinner(self):
        """
        Returns the winning population for each pair, using the record of trials and the firing rate threshold
        :return:
        """
        # Index the last time step where the stimulus was present
        check_ind = int((self.P['stim_on'] + self.P['stim_dur']) / (self.P['time_step'] * self.P['sample_rate']))
        check_time = self.R[:, check_ind, :] > self.P['fr_threshold']
        self.winners = check_time * np.tile(np.array([1, 2]), [self.P['n_trials'], self.P['n_areas']]).transpose()

    def calcRT(self):
        """Gets the reaction times for the winner of each area"""
        # Time relative to stimulus onset
        tm = np.arange(0, self.P['duration'], self.P['time_step']) - brian2.asarray(self.P['stim_on'])
        # Loop through trials
        for t in range(self.P['n_trials']):
            for a in range(self.P['n_areas']):
                # Get index of winning population
                a_win = np.argmax(self.winners[2 * a:2 * a + 2, t]) + 2 * a
                # Extract time of first threshold crossing
                if (self.R[a_win, :, t] > self.P['fr_threshold']).any():  # Make sure there's a winning population
                    self.thresh_cross_time[a, t] = tm[np.argmax(self.R[a_win, :, t] > self.P['fr_threshold'])]
                else:
                    self.thresh_cross_time[a, t] = np.nan  # If not, it gets a nan value

    def conflictReadoutAgree(self):
        """
        When conflict trials occur in hierarchical network, this determines what proportion of trials readout agree with
        population 1
        :return:
        """
        conflict = self.checkConflict()
        trls = self.winners[:, conflict]
        agree = np.zeros((trls.shape[1]), dtype=bool)
        # Loop through only conflict trials
        for t in range(trls.shape[1]):
            if max(trls[0:2, t]) == max(trls[4:6, t]):
                print(self.winners[:, t])
                agree[t] = True
            elif max(trls[2:4, t]) != max(trls[4:, t]):
                raise ValueError('On trial ' + str(t + 1) + ' readout did not match area 1 or 2')
        return agree

    def checkConflict(self):
        """
        Checks if there is a conflict between the readout layer and intermediate layers in the hierarchical network
        :return:
        """
        conflict = np.zeros((self.P['n_trials']), dtype=bool)
        for t in range(self.P['n_trials']):
            if max(self.winners[0:2, t]) != max(self.winners[2:4, t]):
                conflict[t] = True
        return conflict

    def saveTrialData(self, file_name="single_trial_data.pickle"):
        """
        Saves key variables of the object.
        :param file_name: The name and path where the file will be stored
        :return:
        """
        # Create dictionary to be saved
        save_dict = {'P': self.P,
                     'stim_inputs': self.stim_inputs,
                     'R': self.R,
                     'I': self.I,
                     'S': self.S,
                     'Ib': self.Ib,
                     'thresh_cross_time': self.thresh_cross_time,
                     'winners': self.winners,
                     'nSteps': self.nSteps}
        # Save it
        saveData(file_name, save_dict)

    def loadTrialData(self, file_name="single_trial_data.pickle"):
        """
        Loads key variables from prior runs
        :param file_name: The name and path of where the file is be stored
        :return:
        """
        # Load it
        save_dict = loadData(file_name=file_name)
        # Update variables with saved values
        self.P = save_dict['P']
        self.stim_inputs = save_dict['stim_inputs']
        self.R = save_dict['R']
        self.I = save_dict['I']
        self.S = save_dict['S']
        self.Ib = save_dict['Ib']
        self.thresh_cross_time = save_dict['thresh_cross_time']
        self.winners = save_dict['winners']
        self.nSteps = save_dict['nSteps']
        # Delete the save_dict
        del save_dict

    def saveTrialPlots(self, fig_name="Trial_Details.pdf", trial=0):
        """
        Plots the firing rate and synaptic current results. Primarily used for diagnostics of fishy behavior
        :param fig_name: Path, name and extension of figure
        :param trial: Index of the trial to be plotted
        :return:
        """
        if not fig_name.endswith('.pdf'): fig_name = (fig_name + '.pdf')

        self.pdf = matplotlib.backends.backend_pdf.PdfPages(fig_name)
        # Construct array of populations
        pops, plot_thresh = np.array([]), False
        for a in range(self.P['n_areas']):
            pops = np.append(pops, [(str(a + 1) + 'A'), (str(a + 1) + 'B')])

        # Plot the firing rate (TOGGLE WITH COMMENTING)
        for p in range(len(pops)):
            if p >= (len(pops) - 2): plot_thresh = True
            self.plotTime(self.R[p, :, trial], "FR of population " + pops[p], 'time', 'FR', plot_thresh=plot_thresh)

        # Plot the synaptic drive (TOGGLE WITH COMMENTING)
        for p in range(len(pops)):
            self.plotTime(self.S[p, :, trial], "Synaptic drive of population " + pops[p], 'time', 'Synaptic Drive')

        # Plot phase space, FR and S for each population (TOGGLE WITH COMMENTING)
        for a in range(self.P['n_areas']):
            self.plotPhaseSpace(self.R[a * 2, :, trial], self.R[a * 2 + 1, :, trial],
                                ("Area " + str(a + 1) + " FR phase space"),
                                ('FR Pop ' + str(a + 1) + 'A'), ('FR Pop ' + str(a + 1) + 'B'))
            self.plotPhaseSpace(self.S[a * 2, :, trial], self.S[a * 2 + 1, :, trial],
                                ("Area " + str(a + 1) + " synaptic drive phase space "),
                                ('S Pop ' + str(a + 1) + 'A'), ('S Pop ' + str(a + 1) + 'B'))

        # Plot synaptic currents (TOGGLE WITH COMMENTING)
        for p in range(len(pops)):
            self.plotTime(self.I[p, :, trial], "Synaptic current of population " + pops[p], 'time', 'Synaptic Current')

        # # Plot noise currents (TOGGLE WITH COMMENTING)
        # for p in range(len(pops)):
        #     self.plotTime(self.Ib[p, :], "Noise current of population " + pops[p], 'time', 'Synaptic Current')

        self.pdf.close()
        self.pdf = None

    def plotEpochs(self):
        """Places vertical lines where events occur"""
        plt.axvline(brian2.asarray(self.P['stim_on']), color='r')
        plt.axvline(brian2.asarray(self.P['stim_on'] + self.P['stim_dur']), color='r')
        # if self.decision_time is not None:
        #     plt.axvline(self.decision_time, color='g')

    def plotTime(self, Y, ttl, X_lab, Y_lab, plot_thresh=False):
        '''' Plots variables where time is on the X axis '''
        tm = np.arange(0, self.P['duration'], self.P['time_step'] * self.P['sample_rate'])
        tm = tm[:Y.shape[0]]  # Correct incongruencies in length due to sampling rate
        plt.plot(tm, Y)
        if plot_thresh:
            plt.plot([0, self.P['duration']], [self.P['fr_threshold'], self.P['fr_threshold']], color='k',
                     linestyle='--')
        self.plotEpochs()
        plt.title(ttl)
        plt.ylabel(Y_lab)
        plt.xlabel(X_lab)
        self.pdf.savefig()
        plt.close()

    def plotPhaseSpace(self, X1, X2, ttl, X_lab, Y_lab):
        """Phase space of individual Values"""
        plt.plot(X1, X2)
        plt.title(ttl)
        plt.ylabel(Y_lab)
        plt.xlabel(X_lab)
        self.pdf.savefig()
        plt.close()

    def histHelper(self, ax, RTs, ttl):
        """Helper function for plotting reaction time histograms"""
        ax.hist(RTs.flatten(), color='k', label='All')
        colors = ['r', 'b', 'g']
        for a in range(self.P['n_areas']):
            ax.hist(RTs[a, :], color=colors[a], label=('Area ' + str(a + 1)))
        ax.legend()
        ax.set_xlabel('Threshold Cross Time (ms)')
        ax.set_ylabel('# Trials')
        ax.set_title(ttl + ', Coherence = ' + str(self.P['coherence']))

    def saveRThistPlots(self, fig_name='RT_histograms'):
        """Plots the reaction time histograms for the three populations in both conflict and agreement trials"""
        conflict = self.checkConflict()
        fig, axes = plt.subplots(nrows=2, ncols=1, sharex='all', sharey='all')
        self.histHelper(axes[0], self.thresh_cross_time[:, conflict], 'Conflict Trials')
        self.histHelper(axes[1], self.thresh_cross_time[:, ~np.array(conflict)], 'Agreement Trials')
        fig.tight_layout()
        plt.savefig(fig_name + '.pdf', format='pdf')
        plt.close()


class fpDistribution():
    """Calculate the distribution of fixed points over a number of trials for runs of the decisionTrials() simulation"""
    def __init__(self, P, duration=4000):
        """
        Initiation method
        :param P: Parameter produced by hnParameters or lnParameters
        :param duration: Duration of simulation (ms)
        """
        self.P = P
        self.P['duration'] = duration * brian2.ms
        self.P['stim_dur'] = self.P['duration'] - (self.P['stim_on'] + 100 * brian2.ms)
        self.P['sample_rate'] = 100
        self.fp_perc = np.nan
        self.fp_n = np.nan
        self.fp = np.nan

    def run(self):
        """
        Runs simulation and calculates values
        :return:
        """
        trials = decisionTrials(self.P)
        if len(trials.fp) == 0:
            raise ValueError('Fixed points not calculated for this distribution of weights and inputs')
        trials.runTrials()
        fp_n = np.zeros((len(trials.fp)) + 1)
        drop_ind = np.isnan(trials.stability_time)
        fp_n[-1] = drop_ind.sum()  # Total number of trials with no stability
        S, stable_tm = trials.S[:, :, drop_ind < 1], trials.stability_time[drop_ind < 1]
        indx = (((stable_tm * brian2.second) / self.P['time_step']) / self.P['sample_rate']).astype(int)
        sft = np.arange(0, S[0, :, :].size, S[0, :, :].shape[0])
        indx_sft = (indx + sft).astype(int)
        err = 0.01
        for f in range(len(trials.fp)):
            s1 = abs(S[0, :, :].transpose().flatten()[indx_sft] - trials.fp[f][0]) < err
            s2 = abs(S[1, :, :].transpose().flatten()[indx_sft] - trials.fp[f][1]) < err
            fp_n[f] = sum(s1 * s2)
        fp_perc = fp_n / fp_n.sum()
        self.fp_n, self.fp_perc, self.fp = fp_n, fp_perc, trials.fp

    def saveDist(self, f_name=''):
        """
        Saves the distribution of fixed points
        :param f_name: path and filename
        :return:
        """
        if len(f_name) == 0:
            f_name = 'fp_dist_E_%.3f_I_%.3f_mu_%d_%d' % ((self.P['w'][0, 0] / brian2.nA),
                                                         (self.P['w'][0, 1] / brian2.nA),
                                                         brian2.asarray(self.P['mu'][[0, 2]].mean()),
                                                         brian2.asarray(self.P['mu'][[1, 3]].mean()))
        with open(f_name, 'w') as f: pickle.dump([self.fp_n, self.fp_perc, self.P], f)


class psychometricWithReference():
    """"
    Simulates psychometric function by keeping the attributes of one bundle fixed for reference (at same value) and then
    increasing both attributes of the other bundle by the same value on each trial.
    """
    def __init__(self, P=None, input_range=np.arange(10, 21, 1), ref_vals=15):
        if P is None:
            P = hnParameters(input_range=input_range)
        self.P = P
        self.ref_vals = ref_vals
        self.p_A = np.zeros(len(self.P['input_range'])) * np.nan

    def runSVsweep(self, parallel=True):
        """
        Main method in the function to compute the values
        :param parallel: Whether to run as parallel processing (default=True). Currently only parallel is implemented
        :return:
        """
        if parallel:
            print('Running single decision simulation in parallel')
            pool = getPool()
            results = pool.map(self.runSVsweepCall,
                               self.P['input_range'].reshape(self.P['input_range'].shape[0], 1))
            pool.close()
            print('Finished collecting results')
            for c in range(self.P['input_range'].shape[0]):
                self.p_A[c] = results[c][0]
        else:
            raise ValueError('Only parallel is currently implemented')

    def runSVsweepCall(self, input):
        """
        Main call function in the parallelization loop
        :param input: input value for that run of the simulation
        :return: p_A, winning_pop, winners, decision_time
        """
        print('Testing input levels ' + str(input))
        print("input type is " + str(type(input)))
        Par = self.changeMu(input)
        trials = decisionTrials(Par)
        trials.runTrials()
        if self.P['n_areas'] == 1:  # Single area
            decision_time = trials.thresh_cross_time
            p_A = (trials.winners[0, :] > 0).sum() / (Par['n_trials'] - sum(np.isnan(decision_time.flatten())))
            winning_pop, winners = None, None
        else:
            p_A, winning_pop, winners, decision_time = findWinning(trials)
        del trials
        return p_A, winning_pop, winners, decision_time

    def changeMu(self, input):
        """
        Updates the parameter dictionary with new input values
        :param input: Input that both attributes of one bundle are set to
        :return:
        """
        Par = copy.deepcopy(self.P)
        print('Simulating Bundle ' + str(input))
        Par['mu'] = np.zeros((6, 1))
        Par['mu'][[0, 2]] = input
        Par['mu'][[1, 3]] = self.ref_vals
        Par['mu'] = Par['mu'] * brian2.Hz
        return Par


class subjectiveValueSweep():
    """
    Vary the input strength of population attribute a for either a single alternative or both alternatives while keeping
    all other input constant. The other values are specified using mu_0 in the parameter dictionary. The input range
    must either be specified in the passed dictionary, or will be created based on the default mu_0.
    """
    def __init__(self, P=None, typ='vary_att'):
        """
        Intilization method
        :param P: Parameter dictionary produced by lnParameters() or hnParameters(). (default=None)
        :param typ: 'vary_att' or 'single_val'. Determines whether the whole attribute or just one value is varied.
        """
        if P is None:
            print("No parameters given")
            P = hnParameters()
            P['input_range'] = np.arange(0, P['mu_0'][0] * 2)
        self.P = P
        self.p_A = np.zeros(P['input_range'].shape)
        self.decision_time = np.zeros((P['input_range'].shape[0], P['n_trials']))
        self.type = typ
        # Three-area specific variables
        if P['n_areas'] > 1:
            self.winning_pop = self.decision_time.copy()
            self.winners = self.decision_time.copy()

    def runSVsweep(self, parallel=True):
        """
        Go through all sets of input levels and collect relevant information. Can run in parallel or loop
        :param parallel: whether to run in parallel (default=True)
        :return:
        """
        if self.P['n_areas'] == 1:
            """ Only running for single area """
            # coherence_sets = self.coherence_sets.mean(axis=1) # Average the input coherence levels
            if parallel:
                print('Running single decision simulation in parallel')
                pool = getPool()
                results = pool.map(self.runSVsweepCall,
                                   self.P['input_range'].reshape(self.P['input_range'].shape[0], 1))
                pool.close()
                print('Finished collecting results')
                for c in range(self.P['input_range'].shape[0]):
                    self.p_A[c] = results[c][0]
                    self.decision_time[c, :] = results[c][3]
            else:
                print('Running single decision simulation in loop')
                for c in range(self.P['input_range'].shape[0]):
                    print('Testing input strength ' + str(self.P['input_range'][c]))
                    self.p_A[c], _, _, self.decision_time[c, :] = self.runSVsweepCall(self.P['input_range'][c])

        else:
            """Running for three areas"""
            if parallel:
                print('Running hierarchical decisions-making simulation in parallel')
                pool = getPool()
                results = pool.map(self.runSVsweepCall,
                                   self.P['input_range'].reshape(self.P['input_range'].shape[0], 1))
                pool.close()
                print('Finished collecting results')
                for c in range(self.P['input_range'].shape[0]):
                    self.p_A[c] = results[c][0]
                    self.winning_pop[c, :] = results[c][1]
                    self.winners[c, :] = results[c][2]
                    self.decision_time[c, :] = results[c][3]
            else:
                print('Running hierarchical decisions-making simulation in loop')
                for c in range(self.P['input_range'].shape[0]):
                    print('Testing input strength ' + str(self.P['input_range'][c]))
                    self.p_A[c], self.winning_pop[c, :], self.winners[c, :], self.decision_time[c, :] = \
                        self.runSVsweepCall(self.P['input_range'][c])

    def runSVsweepCall(self, input):
        """
        Main call function in the parallelization loop
        :param input: Input level of that specific simulation
        :return: p_A, winning_pop, winners, decision_time
        """
        Par = copy.deepcopy(self.P)
        print('Testing input levels ' + str(input))
        print("input type is " + str(type(input)))
        print("mu_0 is " + str(Par['mu_0']))
        # Checks if it is a single area or multiple that are varied
        if self.type == 'vary_att':
            Par['mu_0'][0] = input * brian2.Hz
        elif self.type == 'single_val':
            Par['mu'] = np.array([float(input), Par['mu_0'][0], Par['mu_0'][1], Par['mu_0'][0], 0, 0]). \
                            reshape(6,1) * brian2.Hz
        trials = decisionTrials(Par)
        trials.runTrials()
        if self.P['n_areas'] == 1:  # Single area
            decision_time = trials.thresh_cross_time
            p_A = (trials.winners[0, :] > 0).sum() / (Par['n_trials'] - sum(np.isnan(decision_time.flatten())))
            winning_pop, winners = None, None
        else:
            p_A, winning_pop, winners, decision_time = findWinning(trials)
        del trials
        return p_A, winning_pop, winners, decision_time

    def saveSVdata(self, file_name='sv_data.pickle'):
        """
        Saves the data in pickle file
        :param file_name: path and name of file
        :return:
        """
        # Create dictionary to be saved
        save_dict = {'P': self.P,
                     'p_A': self.p_A,
                     'type': self.type}
        if self.P['n_areas'] > 1:
            save_dict['winning_pop'] = self.winning_pop
            save_dict['winners'] = self.winners
            save_dict['decision_time'] = self.decision_time
        saveData(file_name=file_name, save_dict=save_dict)

    def loadSVdata(self, file_name='sv_data.pickle'):
        """
        Loads the data from existing pickle file
        :param file_name: path and name of file
        :return:
        """
        save_dict = loadData(file_name=file_name)
        # Update variables with saved values
        self.P = save_dict['P']
        self.p_A = save_dict['p_A']
        if self.P['n_areas'] > 1:
            self.winning_pop = save_dict['winning_pop']
            self.winners = save_dict['winners']
            self.decision_time = save_dict['decision_time']
        # Delete the save_dict
        del save_dict


class coherenceRange():
    """
    These methods call the simulation over a range of coherence values and analyze behavior. Note that when running with
    A single population, self.pop1_pChooseA and self.conflict_prob become None
    """
    def __init__(self, P):
        """
        Initialization. Specify run prameters (ie input range and coherence range) using the P dictionary.
        :param P: Parameter dictionary produced by lnActionParams() or hnActionParams()
        """
        self.P = P
        self.p_A = np.zeros((P['input_range'].shape[0], P['coherence_range'].shape[0]))
        self.decision_time = np.zeros((P['input_range'].shape[0], 2, P['coherence_range'].shape[0]))  # Mean and STD

    # Loop through all the coherence ranges
    def runCoherenceRange(self):
        """
        Main method for running the simulations. Simply call and it will population p_A and decision_time with results
        :return:
        """
        if self.P['choice_method'] == 'threshcross':
            coherence = np.array([1.0, -1.0, -1.0])
        elif self.P['choice_method'] == 'action_area':
            coherence = np.array([1.0, -1.0, None])
        else:
            raise ValueError('Invalid choice_method specified')
        ind = np.invert(np.isnan(coherence.astype(float)))
        Par = copy.deepcopy(self.P)
        for c in range(self.P['coherence_range'].shape[0]):
            print('Testing coherence level ' + str(self.P['coherence_range'][c]))
            Par['coherence'][ind] = coherence[ind] * self.P['coherence_range'][c]
            sv = subjectiveValueSweep(Par)
            sv.runSVsweep(parallel=True)
            self.p_A[:, c] = sv.p_A
            self.decision_time[:, 0, c] = np.nanmean(sv.decision_time, axis=1)
            self.decision_time[:, 1, c] = np.nanstd(sv.decision_time, axis=1)

    #
    def saveCoherenceRangeData(self, file_name='vary_coherence_data.pickle'):
        """
        Saves p_A and decision_time in pickle file
        :param file_name: Path and name of file to be saved. (default='vary_coherence_data.pickle')
        :return:
        """
        # Create dictionary to be saved
        save_dict = {'P': self.P,
                     'p_A': self.p_A,
                     'decision_time': self.decision_time}
        saveData(file_name=file_name, save_dict=save_dict)

    # Loads the data from existing pickle file
    def loadCoherenceRangeData(self, file_name='vary_coherence_data.pickle'):
        """
        Loads object data from a pickle file.
        :param file_name: path and name of file to be loaded. (default='vary_coherence_data.pickle')
        :return:
        """
        save_dict = loadData(file_name=file_name)
        # Update variables with saved values
        self.P = save_dict['P']
        self.p_A = save_dict['p_A']
        self.decision_time = save_dict['decision_time']
        # Delete the save_dict
        del save_dict


class varyNoiseSigma():
    """
    Vary the noise/uncertainty level presented to the decisionTrials() simulation. Easily mbeds in other parameter sweep
    functions.
    """
    def __init__(self, P):
        """
        Initialization method. Specify the range of noise/uncertainty levels using the parameter 'noise_sigma_range'
        :param P: Parameter dictionary produced by lnParameters() or hnParameters()
        """
        print('initializing parameters')
        self.P = P
        self.p_A = np.zeros((len(self.P['noise_sigma_range']), 1))
        self.p_failed = self.p_A.copy()
        self.decision_time = np.zeros((len(self.P['noise_sigma_range']), P['n_trials']))
        if self.P['n_areas'] > 1:
            self.winning_pop = self.decision_time.copy()
            self.winners = self.decision_time.copy()
            self.intermed_time_gain = self.p_A.copy()  # Timed gained by reading out through intermediate layer
            self.intermed_agree = self.p_A.copy()  # How closely the intermediate decision matches the actual
            self.intermed_pA = self.p_A.copy()  # Proportion A chosen by reading out from intermediate layer
            self.dt_intermed_readout = self.p_A.copy()  # Highest intermed pop when decision is reached
            self.p_intermed_decision = self.p_A.copy()  # Proportion of time intermed area reaches a decision
            self.intermed_att_time_gain = self.p_A.copy()  # Reading out from attributes separately, time gained
            self.intermed_att_agree = self.p_A.copy()  # Reading out from attributes separately, when it matches decision
            self.intermed_att_pA = self.p_A.copy()  # Reading out from attributes separately, P(A)
            self.p_intermed_att_decision = self.p_A.copy()  # Reading out from attributes separately, P(decision reached)
            self.stability_time = np.zeros((self.P['n_areas'] - 1, P['n_trials']))  # Only record for first noise level
        else:
            self.stability_time = np.zeros((self.P['n_areas'] - 1, P['n_trials']))  # Only record for first noise level

    def runVaryNoise(self, parallel=False):
        """
        Main method for running the class. Can run in parallel or serial.
        :param parallel: True/False. Whether to run simulations in parallel (default=False)
        :return:
        """
        if parallel:
            print('Varying noise in parallel')
            pool = getPool()
            results = pool.map(self.varyNoiseCall, self.P['noise_sigma_range'])
            pool.close()
            print('Finished collecting results')
            for c in range(len(self.P['noise_sigma_range'])):
                self.p_A[c] = results[c][0]
                self.decision_time[c, :] = results[c][3]
                if c == 1: self.stability_time = results[c][4]
                if self.P['n_areas'] > 1:
                    self.winning_pop[c, :] = results[c][1]
                    self.winners[c, :] = results[c][2]
                    self.intermed_time_gain[c] = results[c][5]
                    self.intermed_agree[c] = results[c][6]
                    self.dt_intermed_readout[c] = results[c][8]
                    self.p_intermed_decision[c] = results[c][9]
                    self.intermed_pA[c] = results[c][10]
                    self.intermed_att_time_gain[c] = results[c][11]
                    self.intermed_att_agree[c] = results[c][12]
                    self.intermed_att_pA[c] = results[c][13]
                    self.p_intermed_att_decision[c] = results[c][14]
                self.p_failed[c] = results[c][7]
        else:
            for n in range(len(self.P['noise_sigma_range'])):
                print('Simulating noise level %.4f' % self.P['noise_sigma_range'][n])
                p_A, winning_pop, winners, decision_time, stability_time, intermed_time_gain, intermed_agree, \
                p_failed, dt_intermed_readout, p_intermed_decision, intermed_pA, intermed_att_time_gain, \
                intermed_att_agree, intermed_att_pA, p_intermed_att_decision = \
                    self.varyNoiseCall(input=self.P['noise_sigma_range'][n])
                if self.P['n_areas'] > 1:
                    self.winning_pop[n, :] = winning_pop
                    self.winners[n, :] = winners
                    self.intermed_time_gain[n] = intermed_time_gain
                    self.intermed_agree[n] = intermed_agree
                    self.dt_intermed_readout[n] = dt_intermed_readout
                    self.p_intermed_decision[n] = p_intermed_decision
                    self.intermed_pA[n] = intermed_pA
                    self.intermed_att_time_gain[n] = intermed_att_time_gain
                    self.intermed_att_agree[n] = intermed_att_agree
                    self.intermed_att_pA[n] = intermed_att_pA
                    self.p_intermed_att_decision[n] = p_intermed_att_decision
                self.p_A[n] = p_A
                self.decision_time[n, :] = decision_time
                self.p_failed[n] = p_failed
                if n == 1: self.stability_time = stability_time
        print('Done with simulations!')

    def varyNoiseCall(self, input):
        """
        Method called by runVaryNoise to handle the specifics of simulations
        :param input: noise/uncertainty level for that set of simulations
        :return:
        """
        print('Simulating noise level %.4f in parallel' % input)
        Par = copy.deepcopy(self.P)
        Par['input_noise'] = input
        # Run the trials and collect the results
        trials = decisionTrials(Par)
        trials.runTrials()
        if self.P['n_areas'] == 1:  # Single area
            decision_time = trials.thresh_cross_time
            p_A = getPA(winners=trials.winners[0, :], n_trials=Par['n_trials'], decision_time=decision_time)
            winning_pop, winners, intermed_time_gain, intermed_agree, dt_intermed_readout, p_intermed_decision, \
            intermed_pA, intermed_att_time_gain, intermed_att_agree, intermed_att_pA, p_intermed_att_decision = \
                None, None, None, None, None, None, None, None, None, None, None
        else:
            p_A, winning_pop, winners, decision_time = findWinning(trials)
            intermed_time_gain, intermed_agree, dt_intermed_readout, p_intermed_decision, intermed_pA = \
                calcIntermedLayerGains(trials, winners=winners)
            intermed_att_time_gain, intermed_att_agree, intermed_att_pA, p_intermed_att_decision = \
                calcIntermediateAttStats(trials, winners=winners)
        stability_time, p_failed = trials.stability_time, np.mean(np.isnan(trials.winners[-1, :]))
        del trials
        # print(str(Par['w'][0:4,0:4]) + ', p_A = ' + str(p_A))
        return p_A, winning_pop, winners, decision_time, stability_time, intermed_time_gain, intermed_agree, \
               p_failed, dt_intermed_readout, p_intermed_decision, intermed_pA, intermed_att_time_gain, \
               intermed_att_agree, intermed_att_pA, p_intermed_att_decision

    def saveVaryNoisedata(self, file_name='vary_noise_data.pickle'):
        """
        Saves the class results data in pickle file
        :param file_name: Path and filename (default='vary_noise_data.pickle')
        :return:
        """
        # Create dictionary to be saved
        save_dict = {'P': self.P,
                     'p_A': self.p_A,
                     'p_failed': self.p_failed,
                     'stability_time': self.stability_time,
                     'decision_time': self.decision_time}
        if self.P['n_areas'] > 1:
            save_dict['winning_pop'] = self.winning_pop
            save_dict['winners'] = self.winners
            save_dict['intermed_time_gain'] = self.intermed_time_gain
            save_dict['intermed_agree'] = self.intermed_agree
            save_dict['intermed_pA'] = self.intermed_pA
            save_dict['dt_intermed_readout'] = self.dt_intermed_readout
            save_dict['p_intermed_decision'] = self.p_intermed_decision
            save_dict['intermed_att_time_gain'] = self.intermed_att_time_gain
            save_dict['intermed_att_agree'] = self.intermed_att_agree
            save_dict['intermed_att_pA'] = self.intermed_att_pA
            save_dict['p_intermed_att_decision'] = self.p_intermed_att_decision
        saveData(file_name=file_name, save_dict=save_dict)

    # Loads the data from existing pickle file
    def loadVaryNoisedata(self, file_name='vary_noise_data.pickle'):
        save_dict = loadData(file_name=file_name)
        # Update variables with saved values
        self.P = save_dict['P']
        self.p_A = save_dict['p_A']
        self.decision_time = save_dict['decision_time']
        if self.P['n_areas'] > 1:
            self.winning_pop = save_dict['winning_pop']
            self.winners = save_dict['winners']
            self.decision_time = save_dict['decision_time']
        if 'stability_time' in save_dict:  # backward compatibility
            self.stability_time = save_dict['stability_time']
        if 'intermed_time_gain' in save_dict:
            self.intermed_time_gain = save_dict['intermed_time_gain']
            self.intermed_agree = save_dict['intermed_agree']
            self.p_failed = save_dict['p_failed']
            self.intermed_pA = save_dict['intermed_pA']
            self.p_intermed_decision = save_dict['p_intermed_decision']
            self.dt_intermed_readout = save_dict['dt_intermed_readout']
            self.intermed_att_time_gain = save_dict['intermed_att_time_gain']
            self.intermed_att_agree = save_dict['intermed_att_agree']
            self.intermed_att_pA = save_dict['intermed_att_pA']
            self.p_intermed_att_decision = save_dict['p_intermed_att_decision']
        # Delete the save_dict
        del save_dict


class maxLikeDeviations():
    """
    Runs hierarchical model simulations on bundles that should maximally differentiate between a linear integration of
    attributes, and a max-like, winner-take-all integration of attributes.
    """
    def __init__(self, P=None, bundles=None, vals=None, w=np.array([[0.32, -0.08], [-0.08, 0.32]])):
        """
        :param P: Parameter dictionary produced by lnParameters() or hnParameters()
        :param bundles: Matrix of bundles to simulate. If None, appropriate bundles are created (default=None)
        :param vals: Values to be used in creating bundles. If None, it uses the default of the maxDeviationBundles()
                     function. (default=None)
        :param w: Intermediate layer area weights. (default=np.array([[0.32, -0.08], [-0.08, 0.32]]))
        """
        if P is None:
            print("No parameters dictionary given. One is being created using defaults.")
            P = hnParameters(w=w, n_trials=100)
            P['truncate_trial'] = True
            P['stim_on'] = 50 * brian2.ms
            P['stim_dur'] = 2950 * brian2.ms
            P['duration'] = 3000.0 * brian2.ms
        self.P = P
        if bundles is None:
            bundles = maxDeviationBundles(vals=vals)
        self.bundles = bundles
        # Create variables for decision area
        self.p_A = np.zeros((self.bundles.shape[0]))
        self.p_failed = self.p_A.copy()  # Proportion of trials where no decision was reached
        self.decision_time_mean = self.p_A.copy()
        # Variables for pooled intermediate areas
        self.intermed_agree = self.p_A.copy()  # How closely the intermediate decision matches the actual
        self.intermed_pA = self.p_A.copy()
        self.intermed_time_gain = self.p_A.copy()  # Highest intermed pop when decision is reached
        self.p_intermed_decision = self.p_A.copy()  # Proportion of time intermed area reaches a decision
        # Variales for separated intermedate area
        self.intermed_att_time_gain = self.p_A.copy()
        self.intermed_att_agree = self.p_A.copy()
        self.intermed_att_pA = self.p_A.copy()
        self.p_intermed_att_decision = self.p_A.copy()
        self.intermed_att_discrim_pA = np.zeros((self.bundles.shape[0], self.P[
            'n_areas'] - 1)) * np.nan  # Intemediate layer population that passes threshold
        self.intermed_att_discrim_time_mean = self.intermed_att_discrim_pA.copy()  # Time the comp is resolved for each att using difference
        self.intermed_att_discrim_pFailed = self.intermed_att_discrim_pA.copy()
        self.intermed_att_discrim_diff_relA = np.zeros((self.bundles.shape[0], self.P['n_areas'] - 2)) * np.nan
        # Meta-variables
        self.reward_observed = np.nan
        self.reward_max = np.nan
        self.reward_min = np.nan
        self.p_correct = np.nan
        self.intermed_reward_observed = np.nan
        self.intermed_reward_max = np.nan
        self.intermed_reward_min = np.nan
        self.intermed_p_correct = np.nan
        self.intermed_att_reward_observed = np.nan
        self.intermed_att_reward_max = np.nan
        self.intermed_att_reward_min = np.nan
        self.intermed_att_p_correct = np.nan

    def runMaxDeviations(self, parallel=True, failed_random=False):
        """
        Main class method for running simulations.
        :param parallel: True/False. Run simulations in parallel (default=True)
        :param failed_random: True/False. Sometimes, the network doesn't reach a decision. When that happens, the trial
                              can either be 'failed," or the decision can be made randomly. (default=False)
        :return:
        """
        if parallel:
            pool = getPool()
            results = pool.map(self.decisionTrialCall, self.bundles)
            for c in range(self.bundles.shape[0]):
                self.p_A[c] = results[c][0]
                self.decision_time_mean[c] = np.nanmean(results[c][1].flatten())
                self.p_failed[c] = results[c][2]
                if self.P['n_areas'] > 1:
                    self.intermed_time_gain[c] = results[c][3]
                    self.intermed_agree[c] = results[c][4]
                    self.p_intermed_decision[c] = results[c][5]
                    self.intermed_pA[c] = results[c][6]
                    self.intermed_att_time_gain[c] = results[c][7]
                    self.intermed_att_agree[c] = results[c][8]
                    self.intermed_att_pA[c] = results[c][9]
                    self.p_intermed_att_decision[c] = results[c][10]
                    self.intermed_att_discrim_pA[c, :] = results[c][11]
                    self.intermed_att_discrim_time_mean[c, :] = results[c][12]
                    self.intermed_att_discrim_pFailed[c, :] = results[c][13]
                    self.intermed_att_discrim_diff_relA[c, :] = results[c][14]
        else:
            raise ValueError('Non-parallel has not been implemented yet.')
        # Calculate meta-variables
        self.calcMetaVariables(failed_random=failed_random)

    def changeMu(self, input):
        """
        Helper method that updates the input vector for the simulation runs
        :param input: 4x1 vector of floats. Represents firing rates for a bundle.
        :return: P. The updated parameters dictionary
        """
        Par = copy.deepcopy(self.P)
        print('Simulating Bundle ' + str(input))
        Par['mu'] = np.zeros((6, 1))
        Par['mu'][0:4] = input.reshape(4, 1)
        Par['mu'] = Par['mu'] * brian2.Hz
        return Par

    def calcMetaVariables(self, failed_random=False):
        """
        Helper method that does secondary analysis on simulation results
        :param failed_random: True/False. Sometimes, the network doesn't reach a decision. When that happens, the trial
                              can either be 'failed," or the decision can be made randomly. (default=False)
        :return:
        """
        # Meta variables for decision area
        self.reward_observed, self.reward_max, self.reward_min, self.p_correct = \
            self.calcRewardRates(p_A=self.p_A, p_failed=self.p_failed, failed_random=failed_random)
        # meta-variables for intermediate-pooled
        self.intermed_reward_observed, self.intermed_reward_max, self.intermed_reward_min, self.intermed_p_correct = \
            self.calcRewardRates(p_A=self.intermed_pA, p_failed=(1 - self.p_intermed_decision),
                                 failed_random=failed_random)
        # Meta-varialbes
        self.intermed_att_reward_observed, self.intermed_att_reward_max, self.intermed_att_reward_min, \
        self.intermed_att_p_correct = \
            self.calcRewardRates(p_A=self.intermed_att_pA, p_failed=(1 - self.p_intermed_att_decision),
                                 failed_random=failed_random)

    def decisionTrialCall(self, input):
        """
        Helper function that calls the decisionTrial simulation and performs a number of secondary analysis
        :param input: 4x1 vector of floats. Represents firing rates for a bundle.
        :return: p_A, decision_time, p_failed, intermed_time_gain, intermed_agree, p_intermed_decision, intermed_pA, \
                 intermed_att_time_gain, intermed_att_agree, intermed_att_pA, \
                 p_intermed_att_decision, intermed_att_discrim_pA, intermed_att_discrim_time_mean, \
                 intermed_att_discrim_pFailed, intermed_att_discrim_diff_relA
        """
        Par = self.changeMu(input)
        # Run the trials and collect the results
        trials = decisionTrials(Par)
        trials.runTrials()
        if Par['n_areas'] == 1:  # Single area
            decision_time = trials.thresh_cross_time
            p_A = (trials.winners[0, :] > 0).sum() / (Par['n_trials'] - sum(np.isnan(decision_time.flatten())))

            winning_pop, winners, intermed_time_gain, intermed_agree, dt_intermed_readout, p_intermed_decision, \
            intermed_pA, intermed_att_time_gain, intermed_att_agree, intermed_att_pA, p_intermed_att_decision = \
                None, None, None, None, None, None, None, None, None, None, None
        else:
            p_A, _, winners, decision_time = findWinning(trials)
            p_failed = np.mean(np.isnan(trials.winners[-1, :]))
            intermed_time_gain, intermed_agree, _, p_intermed_decision, intermed_pA = calcIntermedLayerGains(trials,
                                                                                                             winners)
            intermed_att_time_gain, intermed_att_agree, intermed_att_pA, p_intermed_att_decision = calcIntermediateAttStats(
                trials, winners)
            intermed_att_discrim_pA = np.nanmean(trials.att_discrimination_pop == 0, axis=1)
            intermed_att_discrim_time_mean = np.nanmean(trials.att_discrimination_time, axis=1)
            intermed_att_discrim_pFailed = np.nanmean(np.isnan(trials.att_discrimination_time), axis=1)
            # Replace Nan values with overall decision time
            for a in range(trials.P['n_areas'] - 1):
                trials.att_discrimination_time[a, np.isnan(trials.att_discrimination_time[a, :])] = \
                    decision_time[np.isnan(trials.att_discrimination_time[a, :])]
                # Get mean time difference between area A and others
                intermed_att_discrim_diff_relA = np.zeros(trials.P['n_areas'] - 2)
            for a in range(trials.P['n_areas'] - 2):
                intermed_att_discrim_diff_relA[a] = np.nanmean(
                    trials.att_discrimination_time[0, :] - trials.att_discrimination_time[a + 1, :])
        del trials
        return p_A, decision_time, p_failed, intermed_time_gain, intermed_agree, p_intermed_decision, intermed_pA, \
               intermed_att_time_gain, intermed_att_agree, intermed_att_pA, \
               p_intermed_att_decision, intermed_att_discrim_pA, intermed_att_discrim_time_mean, \
               intermed_att_discrim_pFailed, intermed_att_discrim_diff_relA

    def calcRewardRates(self, p_A, p_failed, failed_random=True):
        """
        Helper function that calculates a number of reward metrics.
        :param p_A: vector. proportion of trials where "A" was chosen.
        :param p_failed: Vector. Proportion of trials where a decision was not reached.
        :param failed_random: True/False. Sometimes, the network doesn't reach a decision. When that happens, the trial
                              can either be 'failed," or the decision can be made randomly. (default=False)
        :return:
        """
        A = np.sum(self.bundles[:, [0, 2]], axis=1)
        B = np.sum(self.bundles[:, [1, 3]], axis=1)
        if failed_random:
            R = (A + B) / 2
        else:
            R = 0
        p_failed[np.isnan(p_failed)] = 1
        p_A = p_A * (1 - p_failed)
        p_A[np.isnan(p_A)] = 0
        p_B = 1 - p_A - p_failed
        comparison = np.concatenate((A.reshape(len(A), 1), B.reshape(len(B), 1)), axis=1)
        # Get the reward obtained
        denom = len(A)
        reward_observed = (np.nansum(A * p_A) + np.nansum(B * p_B) + np.nansum(R * p_failed)) / denom
        reward_max = comparison.max(axis=1).sum() / denom
        reward_min = comparison.min(axis=1).sum() / denom
        # Get the average correct
        I_delete = np.arange(0, len(comparison), 1)[(comparison[:, 0] == comparison[:, 1])]
        comparison = np.delete(comparison, I_delete, axis=0)
        p_A, p_B, p_failed = np.delete(p_A, I_delete), np.delete(p_B, I_delete), np.delete(p_failed, I_delete)
        if failed_random:
            p_A += (p_failed / 2)
            p_B += (p_failed / 2)
        choice_dist = np.concatenate((p_A.reshape(len(p_A), 1), p_B.reshape(len(p_B), 1)), axis=1)
        large = comparison.argmax(axis=1)
        mask = np.concatenate(((1 - large).reshape(len(large), 1), large.reshape(len(large), 1)), axis=1)
        valid = np.isnan(choice_dist[:, 0]) < 1
        denom = len(mask)
        p_correct = (np.dot(mask[valid, 0].transpose(), choice_dist[valid, 0]) + np.dot(mask[valid, 1].transpose(),
                                                                                        choice_dist[valid, 1])) / denom
        return reward_observed, reward_max, reward_min, p_correct

    def saveMaxDeviations(self, file_name='max-like_deviations.pickle'):
        """
        Save method that stores data from simulation results in a pickle file.
        :param file_name: Path and name of the file (default='max-like_deviations.pickle')
        :return: 0 if successful, 1 if not.
        """
        save_dict = {'P': self.P,
                     'bundles': self.bundles,
                     'p_A': self.p_A,
                     'p_failed': self.p_failed,
                     'decision_time_mean': self.decision_time_mean,
                     'reward_observed': self.reward_observed,
                     'reward_max': self.reward_max,
                     'reward_min': self.reward_min,
                     'p_correct': self.p_correct,
                     'intermed_reward_observed': self.intermed_reward_observed,
                     'intermed_reward_max': self.intermed_reward_max,
                     'intermed_reward_min': self.intermed_reward_min,
                     'intermed_p_correct': self.intermed_p_correct,
                     'intermed_time_gain': self.intermed_time_gain,
                     'intermed_agree': self.intermed_agree,
                     'intermed_pA': self.intermed_pA,
                     'intermed_att_time_gain': self.intermed_att_time_gain,
                     'intermed_att_agree': self.intermed_att_agree,
                     'intermed_att_pA': self.intermed_att_pA,
                     'p_intermed_att_decision': self.p_intermed_att_decision,
                     'p_intermed_decision': self.p_intermed_decision,
                     'intermed_att_discrim_pA': self.intermed_att_discrim_pA,
                     'intermed_att_discrim_time_mean': self.intermed_att_discrim_time_mean,
                     'intermed_att_discrim_pFailed': self.intermed_att_discrim_pFailed,
                     'intermed_att_discrim_diff_relA': self.intermed_att_discrim_diff_relA,
                     'intermed_att_reward_observed': self.intermed_att_reward_observed,
                     'intermed_att_reward_max': self.intermed_att_reward_max,
                     'intermed_att_reward_min': self.intermed_att_reward_min,
                     'intermed_att_p_correct': self.intermed_att_p_correct,
                     }
        try:
            saveData(file_name=file_name, save_dict=save_dict)
        except:
            return 1
        return 0

    def loadMaxDeviations(self, file_name='reward_rate.pickle'):
        """
        Loads results from prior simulation into the object
        :param file_name: Path and name of data file (default='max-like_deviations.pickle')
        :return: 0 if successful, 1 if not
        """
        try:
            save_dict = loadData(file_name=file_name)
        except:
            return 1
        # Update variables with saved values
        self.P = save_dict['P']
        self.bundles = save_dict['bundles']
        self.p_A = save_dict['p_A']
        self.decision_time_mean = save_dict['decision_time_mean']
        self.reward_observed = save_dict['reward_observed']
        self.reward_max = save_dict['reward_max']
        self.reward_min = save_dict['reward_min']
        self.p_correct = save_dict['p_correct']
        self.intermed_time_gain = save_dict['intermed_time_gain']
        self.intermed_agree = save_dict['intermed_agree']
        self.p_failed = save_dict['p_failed']
        self.p_intermed_decision = save_dict['p_intermed_decision']
        self.intermed_pA = save_dict['intermed_pA']
        self.intermed_reward_observed = save_dict['intermed_reward_observed']
        self.intermed_reward_max = save_dict['intermed_reward_max']
        self.intermed_reward_min = save_dict['intermed_reward_min']
        self.intermed_p_correct = save_dict['intermed_p_correct']
        self.intermed_att_time_gain = save_dict['intermed_att_time_gain']
        self.intermed_att_agree = save_dict['intermed_att_agree']
        self.intermed_att_pA = save_dict['intermed_att_pA']
        self.p_intermed_att_decision = save_dict['p_intermed_att_decision']
        self.intermed_att_discrim_pA = save_dict['intermed_att_discrim_pA']
        self.intermed_att_discrim_time_mean = save_dict['intermed_att_discrim_time_mean']
        self.intermed_att_discrim_pFailed = save_dict['intermed_att_discrim_pFailed']
        self.intermed_att_discrim_diff_relA = save_dict['intermed_att_discrim_diff_relA']
        self.intermed_att_reward_observed = save_dict['intermed_att_reward_observed']
        self.intermed_att_reward_max = save_dict['intermed_att_reward_max']
        self.intermed_att_reward_min = save_dict['intermed_att_reward_min']
        self.intermed_att_p_correct = save_dict['intermed_att_p_correct']
        # Delete the save_dict
        del save_dict
        return 0


class rewardRate():
    """
    Highly developed class method that runs bundled choice decisionTrial() simulations while allowing the user to vary
    other parameters. Depending on the parameter dictionary provided, it can vary the E/I weight structure, and
    the noise/uncertainty level. It handles both linear and hierarchical network simulations.
    """
    def __init__(self, P=None, bundles=None):
        """
        Class initialization method
        :param P: Parameter dictionary produced by lnParameters() or hnParameters(). This is where one
                  specifies the simulations to be run.
        :param bundles: Matrix of bundles for the simulation. If None provided, it uses the 'bundle_vals' parameter to
                        generate them.
        """
        if P is None:
            print("No parameters given")
            P = hnParameters(bundle_vals=np.arange(15, 17, 1))
        self.P = P
        if bundles is None:
            self.bundles = makeBundles(self.P['bundle_vals'])
        else:
            self.bundles = bundles
        if self.bundles.ndim == 1:
            self.bundles = np.reshape(self.bundles, (1, -1))
        # Determine what simulations are being run, and how to build the variables
        if (len(self.P['excit_range']) > 0) or (len(self.P['inhib_range']) > 0):
            self.vary_weights = True
            self.weight_range = np.array(
                list(product(self.P['excit_range'], self.P['inhib_range'], repeat=1)))  # Excitatory, inhibitory
        else:
            self.weight_range = []
            self.vary_weights = False
        if len(self.P['noise_sigma_range']) > 0:
            self.vary_noise_sigma = True
        else:
            self.vary_noise_sigma = False
        # Build the variables
        if self.vary_weights and self.vary_noise_sigma:
            self.dim_key = ['bundles', 'weights', 'noise']
            self.p_A = np.zeros((self.bundles.shape[0], self.weight_range.shape[0], len(self.P['noise_sigma_range'])))
            if self.P['n_areas'] == 1:
                self.stability_time_mean = np.zeros((self.bundles.shape[0], self.weight_range.shape[0]))
            else:
                self.stability_time_mean = np.zeros(
                    (self.bundles.shape[0], self.weight_range.shape[0], self.P['n_areas'] - 1))
        elif self.vary_weights:
            self.dim_key = ['bundles', 'weights']
            self.p_A = np.zeros((self.bundles.shape[0], self.weight_range.shape[0]))
            if self.P['n_areas'] == 1:
                self.stability_time_mean = self.p_A.copy()
            else:
                self.stability_time_mean = np.zeros(
                    (self.bundles.shape[0], self.weight_range.shape[0], self.P['n_areas'] - 1))
        elif self.vary_noise_sigma:
            self.dim_key = ['bundles', 'noise']
            self.p_A = np.zeros((self.bundles.shape[0], len(self.P['noise_sigma_range'])))
            if self.P['n_areas'] == 1:
                self.stability_time_mean = np.zeros((self.bundles.shape[0]))
            else:
                self.stability_time_mean = np.zeros((self.bundles.shape[0], self.P['n_areas'] - 1))
        else:
            self.dim_key = ['bundles']
            self.p_A = np.zeros((self.bundles.shape[0]))
            if self.P['n_areas'] == 1:
                self.stability_time_mean = np.zeros((self.bundles.shape[0]))
            else:
                self.stability_time_mean = np.zeros((self.bundles.shape[0], self.P['n_areas'] - 1))
        self.decision_time_mean, self.n_trials_stable = self.p_A.copy(), self.stability_time_mean.copy()
        self.p_failed = self.p_A.copy()  # Proportion of trials where no decision was reached
        self.intermed_time_gain = self.p_A.copy()  # Timed gained by reading out through intermediate layer
        self.intermed_agree = self.p_A.copy()  # How closely the intermediate decision matches the actual
        self.intermed_pA = self.p_A.copy()
        self.dt_intermed_readout = self.p_A.copy()  # Highest intermed pop when decision is reached
        self.p_intermed_decision = self.p_A.copy()  # Proportion of time intermed area reaches a decision
        self.intermed_att_time_gain = self.p_A.copy()  # Reading out from attributes separately, time gained
        self.intermed_att_agree = self.p_A.copy()  # Reading out from attributes separately, when it matches decision
        self.intermed_att_pA = self.p_A.copy()  # Reading out from attributes separately, P(A)
        self.p_intermed_att_decision = self.p_A.copy()  # Reading out from attributes separately, P(decision reached)
        # Variables with reward results
        self.reward_observed = np.nan
        self.reward_max = np.nan
        self.reward_min = np.nan
        self.p_correct = np.nan
        self.intermed_reward_observed = np.nan
        self.intermed_reward_max = np.nan
        self.intermed_reward_min = np.nan
        self.intermed_p_correct = np.nan
        self.intermed_att_reward_observed = np.nan
        self.intermed_att_reward_max = np.nan
        self.intermed_att_reward_min = np.nan
        self.intermed_att_p_correct = np.nan
        self.reward_observed_slope = np.nan
        self.reward_max_slope = np.nan
        self.reward_min_slope = np.nan
        self.p_correct_slope = np.nan
        self.decision_time_slope = np.nan
        self.intermed_reward_observed_slope = np.nan
        self.intermed_reward_max_slope = np.nan
        self.intermed_reward_min_slope = np.nan
        self.intermed_p_correct_slope = np.nan
        self.intermed_decision_time_slope = np.nan
        self.parallelWeights = len(self.weight_range) > self.bundles.shape[0]

    def runRewardRate(self, parallel=True):
        """
        Primary call method for the class. It runs the simulations and records the results.
        :param parallel: True/False. Run the simulation in parallel. If one is varying weights, it automatically
                         determines if parallelizing weights or bundles is more efficient.
        :return:
        """
        if parallel:
            if self.parallelWeights:
                self.runParallelWeights()
                # There are more weight combinations than bundles
            else:
                # If there are more bundles than weights
                self.runParallelBundles()
            # Calculate values over bundles
            self.reward_observed, self.reward_max, self.reward_min, self.p_correct = self.calcRewardRates()
            if self.vary_noise_sigma:
                self.reward_observed_slope, self.reward_max_slope, self.reward_min_slope, \
                self.p_correct_slope, self.decision_time_slope = self.calcNoiseSlope()
        else:
            raise ValueError("Non-Parallel is not yet implemented")

    def runParallelWeights(self):
        """
        Helper function that runs the simulations, using the weights for parallel processing.
        :return:
        """
        print("Parallel Weights")
        """Parallelization occurs with the weights"""
        for c in range(self.bundles.shape[0]):
            p_A, decision_time, stability_time, intermed_time_gain, intermed_agree, p_failed, \
            dt_intermed_readout, p_intermed_decision, intermed_pA, intermed_att_time_gain, \
            intermed_att_agree, intermed_att_pA, p_intermed_att_decision = self.varyWeightCall(self.bundles[c, :])
            if self.vary_noise_sigma:
                self.p_A[c, :, :] = p_A
                self.decision_time_mean[c, :, :] = np.nanmean(decision_time, axis=1)
                self.p_failed[c, :, :] = p_failed
                if self.P['n_areas'] > 1:
                    self.intermed_time_gain[c, :, :] = intermed_time_gain
                    self.intermed_agree[c, :, :] = intermed_agree
                    self.dt_intermed_readout[c, :, :] = dt_intermed_readout
                    self.p_intermed_decision[c, :, :] = p_intermed_decision
                    self.intermed_pA[c, :, :] = intermed_pA
                    self.intermed_att_time_gain[c, :, :] = intermed_att_time_gain
                    self.intermed_att_agree[c, :, :] = intermed_att_agree
                    self.intermed_att_pA[c, :, :] = intermed_att_pA
                    self.p_intermed_att_decision[c, :, :] = p_intermed_att_decision
            else:
                self.p_A[c, :] = p_A.flatten()
                self.decision_time_mean[c, :] = np.nanmean(decision_time, axis=1)
                self.p_failed[c, :] = p_failed.flatten()
                if self.P['n_areas'] > 1:
                    self.intermed_time_gain[c, :] = intermed_time_gain.flatten()
                    self.intermed_agree[c, :] = intermed_agree.flatten()
                    self.dt_intermed_readout[c, :] = dt_intermed_readout.flatten()
                    self.p_intermed_decision[c, :] = p_intermed_decision.flatten()
                    self.intermed_pA[c, :] = intermed_pA.flatten()
                    self.intermed_att_time_gain[c, :] = intermed_att_time_gain.flatten()
                    self.intermed_att_agree[c, :] = intermed_att_agree.flatten()
                    self.intermed_att_pA[c, :] = intermed_att_pA.flatten()
                    self.p_intermed_att_decision[c, :] = p_intermed_att_decision.flatten()
            if self.P['n_areas'] == 1:
                self.stability_time_mean[c, :] = np.nanmean(stability_time, axis=1)
                self.n_trials_stable[c, :] = np.sum(np.isnan(stability_time) < 1, axis=1)
            else:
                self.stability_time_mean[c, :, :] = np.nanmean(stability_time, axis=2)
                self.n_trials_stable[c, :, :] = np.sum(np.isnan(stability_time) < 1, axis=2)

    def runParallelBundles(self):
        """
        Helper function that runs the simulations, using the bundles for parallel processing.
        :return:
        """
        print("Parallel Bundles")
        pool = getPool()
        if self.vary_weights:
            results = pool.map(self.varyWeightCall, self.bundles)
            for c in range(self.bundles.shape[0]):
                if self.vary_noise_sigma:
                    self.p_A[c, :, :] = results[c][0]
                    self.decision_time_mean[c, :, :] = np.nanmean(results[c][1], axis=1)
                    self.p_failed[c, :, :] = results[c][5]
                    if self.P['n_areas'] > 1:
                        self.intermed_time_gain[c, :, :] = results[c][3]
                        self.intermed_agree[c, :, :] = results[c][4]
                        self.dt_intermed_readout[c, :, :] = results[c][6]
                        self.p_intermed_decision[c, :, :] = results[c][7]
                        self.intermed_pA[c, :, :] = results[c][8]
                        self.intermed_att_time_gain[c, :, :] = results[c][9]
                        self.intermed_att_agree[c, :, :] = results[c][10]
                        self.intermed_att_pA[c, :, :] = results[c][11]
                        self.p_intermed_att_decision[c, :, :] = results[c][12]
                else:
                    self.p_A[c, :] = results[c][0].flatten()
                    self.decision_time_mean[c, :] = np.nanmean(results[c][1], axis=1)
                    self.p_failed[c, :] = results[c][5].flatten()
                    if self.P['n_areas'] > 1:
                        self.intermed_time_gain[c, :] = results[c][3].flatten()
                        self.intermed_agree[c, :] = results[c][4].flatten()
                        self.dt_intermed_readout[c, :] = results[c][6].flatten()
                        self.p_intermed_decision[c, :] = results[c][7].flatten()
                        self.intermed_pA[c, :] = results[c][8].flatten()
                        self.intermed_att_time_gain[c, :] = results[c][9].flatten()
                        self.intermed_att_agree[c, :] = results[c][10].flatten()
                        self.intermed_att_pA[c, :] = results[c][11].flatten()
                        self.p_intermed_att_decision[c, :] = results[c][12].flatten()
                if self.P['n_areas'] == 1:
                    self.stability_time_mean[c, :] = np.nanmean(results[c][2], axis=1)
                    self.n_trials_stable[c, :] = np.sum(np.isnan(results[c][2]) < 1, axis=1)
                else:
                    self.stability_time_mean[c, :, :] = np.nanmean(results[c][2], axis=2)
                    self.n_trials_stable[c, :, :] = np.sum(np.isnan(results[c][2]) < 1, axis=2)
        elif self.vary_noise_sigma:
            results = pool.map(self.varyNoiseCall, self.bundles)
            for c in range(self.bundles.shape[0]):
                self.p_A[c, :] = results[c][0].flatten()
                self.decision_time_mean[c, :] = np.nanmean(results[c][1], axis=1)
                self.p_failed[c, :] = results[c][5].flatten()
                if self.P['n_areas'] == 1:
                    self.stability_time_mean[c] = np.nanmean(results[c][2], axis=0)
                    self.n_trials_stable[c] = np.sum(np.isnan(results[c][2]) < 1, axis=0)
                else:
                    self.stability_time_mean[c, :] = np.nanmean(results[c][2], axis=1)
                    self.n_trials_stable[c, :] = np.sum(np.isnan(results[c][2]) < 1, axis=1)
                    self.intermed_time_gain[c, :] = results[c][3].flatten()
                    self.intermed_agree[c, :] = results[c][4].flatten()
                    self.dt_intermed_readout[c, :] = results[c][6].flatten()
                    self.p_intermed_decision[c, :] = results[c][7].flatten()
                    self.intermed_pA[c, :] = results[c][8].flatten()
                    self.intermed_att_time_gain[c, :] = results[c][9].flatten()
                    self.intermed_att_agree[c, :] = results[c][10].flatten()
                    self.intermed_att_pA[c, :] = results[c][11].flatten()
                    self.p_intermed_att_decision[c, :] = results[c][12].flatten()
        else:
            results = pool.map(self.decisionTrialCall, self.bundles)
            for c in range(self.bundles.shape[0]):
                self.p_A[c] = results[c][0]
                self.decision_time_mean[c] = np.nanmean(results[c][1].flatten())
                self.p_failed[c] = results[c][5]
                if self.P['n_areas'] == 1:
                    self.stability_time_mean[c] = np.nanmean(results[c][2], axis=0)
                    self.n_trials_stable[c] = np.sum(np.isnan(results[c][2]) < 1, axis=0)
                else:
                    self.stability_time_mean[c, :] = np.nanmean(results[c][2], axis=1)
                    self.n_trials_stable[c, :] = np.sum(np.isnan(results[c][2]) < 1, axis=1)
                    self.intermed_time_gain[c] = results[c][3]
                    self.intermed_agree[c] = results[c][4]
                    self.dt_intermed_readout[c] = results[c][6]
                    self.p_intermed_decision[c] = results[c][7]
                    self.intermed_pA[c] = results[c][8]
                    self.intermed_att_time_gain[c] = results[c][9]
                    self.intermed_att_agree[c] = results[c][10]
                    self.intermed_att_pA[c] = results[c][11]
                    self.p_intermed_att_decision[c] = results[c][12]

    def changeMu(self, input):
        """
        Helper method that updates the input vector for the simulation runs
        :param input: 4x1 vector of floats. Represents firing rates for a bundle.
        :return: P. The updated parameters dictionary
        """
        Par = copy.deepcopy(self.P)
        print('Simulating Bundle ' + str(input))
        Par['mu'] = np.zeros((6, 1))
        Par['mu'][0:4] = input.reshape(4, 1)
        Par['mu'] = Par['mu'] * brian2.Hz
        return Par

    def varyWeightCall(self, input):
        """
        Helper method used when the parameters call for varying weights. It also handles if noise/uncertainty is
        simultaniously varied.
        :param input: Vector of inputs for the network representing bundles
        :return: p_A, decision_time, stability_time, intermed_time_gain, intermed_agree, p_failed, \
               dt_intermed_readout, p_intermed_decision, intermed_pA, intermed_att_time_gain, \
               intermed_att_agree, intermed_att_pA, p_intermed_att_decision
        """
        Par = self.changeMu(input)
        vary_EI = varyEI(Par)
        vary_EI.runVaryEI(parallel=self.parallelWeights)
        p_A, decision_time, stability_time, intermed_time_gain, intermed_agree, p_failed, dt_intermed_readout, \
        p_intermed_decision, intermed_pA, intermed_att_time_gain, intermed_att_agree, intermed_att_pA, \
        p_intermed_att_decision = \
            vary_EI.p_A, vary_EI.decision_time, vary_EI.stability_time, vary_EI.intermed_time_gain, \
            vary_EI.intermed_agree, vary_EI.p_failed, vary_EI.dt_intermed_readout, vary_EI.p_intermed_decision, \
            vary_EI.intermed_pA, vary_EI.intermed_att_time_gain, vary_EI.intermed_att_agree, vary_EI.intermed_att_pA, \
            vary_EI.p_intermed_att_decision
        del vary_EI
        return p_A, decision_time, stability_time, intermed_time_gain, intermed_agree, p_failed, \
               dt_intermed_readout, p_intermed_decision, intermed_pA, intermed_att_time_gain, \
               intermed_att_agree, intermed_att_pA, p_intermed_att_decision

    def varyNoiseCall(self, input):
        """
        Helper method used when the parameters call for varying the noise/uncertainty level.
        :param input: Vector of inputs for the network representing bundles
        :return: p_A, decision_time, stability_time, intermed_time_gain, intermed_agree, p_failed, \
               dt_intermed_readout, p_intermed_decision, intermed_pA, intermed_att_time_gain, \
               intermed_att_agree, intermed_att_pA, p_intermed_att_decision
        """
        Par = self.changeMu(input)
        # Run the trials and collect the results
        vary_noise = varyNoiseSigma(Par)
        vary_noise.runVaryNoise(parallel=False)
        p_A, decision_time, stability_time, p_failed = vary_noise.p_A, vary_noise.decision_time, \
                                                       vary_noise.stability_time, vary_noise.p_failed
        if self.P['n_areas'] > 1:
            intermed_time_gain, intermed_agree, dt_intermed_readout, p_intermed_decision, intermed_pA, intermed_att_time_gain, \
            intermed_att_agree, intermed_att_pA, p_intermed_att_decision = \
                vary_noise.intermed_time_gain, vary_noise.intermed_agree, vary_noise.dt_intermed_readout, \
                vary_noise.p_intermed_decision, vary_noise.intermed_pA, vary_noise.intermed_att_time_gain, \
                vary_noise.intermed_att_agree, vary_noise.intermed_att_pA, vary_noise.p_intermed_att_decision
        else:
            intermed_time_gain, intermed_agree, dt_intermed_readout, p_intermed_decision, intermed_pA, intermed_att_time_gain, \
            intermed_att_agree, intermed_att_pA, p_intermed_att_decision = \
                None, None, None, None, None, None, None, None, None
        del vary_noise
        return p_A, decision_time, stability_time, intermed_time_gain, intermed_agree, p_failed, \
               dt_intermed_readout, p_intermed_decision, intermed_pA, intermed_att_time_gain, \
               intermed_att_agree, intermed_att_pA, p_intermed_att_decision

    def decisionTrialCall(self, input):
        """
        Helper function that calls the decisionTrial simulation and performs a number of secondary analysis
        :param input: 4x1 vector of floats. Represents firing rates for a bundle.
        :return: p_A, decision_time, stability_time, intermed_time_gain, intermed_agree, p_failed, dt_intermed_readout,
               p_intermed_decision, intermed_pA, intermed_att_time_gain, intermed_att_agree, intermed_att_pA,
               p_intermed_att_decision
        """
        Par = self.changeMu(input)
        # Run the trials and collect the results
        trials = decisionTrials(Par)
        trials.runTrials()
        if Par['n_areas'] == 1:  # Single area
            decision_time = trials.thresh_cross_time
            p_A = (trials.winners[0, :] > 0).sum() / (Par['n_trials'] - sum(np.isnan(decision_time.flatten())))
            winning_pop, winners, intermed_time_gain, intermed_agree, dt_intermed_readout, p_intermed_decision, \
            intermed_pA, intermed_att_time_gain, intermed_att_agree, intermed_att_pA, p_intermed_att_decision = \
                None, None, None, None, None, None, None, None, None, None, None
        else:
            p_A, winning_pop, winners, decision_time = findWinning(trials)
            intermed_time_gain, intermed_agree, dt_intermed_readout, p_intermed_decision, intermed_pA = calcIntermedLayerGains(
                trials, winners)
            intermed_att_time_gain, intermed_att_agree, intermed_att_pA, p_intermed_att_decision = calcIntermediateAttStats(
                trials, winners)
        stability_time, p_failed = trials.stability_time, np.mean(np.isnan(trials.winners[-1, :]))
        del trials
        return p_A, decision_time, stability_time, intermed_time_gain, intermed_agree, p_failed, dt_intermed_readout, \
               p_intermed_decision, intermed_pA, intermed_att_time_gain, intermed_att_agree, intermed_att_pA, \
               p_intermed_att_decision

    def calcRewardRates(self, p_A=None, p_failed=None, failed_random=True):
        """
        Helper function that calculates a number of reward metrics.
        :param p_A: vector. proportion of trials where "A" was chosen.
        :param p_failed: Vector. Proportion of trials where a decision was not reached.
        :param failed_random: True/False. Sometimes, the network doesn't reach a decision. When that happens, the trial
                              can either be 'failed," or the decision can be made randomly. (default=True)
        :return: reward_observed, reward_max, reward_min, p_correct
        """
        if p_A is None: p_A = self.p_A
        if p_failed is None: p_failed = self.p_failed
        if p_A.ndim == 1:
            reward_observed, reward_max, reward_min, p_correct = self.calcRewardRatesCall(p_A, p_failed)
        elif p_A.ndim == 2:
            reward_observed, reward_max = np.zeros((p_A.shape[1])), np.zeros((p_A.shape[1]))
            reward_min, p_correct = np.zeros((p_A.shape[1])), np.zeros((p_A.shape[1]))
            for c in range(p_A.shape[1]):
                reward_observed[c], reward_max[c], reward_min[c], p_correct[c] = \
                    self.calcRewardRatesCall(p_A[:, c], p_failed[:, c], failed_random=failed_random)
        elif p_A.ndim == 3:
            d1, d2 = p_A.shape[1], p_A.shape[2]
            reward_observed, reward_max = np.zeros((d1, d2)), np.zeros((d1, d2))
            reward_min, p_correct = np.zeros((d1, d2)), np.zeros((d1, d2))
            for c in range(d1):
                for n in range(d2):
                    reward_observed[c, n], reward_max[c, n], reward_min[c, n], p_correct[c, n] = \
                        self.calcRewardRatesCall(p_A[:, c, n], p_failed[:, c, n], failed_random=failed_random)
        else:
            raise ValueError('Issue with dimensionality of p_A')
        return reward_observed, reward_max, reward_min, p_correct

    def calcNoiseSlope(self, intermed=False):
        """
        Helper method that analyzes simulation results to calculate various reward metrics as a function of
        noise/uncertainty level.
        :param intermed: True/False. Whether to calculate metrics for intermediate-layer area readout (True), or readout
                         from the final area (False).
        :return:
        """
        if intermed:  # calculating values for intermediate=layer area
            if type(self.intermed_reward_observed) == float:
                if np.isnan(self.intermed_reward_observed): raise ValueError('Must first calculate the reward rates')
            if self.intermed_reward_observed.ndim == 1:
                reward_observed_slope = self.fitSlope(self.P['noise_sigma_range'], self.intermed_reward_observed)
                reward_min_slope = self.fitSlope(self.P['noise_sigma_range'], self.intermed_reward_min)
                reward_max_slope = self.fitSlope(self.P['noise_sigma_range'], self.intermed_reward_max)
                p_correct_slope = self.fitSlope(self.P['noise_sigma_range'], self.intermed_p_correct)
                decision_time_slope = self.fitSlope(self.P['noise_sigma_range'],
                                                    np.nanmean(self.decision_time_mean - self.intermed_time_gain,
                                                               axis=0))
                reward_max_slope, reward_min_slope, p_correct_slope
            elif self.intermed_reward_observed.ndim == 2:
                dm = self.intermed_reward_observed.shape[0]
                reward_observed_slope, reward_min_slope = np.zeros((dm)), np.zeros((dm))
                reward_max_slope, p_correct_slope, decision_time_slope = np.zeros((dm)), np.zeros((dm)), np.zeros((dm))
                for c in range(self.intermed_reward_observed.shape[0]):
                    reward_observed_slope[c] = self.fitSlope(self.P['noise_sigma_range'],
                                                             self.intermed_reward_observed[c, :])
                    reward_min_slope[c] = self.fitSlope(self.P['noise_sigma_range'], self.intermed_reward_min[c, :])
                    reward_max_slope[c] = self.fitSlope(self.P['noise_sigma_range'], self.intermed_reward_max[c, :])
                    p_correct_slope[c] = self.fitSlope(self.P['noise_sigma_range'], self.intermed_p_correct[c, :])
                    decision_time_slope[c] = self.fitSlope(self.P['noise_sigma_range'],
                                                           np.nanmean(self.decision_time_mean - self.intermed_time_gain,
                                                                      axis=0)[c, :])
            else:
                raise ValueError('Issue with dimensionality of reward rates')
        else:  # Calculating values for decision-area
            if type(self.reward_observed) == float:
                if np.isnan(self.reward_observed): raise ValueError('Must first calculate the reward rates')
            if self.reward_observed.ndim == 1:
                reward_observed_slope = self.fitSlope(self.P['noise_sigma_range'], self.reward_observed)
                reward_min_slope = self.fitSlope(self.P['noise_sigma_range'], self.reward_min)
                reward_max_slope = self.fitSlope(self.P['noise_sigma_range'], self.reward_max)
                p_correct_slope = self.fitSlope(self.P['noise_sigma_range'], self.p_correct)
                decision_time_slope = self.fitSlope(self.P['noise_sigma_range'],
                                                    np.nanmean(self.decision_time_mean, axis=0))
                reward_max_slope, reward_min_slope, p_correct_slope
            elif self.reward_observed.ndim == 2:
                dm = self.reward_observed.shape[0]
                reward_observed_slope, reward_min_slope = np.zeros((dm)), np.zeros((dm))
                reward_max_slope, p_correct_slope, decision_time_slope = np.zeros((dm)), np.zeros((dm)), np.zeros((dm))
                for c in range(self.reward_observed.shape[0]):
                    reward_observed_slope[c] = self.fitSlope(self.P['noise_sigma_range'], self.reward_observed[c, :])
                    reward_min_slope[c] = self.fitSlope(self.P['noise_sigma_range'], self.reward_min[c, :])
                    reward_max_slope[c] = self.fitSlope(self.P['noise_sigma_range'], self.reward_max[c, :])
                    p_correct_slope[c] = self.fitSlope(self.P['noise_sigma_range'], self.p_correct[c, :])
                    decision_time_slope[c] = self.fitSlope(self.P['noise_sigma_range'],
                                                           np.nanmean(self.decision_time_mean, axis=0)[c, :])
            else:
                raise ValueError('Issue with dimensionality of reward rates')
        return reward_observed_slope, reward_max_slope, reward_min_slope, p_correct_slope, decision_time_slope

    def fitSlope(self, x, y):
        """
        Helper method that simply fits the slope, indicating the relationship of two variables.
        :param x: vector
        :param y: vector
        :return: slope (no intercept)
        """
        try:
            poly_w = np.polyfit(x, y, 1)
            slope = poly_w[0]
        except:
            slope = np.nan
        return slope

    def calcRewardRatesCall(self, p_A, p_failed, failed_random=True):
        """
        Helper method for calcRewardRates() that does the meat of computations.
        :param p_A: vector. proportion of trials where "A" was chosen.
        :param p_failed: Vector. Proportion of trials where a decision was not reached.
        :param failed_random: True/False. Sometimes, the network doesn't reach a decision. When that happens, the trial
                              can either be 'failed," or the decision can be made randomly. (default=True)
        :return: reward_observed, reward_max, reward_min, p_correct
        """
        A = np.sum(self.bundles[:, [0, 2]], axis=1)
        B = np.sum(self.bundles[:, [1, 3]], axis=1)
        if failed_random:
            R = (A + B) / 2
        else:
            R = 0
        p_failed[np.isnan(p_failed)] = 1
        p_A = p_A * (1 - p_failed)
        p_A[np.isnan(p_A)] = 0
        p_B = 1 - p_A - p_failed
        comparison = np.concatenate((A.reshape(len(A), 1), B.reshape(len(B), 1)), axis=1)
        # Get the reward obtained
        denom = len(A)
        reward_observed = (np.nansum(A * p_A) + np.nansum(B * p_B) + np.nansum(R * p_failed)) / denom
        reward_max = comparison.max(axis=1).sum() / denom
        reward_min = comparison.min(axis=1).sum() / denom
        # Get the average correct
        I_delete = np.arange(0, len(comparison), 1)[(comparison[:, 0] == comparison[:, 1])]
        comparison = np.delete(comparison, I_delete, axis=0)
        p_A, p_B, p_failed = np.delete(p_A, I_delete), np.delete(p_B, I_delete), np.delete(p_failed, I_delete)
        if failed_random:
            p_A += (p_failed / 2)
            p_B += (p_failed / 2)
        choice_dist = np.concatenate((p_A.reshape(len(p_A), 1), p_B.reshape(len(p_B), 1)), axis=1)
        large = comparison.argmax(axis=1)
        mask = np.concatenate(((1 - large).reshape(len(large), 1), large.reshape(len(large), 1)), axis=1)
        valid = np.isnan(choice_dist[:, 0]) < 1
        denom = len(mask)
        p_correct = (np.dot(mask[valid, 0].transpose(), choice_dist[valid, 0]) + np.dot(mask[valid, 1].transpose(),
                                                                                        choice_dist[valid, 1])) / denom
        return reward_observed, reward_max, reward_min, p_correct

    def joinClusterFiles(self, dir_name='pickles/', file_name_base='reward_rate', failed_random=True,
                         failed_consistent=False):
        """
        User-called method that joins simulation files produced by the cluster, creating one unified set of results.
        :param dir_name: File directory
        :param file_name_base: Filename base shared across all relevant files
        :param failed_random: True/False. Aids in backward-compatibility (science produces rabbit dens of code)
                              (default=True)
        :param failed_consistent: True/False. Aids in backward-compatibility (default=True)
        :return:
        """
        """ Takes all the cluster babies and joins them together"""
        f_names = glob(dir_name + file_name_base + '*')
        reward_rate = rewardRate(self.P, self.bundles)
        if len(f_names) == 0:
            raise ValueError('No files found with that base')
        for f in range(len(f_names)):
            reward_rate.loadRewardRate(f_names[f])
            if f == 0:
                # Get the basics
                self.P = reward_rate.P
                self.vary_weights = reward_rate.vary_weights
                self.vary_noise_sigma = reward_rate.vary_noise_sigma
                if reward_rate.vary_weights:
                    self.weight_range = reward_rate.weight_range
                # Start the variables that will build
                bundles = copy.deepcopy(reward_rate.bundles)
                p_A = copy.deepcopy(reward_rate.p_A)
                p_failed = copy.deepcopy(reward_rate.p_failed)
                decision_time_mean = copy.deepcopy(reward_rate.decision_time_mean)
                stability_time_mean = copy.deepcopy(reward_rate.stability_time_mean)
                n_trials_stable = copy.deepcopy(reward_rate.n_trials_stable)
                if hasattr(reward_rate, 'intermed_time_gain'):  # Variable added later, so make it backward-compatible
                    intermed_time_gain = copy.deepcopy(reward_rate.intermed_time_gain)
                    intermed_agree = copy.deepcopy(reward_rate.intermed_agree)
                    dt_intermed_readout = copy.deepcopy(reward_rate.dt_intermed_readout)
                    p_intermed_decision = copy.deepcopy(reward_rate.p_intermed_decision)
                    intermed_pA = copy.deepcopy(reward_rate.intermed_pA)
                if hasattr(reward_rate, 'intermed_att_time_gain'):
                    intermed_att_time_gain = copy.deepcopy(reward_rate.intermed_att_time_gain)
                    intermed_att_agree = copy.deepcopy(reward_rate.intermed_att_time_gain)
                    intermed_att_pA = copy.deepcopy(reward_rate.intermed_att_time_gain)
                    p_intermed_att_decision = copy.deepcopy(reward_rate.intermed_att_time_gain)
                else:
                    print('Problem with the attributes of the files you are loading')
            else:
                bundles = np.concatenate((bundles, reward_rate.bundles), axis=0)
                p_A = np.concatenate((p_A, reward_rate.p_A), axis=0)
                p_failed = np.concatenate((p_failed, reward_rate.p_failed), axis=0)
                decision_time_mean = np.concatenate((decision_time_mean, reward_rate.decision_time_mean), axis=0)
                stability_time_mean = np.concatenate((stability_time_mean, reward_rate.stability_time_mean), axis=0)
                n_trials_stable = np.concatenate((n_trials_stable, reward_rate.n_trials_stable), axis=0)
                if hasattr(reward_rate, 'intermed_time_gain'):  # Variable added later, so make it backward-compatible
                    intermed_time_gain = np.concatenate((intermed_time_gain, reward_rate.intermed_time_gain), axis=0)
                    intermed_agree = np.concatenate((intermed_agree, reward_rate.intermed_agree), axis=0)
                    dt_intermed_readout = np.concatenate((dt_intermed_readout, reward_rate.dt_intermed_readout), axis=0)
                    p_intermed_decision = np.concatenate((p_intermed_decision, reward_rate.p_intermed_decision), axis=0)
                    intermed_pA = np.concatenate((intermed_pA, reward_rate.intermed_pA), axis=0)
                else:
                    print('Problem with the attributes of the files you are loading')
                if hasattr(reward_rate, 'intermed_att_time_gain'):
                    intermed_att_time_gain = np.concatenate(
                        (intermed_att_time_gain, reward_rate.intermed_att_time_gain), axis=0)
                    intermed_att_agree = np.concatenate((intermed_att_agree, reward_rate.intermed_att_agree), axis=0)
                    intermed_att_pA = np.concatenate((intermed_att_pA, reward_rate.intermed_att_pA), axis=0)
                    p_intermed_att_decision = np.concatenate(
                        (p_intermed_att_decision, reward_rate.p_intermed_att_decision), axis=0)
        if failed_consistent: p_A = self.failedConsistent(p_A)
        # Sort by bundle order
        indx_bunds = argSortMatRows(bundles)
        self.bundles = bundles[indx_bunds, :]
        self.p_A = p_A[indx_bunds, :]
        self.decision_time_mean = decision_time_mean[indx_bunds, :]
        self.p_failed = p_failed[indx_bunds, :]
        self.stability_time_mean = stability_time_mean[indx_bunds, :]
        self.n_trials_stable = n_trials_stable[indx_bunds, :]
        self.reward_observed, self.reward_max, self.reward_min, self.p_correct = self.calcRewardRates(
            failed_random=failed_random)
        self.reward_observed_slope, self.reward_max_slope, self.reward_min_slope, self.p_correct_slope, \
        self.decision_time_slope = self.calcNoiseSlope()
        if hasattr(reward_rate, 'intermed_att_time_gain'):
            self.intermed_att_time_gain = intermed_att_time_gain[indx_bunds, :]
            self.intermed_att_agree = intermed_att_agree[indx_bunds, :]
            self.intermed_att_pA = intermed_att_pA[indx_bunds, :]
            self.p_intermed_att_decision = p_intermed_att_decision[indx_bunds, :]
            self.intermed_att_reward_observed, self.intermed_att_reward_max, self.intermed_att_reward_min, \
            self.intermed_att_p_correct = \
                self.calcRewardRates(p_A=self.intermed_att_pA, p_failed=(1 - self.p_intermed_att_decision),
                                     failed_random=failed_random)

    def failedConsistent(self, p_A):
        """
        Helper method that makes sure if weight failed a bundle at one noise level, fails at all noise levels.
        :param p_A: matrix of p_A values
        :return: p_A, corrected
        """
        if p_A.ndim == 3:  # hierarchical network
            for w in range(p_A.shape[1]):
                n_nan = np.sum(np.isnan(p_A[:, w, :]), axis=1)
                p_A[n_nan > 0, w, :] = np.nan
        else:  # Single decision
            n_nan = np.sum(np.isnan(p_A[:, :]), axis=1)
            p_A[n_nan > 0, :] = np.nan
        return p_A

    def saveRewardRate(self, file_name='reward_rate.pickle'):
        """
        Save method that stores data from simulation results in a pickle file.
        :param file_name: Path and name of the file (default='reward_rate.pickle')
        :return: 0 if successful, 1 if not.
        """
        save_dict = {'P': self.P,
                     'dim_key': self.dim_key,
                     'bundles': self.bundles,
                     'p_A': self.p_A,
                     'p_failed': self.p_failed,
                     'decision_time_mean': self.decision_time_mean,
                     'stability_time_mean': self.stability_time_mean,
                     'n_trials_stable': self.n_trials_stable,
                     'vary_weights': self.vary_weights,
                     'vary_noise_sigma': self.vary_noise_sigma,
                     'reward_observed': self.reward_observed,
                     'reward_max': self.reward_max,
                     'reward_min': self.reward_min,
                     'p_correct': self.p_correct,
                     'reward_observed_slope': self.reward_observed_slope,
                     'reward_max_slope': self.reward_max_slope,
                     'reward_min_slope': self.reward_min_slope,
                     'p_correct_slope': self.p_correct_slope,
                     'decision_time_slope': self.decision_time_slope,
                     'intermed_reward_observed': self.intermed_reward_observed,
                     'intermed_reward_max': self.intermed_reward_max,
                     'intermed_reward_min': self.intermed_reward_min,
                     'intermed_p_correct': self.intermed_p_correct,
                     'intermed_reward_observed_slope': self.intermed_reward_observed_slope,
                     'intermed_reward_max_slope': self.intermed_reward_max_slope,
                     'intermed_reward_min_slope': self.intermed_reward_min_slope,
                     'intermed_p_correct_slope': self.intermed_p_correct_slope,
                     'intermed_decision_time_slope': self.intermed_decision_time_slope,
                     'intermed_time_gain': self.intermed_time_gain,
                     'intermed_agree': self.intermed_agree,
                     'intermed_pA': self.intermed_pA,
                     'intermed_att_time_gain': self.intermed_att_time_gain,
                     'intermed_att_agree': self.intermed_att_agree,
                     'intermed_att_pA': self.intermed_att_pA,
                     'intermed_att_reward_observed': self.intermed_att_reward_observed,
                     'intermed_att_reward_max': self.intermed_att_reward_max,
                     'intermed_att_reward_min': self.intermed_att_reward_min,
                     'intermed_att_p_correct': self.intermed_att_p_correct,
                     'p_intermed_att_decision': self.p_intermed_att_decision,
                     'p_intermed_decision': self.p_intermed_decision,
                     'dt_intermed_readout': self.dt_intermed_readout
                     }
        if self.vary_weights:
            save_dict['weight_range'] = self.weight_range
        try:
            saveData(file_name=file_name, save_dict=save_dict)
        except:
            return 1
        return 0

    def loadRewardRate(self, file_name='reward_rate.pickle'):
        """
        Loads results from prior simulation into the object
        :param file_name: Path and name of data file (default='reward_rate.pickle')
        :return: 0 if successful, 1 if not
        """
        try:
            save_dict = loadData(file_name=file_name)
            # Update variables with saved values
            self.P = save_dict['P']
            self.dim_key = save_dict['dim_key']
            self.bundles = save_dict['bundles']
            self.p_A = save_dict['p_A']
            self.decision_time_mean = save_dict['decision_time_mean']
            self.vary_weights = save_dict['vary_weights']
            self.vary_noise_sigma = save_dict['vary_noise_sigma']
            self.reward_observed = save_dict['reward_observed']
            self.reward_max = save_dict['reward_max']
            self.reward_min = save_dict['reward_min']
            self.p_correct = save_dict['p_correct']
            if 'reward_observed_slope' in save_dict:
                self.reward_observed_slope = save_dict['reward_observed_slope']
                self.reward_max_slope = save_dict['reward_max_slope']
                self.reward_min_slope = save_dict['reward_min_slope']
                self.p_correct_slope = save_dict['p_correct_slope']
            if self.vary_weights:
                self.weight_range = save_dict['weight_range']
            if self.decision_time_slope in save_dict:
                self.decision_time_slope = save_dict['decision_time_slope']
            if 'stability_time_mean' in save_dict:
                self.stability_time_mean = save_dict['stability_time_mean']
                self.n_trials_stable = save_dict['n_trials_stable']
            if 'intermed_time_gain' in save_dict:
                self.intermed_time_gain = save_dict['intermed_time_gain']
                if 'intermed_readout' in save_dict:
                    self.intermed_agree = save_dict['intermed_readout']  # backward compatability
                else:
                    self.intermed_agree = save_dict['intermed_agree']
            if 'p_failed' in save_dict:
                self.p_failed = save_dict['p_failed']
            if 'p_intermed_decision' in save_dict:
                self.p_intermed_decision = save_dict['p_intermed_decision']
                self.dt_intermed_readout = save_dict['dt_intermed_readout']
            if 'intermed_pA' in save_dict:
                self.intermed_pA = save_dict['intermed_pA']
            if 'intermed_p_correct' in save_dict:
                self.intermed_reward_observed = save_dict['intermed_reward_observed']
                self.intermed_reward_max = save_dict['intermed_reward_max']
                self.intermed_reward_min = save_dict['intermed_reward_min']
                self.intermed_p_correct = save_dict['intermed_p_correct']
                self.intermed_reward_observed_slope = save_dict['intermed_reward_observed_slope']
                self.intermed_reward_max_slope = save_dict['intermed_reward_max_slope']
                self.intermed_reward_min_slope = save_dict['intermed_reward_min_slope']
                self.intermed_p_correct_slope = save_dict['intermed_p_correct_slope']
                self.intermed_decision_time_slope = save_dict['intermed_decision_time_slope']
            if 'intermed_att_time_gain' in save_dict:
                self.intermed_att_time_gain = save_dict['intermed_att_time_gain']
                self.intermed_att_agree = save_dict['intermed_att_agree']
                self.intermed_att_pA = save_dict['intermed_att_pA']
                self.p_intermed_att_decision = save_dict['p_intermed_att_decision']
                if 'intermed_att_reward_observed' in save_dict:
                    self.intermed_att_reward_observed = save_dict['intermed_att_reward_observed']
                    self.intermed_att_reward_max = save_dict['intermed_att_reward_max']
                    self.intermed_att_reward_min = save_dict['intermed_att_reward_min']
                    self.intermed_att_p_correct = save_dict['intermed_att_p_correct']
            # Delete the save_dict
            del save_dict
        except:
            return 1
        return 0


class varyEI():
    """
    Class that allows user to systematically vary the excitatory and inhibitory weights of the intermediate layer areas.
    It also can systematically vary the noise/uncertainty level, if that is specified. Function of the class is
    specified controlled using the parameters dictionary. This class is called by other classes, such as rewardRates().
    """
    def __init__(self, P=None):
        """
        Initialization method. Class function is dictated by the input parameters dictionary.
        :param P: Parameter dictionary produced by lnParameters() or hnParameters(). (default=None)
        """
        print('Initializing parameters')
        if P is None:
            print('No parameters given. Initializing hierarchical network with defaults')
            P = hnParameters()
        self.P = P
        self.weight_range = np.array(
            list(product(self.P['excit_range'], self.P['inhib_range'], repeat=1)))  # Excitatory, inhibitory
        if self.P['n_areas'] > 1:  # Only going to store them for the first noise level
            self.stability_time = np.zeros((self.weight_range.shape[0], self.P['n_areas'] - 1, P['n_trials']))
        else:
            self.stability_time = np.zeros((self.weight_range.shape[0], P['n_trials']))
        if len(self.P['noise_sigma_range']) > 0:
            self.vary_noise_sigma = True
            self.p_A = np.zeros((self.weight_range.shape[0], len(self.P['noise_sigma_range'])))
            self.decision_time = np.zeros((self.weight_range.shape[0], P['n_trials'], len(self.P['noise_sigma_range'])))
            self.p_A_slope = np.zeros((self.weight_range.shape[0], 1))
        else:
            self.vary_noise_sigma = False
            self.p_A = np.zeros((self.weight_range.shape[0], 1))
            self.decision_time = np.zeros((self.weight_range.shape[0], P['n_trials']))
        if self.P['n_areas'] > 1:
            self.winning_pop = self.decision_time.copy()
            self.winners = self.decision_time.copy()
            self.intermed_time_gain = self.p_A.copy()  # Timed gained by reading out through intermediate layer
            self.intermed_agree = self.p_A.copy()  # How closely the intermediate decision matches the actual
            self.intermed_pA = self.p_A.copy()
            self.dt_intermed_readout = self.p_A.copy()  # Highest intermed pop when decision is reached
            self.p_intermed_decision = self.p_A.copy()  # Proportion of time intermed area reaches a decision
            self.intermed_att_time_gain = self.p_A.copy()  # Reading out from attributes separately, time gained
            self.intermed_att_agree = self.p_A.copy()  # Reading out from attributes separately, when it matches decision
            self.intermed_att_pA = self.p_A.copy()  # Reading out from attributes separately, P(A)
            self.p_intermed_att_decision = self.p_A.copy()  # Reading out from attributes separately, P(decision reached)
        else:
            self.winning_pop = None
            self.winners = None
            self.intermed_time_gain = None
            self.intermed_agree = None
            self.dt_intermed_readout = None
            self.p_intermed_decision = None
            self.intermed_att_time_gain = None
            self.intermed_att_agree = None
            self.intermed_att_pA = None
            self.p_intermed_att_decision = None
        self.p_failed = self.p_A.copy()

    def runVaryEI(self, parallel=True):
        """
        Main method called by user to run simulation and store results.
        :param parallel: True/False. Whether to run simulations in parallel (default=True)
        :return:
        """
        print('Initiating run of simulations')
        if self.vary_noise_sigma:
            if parallel:
                pool = getPool()
                results = pool.map(self.runVaryEICallNoise, self.weight_range)
                pool.close()
                for c in range(self.weight_range.shape[0]):
                    self.p_A[c, :] = results[c][0].flatten()
                    self.decision_time[c, :, :] = results[c][3].transpose()
                    self.p_A_slope[c] = results[c][4]
                    if self.P['n_areas'] > 1:
                        self.winning_pop[c, :, :] = results[c][1].transpose()
                        self.winners[c, :, :] = results[c][2].transpose()
                        self.stability_time[c, :, :] = results[c][5]
                        self.intermed_time_gain[c, :] = results[c][6].flatten()
                        self.intermed_agree[c, :] = results[c][7].flatten()
                        self.dt_intermed_readout[c, :] = results[c][9].flatten()
                        self.p_intermed_decision[c, :] = results[c][10].flatten()
                        self.intermed_pA[c, :] = results[c][11].flatten()
                        self.intermed_att_time_gain[c, :] = results[c][12].flatten()
                        self.intermed_att_agree[c, :] = results[c][13].flatten()
                        self.intermed_att_pA[c, :] = results[c][14].flatten()
                        self.p_intermed_att_decision[c, :] = results[c][15].flatten()
                    else:
                        self.stability_time[c, :] = results[c][5]
                    self.p_failed[c, :] = results[c][8].flatten()
            else:
                for c in range(self.weight_range.shape[0]):
                    p_A, winning_pop, winners, decision_time, self.p_A_slope[c], stability_time, intermed_time_gain, \
                    intermed_agree, p_failed, dt_intermed_readout, p_intermed_decision, intermed_pA, \
                    intermed_att_time_gain, intermed_att_agree, intermed_att_pA, p_intermed_att_decision = \
                        self.runVaryEICallNoise(self.weight_range[c, :])
                    self.p_A[c, :], self.decision_time[c, :, :], self.p_failed[c, :], = p_A.flatten(), \
                                                                                        decision_time.transpose(), p_failed.flatten()
                    if self.P['n_areas'] > 1:
                        self.winning_pop[c, :, :] = winning_pop.transpose()
                        self.winners[c, :, :] = winners.transpose()
                        self.stability_time[c, :, :] = stability_time
                        self.intermed_time_gain[c, :] = intermed_time_gain.flatten()
                        self.intermed_agree[c, :] = intermed_agree.flatten()
                        self.dt_intermed_readout[c, :] = dt_intermed_readout.flatten()
                        self.p_intermed_decision[c, :] = p_intermed_decision.flatten()
                        self.intermed_pA[c, :] = intermed_pA.flatten()
                        self.intermed_att_time_gain[c, :] = intermed_att_time_gain.flatten()
                        self.intermed_att_agree[c, :] = intermed_att_agree.flatten()
                        self.intermed_att_pA[c, :] = intermed_att_pA.flatten()
                        self.p_intermed_att_decision[c, :] = p_intermed_att_decision.flatten()
                    else:
                        self.stability_time[c, :] = stability_time
        else:
            if parallel:
                pool = getPool()
                results = pool.map(self.runVaryEICall, self.weight_range)
                pool.close()
                for c in range(self.weight_range.shape[0]):
                    self.p_A[c] = results[c][0]
                    self.decision_time[c, :] = results[c][3]
                    if self.P['n_areas'] > 1:
                        self.winning_pop[c, :] = results[c][1]
                        self.winners[c, :] = results[c][2]
                        self.stability_time[c, :, :] = results[c][4]
                        self.intermed_time_gain[c] = results[c][5]
                        self.intermed_agree[c] = results[c][6]
                        self.dt_intermed_readout[c] = results[c][8]
                        self.p_intermed_decision[c] = results[c][9]
                        self.intermed_pA[c] = results[c][10]
                        self.intermed_att_time_gain[c] = results[c][11]
                        self.intermed_att_agree[c] = results[c][12]
                        self.intermed_att_pA[c] = results[c][13]
                        self.p_intermed_att_decision[c] = results[c][14]
                    else:
                        self.stability_time[c, :] = results[c][4]
                    self.p_failed[c] = results[c][7]
            else:
                for c in range(self.weight_range.shape[0]):
                    self.p_A[c], self.winning_pop[c, :], self.winners[c, :], self.decision_time[c, :], \
                    stability_time, self.intermed_time_gain[c], self.intermed_agree[c], self.p_failed[c], \
                    self.dt_intermed_readout[c], self.p_intermed_decision[c], self.intermed_pA[c], \
                    self.intermed_att_time_gain[c], self.intermed_att_agree[c], self.intermed_att_pA[c], \
                    self.p_intermed_att_decision[c] = \
                        self.runVaryEICall(self.weight_range[c, :])
                    if self.P['n_areas'] > 1:
                        self.stability_time[c, :, :] = stability_time
                    else:
                        self.stability_time[c, :] = stability_time
        print('Finished collecting results')

    def runVaryEICall(self, input):
        """
        Helper method that allows user to vary the E/I weights
        :param input: Vector containing the E/I weights for intermediate layer area
        :return: p_A, winning_pop, winners, decision_time, stability_time, intermed_time_gain, intermed_agree, \
               p_failed, dt_intermed_readout, p_intermed_decision, intermed_pA, intermed_att_time_gain, \
               intermed_att_agree, intermed_att_pA, p_intermed_att_decision
        """
        Par = self.changeWeights(input)
        # Run the trials and collect the results
        trials = decisionTrials(Par)
        trials.runTrials()
        if Par['n_areas'] == 1:  # Single area
            decision_time = trials.thresh_cross_time
            p_A = (trials.winners[0, :] > 0).sum() / (Par['n_trials'] - sum(np.isnan(decision_time.flatten())))
            winning_pop, winners, intermed_time_gain, intermed_agree, dt_intermed_readout, p_intermed_decision, \
            intermed_pA, intermed_att_time_gain, intermed_att_agree, intermed_att_pA, p_intermed_att_decision = \
                None, None, None, None, None, None, None, None, None, None, None
        else:
            p_A, winning_pop, winners, decision_time = findWinning(trials)
            intermed_time_gain, intermed_agree, dt_intermed_readout, p_intermed_decision, intermed_pA = \
                calcIntermedLayerGains(trials, winners=winners)
            intermed_att_time_gain, intermed_att_agree, intermed_att_pA, p_intermed_att_decision = \
                calcIntermediateAttStats(trials, winners=winners)
        stability_time, p_failed = trials.stability_time, np.mean(np.isnan(trials.winners[-1, :]))
        del (trials)
        return p_A, winning_pop, winners, decision_time, stability_time, intermed_time_gain, intermed_agree, \
               p_failed, dt_intermed_readout, p_intermed_decision, intermed_pA, intermed_att_time_gain, \
               intermed_att_agree, intermed_att_pA, p_intermed_att_decision

    def runVaryEICallNoise(self, input):
        """
        Helper method that varies weights, along with noise.
        :param input: Vector containing the E/I weights for intermediate layer area
        :return: p_A, winning_pop, winners, decision_time, slope, stability_time, intermed_time_gain, \
               intermed_agree, p_failed, dt_intermed_readout, p_intermed_decision, intermed_pA, intermed_att_time_gain, \
               intermed_att_agree, intermed_att_pA, p_intermed_att_decision
        """
        Par = self.changeWeights(input)
        # Run the trials and collect the results
        vary_noise = varyNoiseSigma(Par)
        vary_noise.runVaryNoise(parallel=False)
        try:
            mask = np.isnan(vary_noise.p_A) < 1
            poly_w = np.polyfit(self.P['noise_sigma_range'][mask], vary_noise.p_A[mask], 1)
            slope = poly_w[0]
        except:
            slope = np.nan
        p_A, winning_pop, winners, decision_time, stability_time, intermed_time_gain, intermed_agree, p_failed, \
        dt_intermed_readout, p_intermed_decision, intermed_pA, intermed_att_time_gain, intermed_att_agree, \
        intermed_att_pA, p_intermed_att_decision = \
            vary_noise.p_A, vary_noise.winning_pop, vary_noise.winners, vary_noise.decision_time, \
            vary_noise.stability_time, vary_noise.intermed_time_gain, vary_noise.intermed_agree, vary_noise.p_failed, \
            vary_noise.dt_intermed_readout, vary_noise.p_intermed_decision, vary_noise.intermed_pA, \
            vary_noise.intermed_att_time_gain, vary_noise.intermed_att_agree, vary_noise.intermed_att_pA, \
            vary_noise.p_intermed_att_decision
        del vary_noise
        return p_A, winning_pop, winners, decision_time, slope, stability_time, intermed_time_gain, \
               intermed_agree, p_failed, dt_intermed_readout, p_intermed_decision, intermed_pA, intermed_att_time_gain, \
               intermed_att_agree, intermed_att_pA, p_intermed_att_decision

    def changeWeights(self, input):
        """
        Helper method that updates the weights for various simulation calls
        :param input: 2x1 vector of excitatory and inhibitory weights
        :return: P. Updated parameters dictionary
        """
        Par = copy.deepcopy(self.P)
        print('E = ' + str(input[0]) + ', I = ' + str(input[1]))
        # Change the weights
        Par['w'][0, 1], Par['w'][1, 0] = input[1] * brian2.nA, input[1] * brian2.nA  # Change inhibitory weights
        if Par['n_areas'] == 1:
            Par['w'][np.diag_indices(2)] = input[0] * brian2.nA  # Change excitatory weights
        else:
            Par['w'][np.diag_indices(4)] = input[0] * brian2.nA  # Change excitatory weights
            Par['w'][2, 3], Par['w'][3, 2] = input[1] * brian2.nA, input[1] * brian2.nA  # Change inhibitory weights
        return Par

    def saveVaryEIData(self, file_name='vary_EI_data.pickle'):
        """
        Save method that stores data from simulation results in a pickle file.
        :param file_name: Path and name of the file (default='vary_EI_data.pickle')
        :return: 0 if successful, 1 if not.
        """
        try:
            save_dict = {'P': self.P,
                         'p_A': self.p_A,
                         'p_failed': self.p_failed,
                         'weight_range': self.weight_range,
                         'decision_time': self.decision_time,
                         'stability_time': self.stability_time,
                         'vary_noise_sigma': self.vary_noise_sigma}
            if self.P['n_areas'] > 1:
                save_dict['winning_pop'] = self.winning_pop
                save_dict['winners'] = self.winners
                save_dict['intermed_time_gain'] = self.intermed_time_gain
                save_dict['intermed_agree'] = self.intermed_agree
                save_dict['dt_intermed_readout'] = self.dt_intermed_readout
                save_dict['p_intermed_decision'] = self.p_intermed_decision
                save_dict['intermed_pA'] = self.intermed_pA
            if self.vary_noise_sigma:
                save_dict['p_A_slope'] = self.p_A_slope

            saveData(file_name=file_name, save_dict=save_dict)
        except:
            return 1
        return 0

    def loadVaryEIData(self, file_name='vary_EI_data.pickle'):
        """
        Loads results from prior simulation into the object
        :param file_name: Path and name of data file (default='vary_EI_data.pickle')
        :return: 0 if successful, 1 if not
        """
        try:
            save_dict = loadData(file_name=file_name)
            # Update variables with saved values
            self.P = save_dict['P']
            self.p_A = save_dict['p_A']
            self.decision_time = save_dict['decision_time']
            self.weight_range = save_dict['weight_range']
            if self.P['n_areas'] > 1:
                self.winning_pop = save_dict['winning_pop']
                self.winners = save_dict['winners']
            # Variables that were added later
            if 'vary_noise_sigma' in save_dict:
                self.vary_noise_sigma = save_dict['vary_noise_sigma']
            if 'stability_time' in save_dict:
                self.stability_time = save_dict['stability_time']
            if 'intermed_time_gain' in save_dict:
                self.intermed_time_gain = save_dict['intermed_time_gain']
                self.intermed_agree = save_dict['intermed_agree']
                self.p_failed = save_dict['p_failed']
            if 'p_intermed_decision' in save_dict:
                self.p_intermed_decision = save_dict['p_intermed_decision']
                self.dt_intermed_readout = save_dict['dt_intermed_readout']
                self.intermed_pA = save_dict['intermed_pA']
            if self.vary_noise_sigma:
                self.p_A_slope = save_dict['p_A_slope']
            # Delete the save_dict
            del save_dict
        except:
            return 1
        return 0


class gridVarBund():
    """
    Class that specifically simulates bundles used to create an indifference curve. It is a wrapper for the
    decisionTrial() class. **IMPORTANT*** The default reference bundle values are created using P['mu_0'][1], and the
    values used in the variable bundle are specified with P['input_range']. Also, the class can either vary attributes or
    alternatives. If P['grid_vary'] = 'alt', one alternative has the reference values, and the attributes of the other
    alternative are varied. If P['grid_vary'] = 'att', one attribute for both alternatives is fixed, while the other
    attribute for both alternatives is varied.
    """
    def __init__(self, P=None, ref_bund=None):
        """
        Initiation method
        :param P: Param dictionary produced by hnParameters() or lnParameters()
        :param ref_bund: Reference bundle for grid search. If None, it uses P['mu_0'][1] (default=None).
        """
        if P is None:
            print("No parameters given.")
            P = hnParameters()
        self.P = P
        if ref_bund is None:
            self.ref_bund = np.array([self.P['mu_0'][1], self.P['mu_0'][1]])
        else:
            self.ref_bund = ref_bund
        self.var_bund = np.array(list(product(self.P['input_range'], repeat=2)))
        self.p_A = np.zeros((self.var_bund.shape[0], 1))
        self.decision_time = np.zeros((self.var_bund.shape[0], P['n_trials']))
        self.final_I = np.full([self.var_bund.shape[0], self.P['n_areas'] * 2, 4],
                               np.nan)  # allocate 4 spaces just in case
        self.final_S = copy.deepcopy(self.final_I)
        self.poly_fit = np.nan
        self.weight_range = np.nan
        if P['n_areas'] > 1:
            self.winning_pop = self.decision_time.copy()
            self.winners = self.decision_time.copy()
            self.intermed_time_gain = self.p_A.copy()
            self.intermed_agree = self.p_A.copy()
            self.stability_time = np.zeros((self.var_bund.shape[0], self.P['n_areas'] - 1, P['n_trials']))
        else:
            self.stability_time = np.zeros((self.var_bund.shape[0], P['n_trials']))

    def runGrid(self, parallel=True):
        """
        Main method called by user to run simulations and store results within the class.
        :param parallel: True/False. Parallel processing
        :return:
        """
        if parallel:
            print('Running decisions-making grid simulations in parallel')
            pool = getPool()
            results = pool.map(self.runGridCall, self.var_bund)
            pool.close()
            print('Finished collecting results')
            for c in range(self.var_bund.shape[0]):
                self.p_A[c] = results[c][0]
                self.decision_time[c, :] = results[c][3]
                self.final_I[c, :, :] = results[c][5]
                self.final_S[c, :, :] = results[c][6]
                if self.P['n_areas'] > 1:
                    self.winning_pop[c, :] = results[c][1]
                    self.winners[c, :] = results[c][2]
                    self.stability_time[c, :, :] = results[c][4]
                    self.intermed_time_gain[c] = results[c][7]
                    self.intermed_agree[c] = results[c][8]
                else:
                    self.stability_time[c, :] = results[c][4]
        else:
            for c in range(self.var_bund.shape[0]):
                self.p_A[c], winning_pop, winners, self.decision_time[c, :], stability_time, self.final_I[c, :, :], \
                self.final_S[c, :, :], intermed_time_gain, intermed_agree = self.runGridCall(self.var_bund[c, :])
                if np.isnan(self.final_S[c, :, 0]).flatten().sum() > 0:
                    print('get it!')
            if self.P['n_areas'] > 1:
                self.winning_pop[c, :] = winning_pop
                self.winners[c, :] = winners
                self.stability_time[c, :, :] = stability_time
                self.intermed_time_gain[c] = intermed_time_gain
                self.intermed_agree[c] = intermed_agree
            else:
                self.stability_time[c, :] = stability_time
        self.poly_fit = self.calcConvexity(vals=self.p_A)

    def runGridCall(self, input):
        """
        Main call function in the parallelization loop
        :param input: 2x1 vector of inputs.
        :return: p_A, winning_pop, winners, decision_time, stability_time, I_rec, S_rec, intermed_time_gain,
                intermed_agree
        """
        Par = copy.deepcopy(self.P)
        print('Testing input levels ' + str(input))
        # Create and modify mu array, depending on which grid strategy
        mu = np.zeros((6, 1))
        if self.P['grid_vary'] == 'alt':
            mu[[0, 2], 0], mu[[1, 3], 0] = input, self.ref_bund
        elif self.P['grid_vary'] == 'att':
            mu[0:2, 0], mu[2:4, 0] = input, self.ref_bund
        else:
            raise ValueError("Invalid value for P['grid_vary']")
        Par['mu'] = mu * brian2.Hz
        # Run the trials and collect the results
        trials = decisionTrials(Par)
        trials.runTrials()
        if self.P['n_areas'] == 1:  # Single area
            decision_time = trials.thresh_cross_time
            p_A = (trials.winners[0, :] > 0).sum() / (Par['n_trials'] - sum(np.isnan(decision_time.flatten())))
            winning_pop, winners, intermed_time_gain, intermed_agree = None, None, None, None
        else:
            p_A, winning_pop, winners, decision_time = findWinning(trials)
            intermed_time_gain, intermed_agree, dt_intermed_readout, p_intermed_decision, intermed_pA = calcIntermedLayerGains(
                trials)
        I_rec, S_rec = self.getFinalModes(trials=trials)
        stability_time = trials.stability_time
        del trials
        return p_A, winning_pop, winners, decision_time, stability_time, I_rec, S_rec, intermed_time_gain, intermed_agree

    def getFinalModes(self, trials, height=0):
        if height == 0:
            height = self.P['n_trials'] / 10
        S_rec = np.full([trials.P['n_areas'] * 2, 4], np.nan)
        I_rec = copy.deepcopy(S_rec)
        for p in range(trials.P['n_areas'] * 2):
            modes_S = findModes(trials.decision_snapshot[p, 1, :], height=height)
            if type(modes_S) != float: S_rec[p, 0:(len(modes_S))] = modes_S
            modes_I = findModes(trials.decision_snapshot[p, 0, :], height=height)
            if type(modes_I) != float: I_rec[p, 0:(len(modes_I))] = modes_I
        return I_rec, S_rec

    def getXYgrid(self, input_range, vals):
        """
        Takes a vector of input values and creates a grid for plotting
        :param input_range: Range of input values used in the simulation
        :param vals: vector of output values
        :return: x, y, grid_vals
        """
        rng = np.arange(0, input_range.shape[0])
        coord = list(product(rng, repeat=2))
        # Create the matrix for plotting
        grid_vals = np.zeros((rng.shape[0], rng.shape[0]))
        # Place values in the plotting matrix
        for v in range(len(coord)):
            grid_vals[list(coord[v])[1], list(coord[v])[0]] = vals[v]
        grid_vals = np.flipud(grid_vals)
        grid_vals[grid_vals < 0.3] = 0
        grid_vals[grid_vals > 0.7] = 1
        edges = feature.canny(grid_vals)
        value = edges.flatten()
        y, x = np.indices(edges.shape).reshape(-1, len(value))
        y, x = y[value > 0], x[value > 0]
        return x, y, grid_vals

    def calcConvexity(self, vals=[], plt_bool=False, failed_random=True, method='ellipse'):
        """
        Calculates the convexity/concavity by rotating the points and either fitting a 2nd degree polynomial or an ellipse
        :param vals: Values to be used in fitting
        :param plt_bool: True/False. Whether to plot the results. Useful for debugging
        :param failed_random: True/False. Trials where the network fails to reach a decision are converted to random choice
        :param method: 'ellipse', 'poly'. The method for fitting (default='ellipse')
        :return: poly_w
        """
        if len(vals) == 0: vals = self.p_A
        if failed_random: vals[np.isnan(vals)] = 0.5
        # Get the coordinates
        x, y, grid_vals = self.getXYgrid(self.P['input_range'], vals)
        if method == 'ellipse':
            try:
                poly_w = self.fitEllipse(x, y)
            except:
                poly_w = np.array([np.nan])
        elif method == 'poly':
            x_rot, y_rot = rotate(x, y, -np.pi / 4)
            try:
                poly_w = np.polyfit(x_rot, y_rot, 2)
            except:
                poly_w = np.full([3, 1], np.nan)
        else:
            raise ValueError('Convexity fit method must be "ellipse" or "poly"')
        if plt_bool:  # Only coded for the polynomial fit
            fig, ax = plt.subplots()
            x_cor, y_cor = rotate(x, y, np.pi / 2)
            ax.scatter(x_cor + self.ref_bund[0] * 2, y_cor, label='Behavior')
            x_rng = np.arange(-self.ref_bund[0] * 2, self.ref_bund[0] * 2, 0.05)
            x_fit, y_fit = rotate(x_rng, poly_w[0] * x_rng ** 2 + poly_w[1] * x_rng + poly_w[2], 3 * np.pi / 4)
            ax.plot(x_fit + self.ref_bund[0] * 2, y_fit, label='Fit')
            ax.legend()
            ax.set_ylim([0, self.ref_bund[0] * 2])
            ax.set_xlim([0, self.ref_bund[0] * 2])
        return poly_w.reshape(poly_w.size, 1)

    def fitEllipse(self,x,y):
        """
        Fits an ellipse to the data.
        :param x: vector of x values
        :param y: vector of y values
        :return: popt
        """
        #Prepare data
        x_hat, y_hat = x - x.min(), y - y.min()
        x_hat, y_hat = x_hat / x_hat.max(), (-y_hat / y_hat.max() + 1)
        y_hat = y_hat / y_hat.max()
        popt, pcov = sc.optimize.curve_fit(ellipse, x_hat, y_hat)
        return popt

    def joinClusterFiles(self, dir_name='pickles/', file_name_base='grid_vary'):
        """
        When simulations are run on the cluster, this joins the disjointed files.
        :param dir_name: str. Directory where cluster fiels are stored (default='pickles/')
        :param file_name_base: Shared base of the cluster files (default='grid_vary')
        :return:
        """
        f_names = glob(dir_name + file_name_base + '*')
        grid_var = gridVarBund(self.P)
        excit_range = np.zeros((len(f_names)))
        inhib_range = excit_range.copy()
        weight_range = np.zeros((len(f_names), 2))
        for f in range(len(f_names)):
            grid_var.loadGridData(f_names[f])
            if f == 0:
                self.P = grid_var.P
                self.ref_bund = grid_var.ref_bund
                self.var_bund = grid_var.var_bund
                self.p_A = grid_var.p_A
                self.decision_time = grid_var.decision_time
                self.final_I = np.expand_dims(grid_var.final_I, axis=3)
                self.final_S = np.expand_dims(grid_var.final_S, axis=3)
                self.poly_fit = grid_var.calcConvexity()
                if hasattr(grid_var, 'intermed_time_gain'):  # Variable added later
                    self.intermed_time_gain = grid_var.intermed_time_gain
                    self.intermed_agree = grid_var.intermed_agree
            else:
                self.poly_fit = np.concatenate((self.poly_fit, grid_var.calcConvexity()), axis=1)
                self.p_A = np.concatenate((self.p_A, grid_var.p_A), axis=1)
                self.decision_time = np.concatenate((self.decision_time, grid_var.decision_time), axis=1)
                self.final_I = np.concatenate((self.final_I, np.expand_dims(grid_var.final_I, axis=3)), axis=3)
                self.final_S = np.concatenate((self.final_S, np.expand_dims(grid_var.final_S, axis=3)), axis=3)
                if hasattr(grid_var, 'intermed_time_gain'):
                    self.intermed_time_gain = np.concatenate((self.intermed_time_gain, grid_var.intermed_time_gain),
                                                             axis=1)
                    self.intermed_agree = np.concatenate((self.intermed_agree, grid_var.intermed_agree), axis=1)
            excit_range[f], inhib_range[f] = grid_var.P['w'][0, 0] / brian2.nA, grid_var.P['w'][0, 1] / brian2.nA
            weight_range[f, 0], weight_range[f, 1] = excit_range[f], inhib_range[f]
        self.P['excit_range'] = np.unique(excit_range)
        self.P['inhib_range'] = np.flip(np.unique(inhib_range))
        self.weight_range = np.array(list(product(self.P['excit_range'], self.P['inhib_range'], repeat=1)))
        ind, rng = np.zeros((len(weight_range))), np.arange(0, len(weight_range), 1)
        # Make sure the values match up with the weight range
        for w in range(len(weight_range)):
            I = abs(self.weight_range[w, :] - weight_range).sum(axis=1) < 0.001
            ind[w] = rng[I].astype(int)
        I = ind.astype(int)
        self.p_A, self.poly_fit, self.decision_time = self.p_A[:, I], self.poly_fit[:, I], self.decision_time[:, I]
        self.final_I, self.final_S = self.final_I[:, :, :, I], self.final_S[:, :, :, I]
        if hasattr(self, 'intermed_time_gain'):
            self.intermed_time_gain, self.intermed_agree = self.intermed_time_gain[:, I], self.intermed_agree[:, I]

    def saveGridData(self, file_name='grid_var_bund_data.pickle'):
        """
        Save the relevant simulation results as a pickle
        :param file_name: Path and filename
        :return:
        """
        save_dict = {'P': self.P,
                     'p_A': self.p_A,
                     'ref_bund': self.ref_bund,
                     'var_bund': self.var_bund,
                     'decision_time': self.decision_time,
                     'stability_time': self.stability_time,
                     'final_I': self.final_I,
                     'final_S': self.final_S,
                     'poly_fit': self.poly_fit}
        if self.P['n_areas'] > 1:
            save_dict['winning_pop'] = self.winning_pop
            save_dict['winners'] = self.winners
            save_dict['intermed_time_gain'] = self.intermed_time_gain
            save_dict['intermed_agree'] = self.intermed_agree
        if hasattr(self, 'weight_range'):
            save_dict['weight_range'] = self.weight_range
        saveData(file_name=file_name, save_dict=save_dict)

    def loadGridData(self, file_name='grid_var_bund_data.pickle'):
        """
        Load simulation results from a pickle
        :param file_name: Path and filename
        :return:
        """
        save_dict = loadData(file_name=file_name)
        # Update variables with saved values
        self.P = save_dict['P']
        self.p_A = save_dict['p_A']
        self.decision_time = save_dict['decision_time']
        self.ref_bund = save_dict['ref_bund']
        self.var_bund = save_dict['var_bund']
        # Variables added later
        if 'final_I' in save_dict:
            self.poly_fit = save_dict['poly_fit']
            self.final_S = save_dict['final_S']
            self.final_I = save_dict['final_I']
        if 'stability_time' in save_dict:
            self.stability_time = save_dict['stability_time']
        if self.P['n_areas'] > 1:
            self.winning_pop = save_dict['winning_pop']
            self.winners = save_dict['winners']
        if 'weight_range' in save_dict:
            self.weight_range = save_dict['weight_range']
        if 'intermed_time_gain' in save_dict:
            self.intermed_time_gain = save_dict['intermed_time_gain']
            self.intermed_agree = save_dict['intermed_agree']
        # Delete the save_dict
        del save_dict


if __name__ == '__main__':

    """
    Below are examples of code usage. If you need guidance on how to implement these on a cluster, reach out. 
    """

    ## RUN SINGLE SIMULATION AND SAVE RESULTS
    # P = hnParameters()
    # P = lnParameters()
    # trials = decisionTrials(P)
    # trials.runTrials()
    # trials.saveTrialPlots(fig_name='output_figures/single_trial_sim.eps')

    ## RUN A REWARD RATE PARAMETER SWEEP
    # f_name = 'reward_rate_test.pickle'
    # save_dir = 'simulation_results/'
    # bundle_vals = np.arange(11, 13, 1)
    # noise_sigma_range = np.array([0, 1.0,])
    # excit_range =  np.array([0.31])
    # inhib_range =  np.array([-0.07, -0.08])
    # # P = lnParameters(bundle_vals=bundle_vals,noise_sigma_range=noise_sigma_range)
    # P = hnParameters(bundle_vals=bundle_vals,excit_range=excit_range, inhib_range=inhib_range, noise_sigma_range=noise_sigma_range,n_trials=20)
    # P['truncate_trial'] = True #Shorten the simulations
    #
    # reward_rate = rewardRate(P=P)
    # reward_rate.runRewardRate(parallel=True)
    # reward_rate.saveRewardRate(file_name=save_dir + f_name)

    ## RUN MAX-LIKE DEVIATIONS
    # f_name = 'max-like_deviations_test.pickle'
    # save_dir = 'simulation_results/'
    # w = np.array([[0.32, -0.08], [-0.08, 0.32]])
    # vals = np.arange(12, 18, 2)
    # max_deviations = maxLikeDeviations(vals=vals,w=w)
    # max_deviations.runMaxDeviations()
    # max_deviations.saveMaxDeviations(file_name=(save_dir + f_name))

    # RUN GRID SEARCH CLASS USED FOR CONVEXITY
    save_dir = 'simulation_results/'
    f_name = 'grid_vary_test.pickle'
    E = 0.3
    I = -1 * 0.1
    mu_0 = np.array([20, 20, 0])
    P = hnParameters(w=np.array([[E, I], [I, E]]), mu_0=mu_0)
    P['input_range'] = np.arange(16, 26, 2)
    P['grid_vary'] = 'alt'  # att, alt
    grid = gridVarBund(P)
    grid.runGrid(parallel=True)
    grid.saveGridData(file_name=save_dir+f_name)