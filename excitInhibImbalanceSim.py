"""

This set of functions and classes are used to simulate E/I imbalance in Pettine, W. W., Louie, K.,
Murray, J. D, Wang, X. J., "Hierarchical Network Model Excitatory-Inhibitory Tone Shapes Alternative Strategies for
Different Degrees of Uncertainty in Multi-Attribute Decisions." PLoS Computational Biology, 2021

If you have questions, email warren.pettine@gmail.com.

"""

from dynamicalModel import *
import numpy as np
import scipy as sc
import matplotlib.pyplot as plt
import pickle
import copy
from smooth import *

def sigmoidFourParam(x, x0, k, a, c):
    '''Basic sigmoid function with two parameters'''
    y = a / (1 + np.exp(-k * (x - x0))) + c
    return y


class simParent():
    def __init__(self,f_dir='pickles/', failed_random=True, restrict_bund_df_thresh=np.nan, restrict_bund_mn_thresh=0,
                 reward_f_name='hd_reward_rate_vals_10_1_20_signal_noise_0_v_2_intrnl_noise_0.003.pickle',
                 grid_f_name='grid_vary_0_40_2_E_.3_.4_I_.0_.1_0.005_ellipseFit.pickle', diff_inds=[0, 3, 7],
                 fb_convex=True, I_thresh=-0.02, trials_per_block=100, n_trials_total=800, n_sessions=10,
                 noise_blocks=['none', 'high', 'medium', 'high', 'none', 'high', 'medium', 'high'], target_E=None,
                 mod_duration='trial',unique=True,no_equal=True,bounds=([-50, -50, -10, -10], [50, 50, 10, 10]),
                 fit_vals='reward_observed'):
        # Load data
        reward_rate = rewardRate()
        reward_rate.loadRewardRate(file_name=(f_dir + reward_f_name))
        # Check mod_duration validity
        if (mod_duration != 'block') * (mod_duration != 'trial') > 0:
            raise ValueError('mod_duration must be "block" or "trial"')
        # Record details of parameters and files used
        self.P = {
            'f_dir': f_dir,
            'reward_f_name': reward_f_name,
            'grid_f_name': grid_f_name,
            'diff_inds': diff_inds,
            'fb_convex': fb_convex,
            'I_thresh': I_thresh,
            'trials_per_block': trials_per_block,
            'n_trials_total': n_trials_total,
            'n_sessions': n_sessions,
            'noise_blocks': noise_blocks,
            'mod_duration': mod_duration,
            'noise_sigma_range': reward_rate.P['noise_sigma_range']
        }
        # Convert failed bundles to toss-ups
        if failed_random:
            reward_rate.p_A[np.isnan(reward_rate.p_A[:])] = .5
        # Define class variables
        self.noise_levels = reward_rate.P['noise_sigma_range']
        self.weight_range = reward_rate.weight_range
        grid = gridVarBund()
        grid.loadGridData(file_name=(f_dir + grid_f_name))
        _, self.ic_curvature = self.alignWeights(reward_rate, grid)
        #Take care of bundles
        self.bundles, self.p_A = copy.deepcopy(reward_rate.bundles), copy.deepcopy(reward_rate.p_A)
        self.cleanBundles(unique=unique,no_equal=no_equal)
        if np.isnan(restrict_bund_df_thresh)<1:
            self.restrictBundles(threshold=restrict_bund_df_thresh,mn=restrict_bund_mn_thresh)
        #Fit for weights
        self.p_correct = reward_rate.p_correct
        self.E_func, self.I_func, _ = self.fitEIfunctions(fit_vals=self.p_correct,bounds=bounds)
        self.reward_observed = self.calcRewardObserved(self.bundles,self.p_A)
        self.restrictExcitatoryRange(self.reward_observed, self.weight_range, target_E=target_E)
        # Define variables to be specified later
        self.reward = []
        self.reward_norm = []
        self.min_poss = []
        self.max_poss = []
        print('Parent Initialized')

    def calcRewardObserved(self, bundles, p_A, include_failed=True, paradigm='block'):
        """Converts choice behavior into reward observed per trial"""
        if self.P['mod_duration'] == 'block':
            reward_observed = np.zeros((p_A.shape[1], p_A.shape[2]))
        elif self.P['mod_duration'] == 'trial':
            reward_observed = p_A * 0
        for w in range(p_A.shape[1]):
            for n in range(p_A.shape[2]):
                A = np.sum(bundles[:, [0, 2]], axis=1)
                B = np.sum(bundles[:, [1, 3]], axis=1)
                p_B = 1 - p_A[:, w, n]
                comparison = np.concatenate((A.reshape(len(A), 1), B.reshape(len(B), 1)), axis=1)
                # Get total reward rate per trial
                if include_failed:
                    denom = len(A)
                else:
                    denom = sum(np.isnan(p_A[:, w, n]) < 1)
                if self.P['mod_duration'] == 'block':
                    reward_observed[w, n] = (np.nansum(A * p_A[:, w, n]) + np.nansum(B * p_B)) / denom
                elif self.P['mod_duration'] == 'trial':
                    reward_observed[:, w, n] = (A * p_A[:, w, n]) + (B * p_B)
        return reward_observed

    def linConvexWeights(self, weight_range, f_name='pickles/grid_vary_0_40_2_E_.3_.4_I_0_.1_ellipseFit.pickle'):
        """"Provide index for convex weight configurations"""
        if len(f_name) == 0:
            cnvx = np.array([[0.32, 0.0],
                             [0.34, 0.0],
                             [0.36, 0.0],
                             [0.36, -0.02],
                             [0.38, 0.0],
                             [0.38, -0.02],
                             [0.40, -0.02],
                             [0.40, -0.04]])
        else:
            grid = gridVarBund()
            grid.loadGridData(file_name=f_name)
            # I = grid.poly_fit[0, :] > (-0.005)
            I = grid.poly_fit[0, :] < (0.1)
            cnvx = grid.weight_range[I,:]
        I = np.zeros((len(weight_range)))
        for w in range(len(cnvx)):
            I_tmp = (abs(weight_range[:, 0] - cnvx[w, 0]) < 0.001) * \
                    (abs(weight_range[:, 1] - cnvx[w, 1]) < 0.001)
            I[I_tmp] = True
        return I>0

    def alignWeights(self,reward_rate,grid):
        if reward_rate.weight_range.shape[0] > grid.weight_range.shape[0]:
            raise ValueError('There are more reward rate weights than grid weights')
        curvature = np.zeros(reward_rate.weight_range.shape[0])
        weight_range = np.zeros((reward_rate.weight_range.shape[0],2))
        for w in range(reward_rate.weight_range.shape[0]):
            I = (abs(reward_rate.weight_range[w, 0] - grid.weight_range[:, 0]) < 0.001) * \
                (abs(reward_rate.weight_range[w, 1] - grid.weight_range[:, 1]) < 0.001)
            curvature[w], weight_range[w, :] = grid.poly_fit[0, I], grid.weight_range[I, :]
        return weight_range, curvature

    def cleanBundles(self,unique=True,no_equal=True):
        """Get rid of repeat bundles and decisions where things are equal"""
        if unique:
            # Clean for bundle repeats
            self.bundles, I = np.unique(self.bundles, axis=0, return_index=True)
            self.p_A = self.p_A[I, :, :]
        if no_equal:
            #Clean for those that are equal
            I = abs(np.sum(self.bundles[:, [0, 2]], axis=1) - np.sum(self.bundles[:, [1, 3]], axis=1)) > 0.01
            self.bundles, self.p_A = self.bundles[I,:], self.p_A[I, :, :]


    def restrictBundles(self,threshold=4,mn=0):
        """Cuts it down so only the hard bundles are observed"""
        A, B = np.sum(self.bundles[:, [0, 2]], axis=1), np.sum(self.bundles[:, [1, 3]], axis=1)
        df = abs(A - B)
        I_df = (df<=threshold) * (df>0)
        mn_sz = self.bundles >= mn
        I_mn = np.sum(mn_sz, axis=1) == 4
        I = I_df*I_mn
        self.bundles, self.p_A = self.bundles[I,:], self.p_A[I,:,:]

    def restrictExcitatoryRange(self,reward_observed, weight_range, target_E=None):
        if target_E != None:
            inds_E = abs(weight_range[:, 0] - target_E) < 0.0001
            if self.P['mod_duration'] == 'block':
                self.reward_observed, self.weight_range = reward_observed[inds_E, :], weight_range[inds_E, :]
            elif self.P['mod_duration'] == 'trial':
                self.reward_observed, self.weight_range = reward_observed[:, inds_E, :], weight_range[:, inds_E, :]

    def fitEIfunctions(self,fit_vals,weight_range=None,bounds=([-50, -50, -10, -10], [50, 50, 10, 10])):
        # Fit the sigmoids
        if weight_range is None:
            weight_range = self.weight_range
        indx = np.argmax((fit_vals), axis=0)
        excit, inhib = weight_range[indx, 0], weight_range[indx, 1]
        noise_sig = self.P['noise_sigma_range']
        popt_E, pcov_E = sc.optimize.curve_fit(sigmoidFourParam, noise_sig, excit, bounds=bounds)
        popt_I, pcov_I = sc.optimize.curve_fit(sigmoidFourParam, noise_sig, inhib, bounds=bounds)
        return popt_E, popt_I, indx

    def calcOptimalEI(self,uncertainty,weight_range=None):
        """use fit parameters to identify the index of the optimal E/I tone"""
        if weight_range is None: weight_range = self.weight_range
        excit = sigmoidFourParam(uncertainty, self.E_func[0], self.E_func[1], self.E_func[2], self.E_func[3])
        inhib = sigmoidFourParam(uncertainty, self.I_func[0], self.I_func[1], self.I_func[2], self.I_func[3])
        I = np.argmin(abs(weight_range[:, 0] - excit) + abs(weight_range[:, 1] - inhib))
        return I

    def plotOptimalEI(self,ax1=None,ax2=None,fit_vals='p_correct',inhib_thresh=-0.1,line_width=3,colors=['m','c'],
                      save_dir='output_figures/', fig_name_base='weight_fit',fmt='png',font_size=12, save_bool=True,linestyle='-',
                      bounds=([-50, -50, -10, -10], [50, 50, 10, 10]),y_lim_inhib=None,lbl='Neurotypical'):
        #Get fit
        indx_inhib = np.abs(self.weight_range[:, 1]) <= np.abs(inhib_thresh)
        if fit_vals == 'reward_observed':
            fit_vals = self.reward_observed[indx_inhib,:]
        elif fit_vals == 'p_correct':
            fit_vals = self.p_correct[indx_inhib, :]
        popt_E, popt_I, indx_weights = self.fitEIfunctions(fit_vals=fit_vals,
                                                           weight_range=self.weight_range[indx_inhib,:],bounds=bounds)
        noise_sig = self.P['noise_sigma_range']
        x = np.arange(np.min(noise_sig), np.max(noise_sig), 0.0001)
        y_E = sigmoidFourParam(x, popt_E[0], popt_E[1], popt_E[2], popt_E[3])
        y_I = sigmoidFourParam(x, popt_I[0], popt_I[1], popt_I[2], popt_I[3])
        # Plot the optimal weights
        if (ax1 is None) or (ax2 is None):
            fig, ax1 = plt.subplots()
            ax2 = ax1.twinx()
        ax1.plot(x, y_E, color=colors[0], linewidth=line_width, label=(lbl + ' ' + 'Excit Weight'),linestyle=linestyle)
        ax1.set_xlabel('$\sigma_{\eta_I}$',fontsize=font_size+2)
        ax1.set_ylabel('Excitatory Weight', color=colors[0],fontsize=font_size+2)
        ax1.set_ylim([self.weight_range[indx_inhib, 0].min(), self.weight_range[indx_inhib, 0].max()])
        ax1.tick_params(axis='y', labelcolor=colors[0],labelsize=font_size)
        ax1.spines['top'].set_visible(False)
        ax1.set_title('Optimal Weights',fontsize=font_size+4)
        ax2.plot(x, y_I, color=colors[1], linewidth=3, label=(lbl + ' ' + 'Inhib Weight'),linestyle=linestyle)
        ax2.set_ylabel('Inhibitory Weight', color=colors[1],fontsize=font_size+2)
        if y_lim_inhib is None:
            y_lim_inhib = [min(self.weight_range[indx_inhib, 1].min(), y_I.min()),
                           max(self.weight_range[indx_inhib, 1].max(), y_I.max())]
        ax2.set_ylim([y_lim_inhib[1], y_lim_inhib[0] - 0.005])
        ax2.set_xlim([noise_sig.min(), noise_sig.max()])
        ax2.tick_params(axis='y', labelcolor=colors[1],labelsize=font_size)
        ax2.spines['top'].set_visible(False)
        plt.tight_layout()
        if save_bool:
            plt.savefig((save_dir + fig_name_base + '_weights' + '.' + fmt), format=fmt, dpi=300)

    def plotOptimalCurvature(self,ax=None,fit_vals='p_correct',inhib_thresh=-0.1,line_width=3,color='k',
                      save_dir='output_figures/', fig_name_base='Curvature_fit',fmt='png',font_size=12, save_bool=True,
                      bounds=([-50, -50, -10, -10], [50, 50, 10, 10]),linestyle='-',lbl='Neurotypical'):
        if ax is None:
            fig, ax = plt.subplots()
        # Get fit
        indx_inhib = np.abs(self.weight_range[:, 1]) <= np.abs(inhib_thresh)
        if fit_vals == 'reward_observed':
            indx_fit = np.argmax(self.reward_observed[indx_inhib, :], axis=0)
        elif fit_vals == 'p_correct':
            indx_fit = np.argmax(self.p_correct[indx_inhib, :], axis=0)
        else:
            raise ValueError("Invalid vals specified")
        excit, inhib = self.weight_range[indx_inhib, 0], self.weight_range[indx_inhib, 1]
        noise_sig, cnvxty = self.P['noise_sigma_range'], self.ic_curvature[indx_inhib][indx_fit]
        x = np.arange(np.min(noise_sig), np.max(noise_sig), 0.0001)
        popt_C, pcov_C = sc.optimize.curve_fit(sigmoidFourParam, noise_sig, cnvxty, bounds=bounds)
        y_C = sigmoidFourParam(x, popt_C[0], popt_C[1], popt_C[2], popt_C[3])
        # Plot the optimal convexity
        # plt.rcParams.update({'font.size': font_size})
        ax.scatter(noise_sig, cnvxty, c=color, linewidth=line_width)
        ax.plot(x, y_C, color=color, linewidth=line_width,label=lbl,linestyle=linestyle)
        ax.set_xlabel('$\sigma_{\eta_I}$',fontsize=font_size+2)
        ax.set_ylabel('Curvature',fontsize=font_size+2)
        ax.set_title('Reward-Maximizing Curvature',fontsize=font_size+4)
        ax.spines['right'].set_visible(False)
        ax.spines['top'].set_visible(False)
        ax.tick_params(labelsize=font_size)
        ax.legend()
        plt.tight_layout()
        if save_bool:
            plt.savefig((save_dir + fig_name_base + '_convexity' + '.' + fmt), format=fmt, dpi=300)


class eiSimAch(simParent):
    def __init__(self, f_dir='pickles/', failed_random=True, restrict_bund_df_thresh=np.nan, restrict_bund_mn_thresh=0,
                 reward_f_name='hd_reward_rate_vals_10_1_20_signal_noise_0_v_2_intrnl_noise_0.003.pickle',
                 grid_f_name='grid_vary_0_40_2_E_.3_.4_I_.0_.1_0.005_ellipseFit.pickle', diff_inds=[0, 3, 7],
                 fb_convex=True, I_thresh=-0.02, trials_per_block=100, n_trials_total=800, n_sessions=10,
                 noise_blocks=['none', 'high', 'medium', 'high', 'none', 'high', 'medium', 'high'], target_E=None,
                 mod_duration='trial',fit_vals='reward_observed'):
        simParent.__init__(self, f_dir=f_dir, failed_random=failed_random, target_E=target_E, grid_f_name=grid_f_name,
                           restrict_bund_df_thresh=restrict_bund_df_thresh, diff_inds=diff_inds, I_thresh=I_thresh,
                           restrict_bund_mn_thresh=restrict_bund_mn_thresh, reward_f_name=reward_f_name,fit_vals=fit_vals,
                           fb_convex=fb_convex, trials_per_block=trials_per_block, mod_duration=mod_duration,
                           n_trials_total=n_trials_total, n_sessions=n_sessions, noise_blocks=noise_blocks)
        self.I_healthy, self.I_local = self.getInds(I_thresh=I_thresh)

    def getInds(self,I_thresh):
        "obtain optimal indices for each bundle combination, for each noise level, for healthy and inhib restricted"
        I_healthy, I_local = np.argmax(self.reward_observed,axis=1), []
        for i in range(len(I_thresh)):
            reward_observed_inhib_restrict = copy.deepcopy(self.reward_observed)
            reward_observed_inhib_restrict[:, self.weight_range[:, 1] < I_thresh[i], :] = 0
            I_local.append(np.argmax(reward_observed_inhib_restrict,axis=1))
        return I_healthy, I_local

    def simulateSession(self,noise_block=[], trials_per_block=0, d_inds=[0,1,2]):
        """Simulates an experimental session"""
        if len(noise_block) > 0: self.P['noise_blocks'] = noise_block
        if trials_per_block > 0: self.P['trials_per_block'] = trials_per_block
        self.P['n_trials_total'] = self.P['trials_per_block'] * len(self.P['noise_blocks'])
        self.reward, self.max_poss, self.min_poss = np.zeros((len(self.I_local)+1 , self.P['n_trials_total'])), \
                                    np.zeros((self.P['n_trials_total'])), np.zeros((self.P['n_trials_total']))
        # Loop through trial blocks
        I_strt = 0
        for b in range(len(self.P['noise_blocks'])):
            print("*******" + self.P['noise_blocks'][b])
            I = np.random.choice(len(self.bundles), self.P['trials_per_block'])
            bunds = self.bundles[I, :]
            bundle_val = np.concatenate((np.sum(bunds[:, [0, 2]], axis=1).reshape(len(bunds), 1),
                                         np.sum(bunds[:, [1, 3]], axis=1).reshape(len(bunds), 1)), axis=1)
            if self.P['noise_blocks'][b] == 'none': d_ind = d_inds[0]
            if self.P['noise_blocks'][b] == 'medium': d_ind = d_inds[1]
            if self.P['noise_blocks'][b] == 'high': d_ind = d_inds[2]
            # Calculate rewards
            self.reward[:, I_strt:(I_strt + self.P['trials_per_block'])] = self.blockRewards(bunds,d_ind)
            # Maximum and Minimum Possible
            self.max_poss[I_strt:(I_strt + self.P['trials_per_block'])] = np.max(bundle_val, axis=1)
            self.min_poss[I_strt:(I_strt + self.P['trials_per_block'])] = np.min(bundle_val, axis=1)
            I_strt += self.P['trials_per_block']

        reward, max_poss = (self.reward - self.min_poss), (self.max_poss - self.min_poss)
        self.reward_norm = reward / max_poss
        print("Done Simulating!")

    def blockRewards(self,bundle_val,d_ind):
        reward = np.zeros((len(self.I_local)+1, bundle_val.shape[0]))
        weight_ind_record = reward*0
        for b in range(bundle_val.shape[0]):
            ind_bund = (abs(self.bundles[:,0]-bundle_val[b,0]) < 0.01) * (abs(self.bundles[:,1]-bundle_val[b,1]) < 0.01)\
                * (abs(self.bundles[:,2]-bundle_val[b,2]) < 0.01) * (abs(self.bundles[:,3]-bundle_val[b,3]) < 0.01)
            reward[0,b] = self.reward_observed[ind_bund,self.I_healthy[ind_bund,d_ind],d_ind]
            weight_ind_record[0,b] = self.I_healthy[ind_bund,d_ind]
            for i in range(len(self.I_local)):
                reward[i+1, b] = self.reward_observed[ind_bund, self.I_local[i][ind_bund, d_ind], d_ind]
                weight_ind_record[0, b] = self.I_local[i][ind_bund, d_ind]
        return reward


class eiSim(simParent):
    def __init__(self, f_dir='pickles/',failed_random=True,restrict_bund_df_thresh=np.nan,restrict_bund_mn_thresh=0,
                 reward_f_name='hd_reward_rate_vals_10_1_20_signal_noise_0_v_2_intrnl_noise_0.003.pickle',
                 grid_f_name='grid_vary_0_40_2_E_.3_.4_I_.0_.1_0.005_ellipseFit.pickle', diff_inds=[0,3,7],
                 fb_convex=True,I_thresh=-0.02,trials_per_block=100,n_trials_total=800, n_sessions=10,
                 noise_blocks=['none', 'high', 'medium', 'high', 'none', 'high', 'medium', 'high'], target_E=None,
                 mod_duration='block',bounds=([-50, -50, -10, -10], [50, 50, 10, 10]),use_fit=True,fit_vals='reward_observed'):
        simParent.__init__(self,f_dir=f_dir,failed_random=failed_random,restrict_bund_df_thresh=restrict_bund_df_thresh,
                restrict_bund_mn_thresh=restrict_bund_mn_thresh,reward_f_name=reward_f_name,grid_f_name=grid_f_name,
                diff_inds=diff_inds,fb_convex=fb_convex,I_thresh=I_thresh,trials_per_block=trials_per_block,
                n_trials_total=n_trials_total,n_sessions=n_sessions,noise_blocks=noise_blocks,mod_duration=mod_duration,
                target_E=target_E,bounds=bounds,fit_vals=fit_vals)
        self.I_healthy, self.I_fb, self.I_local, self.I_local_fb = self.getInds(self.reward_observed, \
                      self.weight_range,fb_convex=fb_convex,I_thresh=I_thresh,use_fit=use_fit)

    def fbInds(self,reward_observed, weight_range, convex=True):
        reward_observed_fb = copy.deepcopy(reward_observed)
        I_convex = self.linConvexWeights(weight_range, f_name=(self.P['f_dir'] + self.P['grid_f_name']))
        reward_observed_fb[I_convex < 1,:] = 0
        I_fb_rep = np.argmax(np.mean(reward_observed_fb, axis=1))
        return I_fb_rep, reward_observed_fb

    def getInds(self, reward_observed, weight_range,fb_convex=True,I_thresh=-0.02,use_fit=True):
        print('Obtaining Indices')
        I_healthy, I_fb, I_local, I_local_fb = [], [], [], []
        #Preprocess for the loop
        I_fb_rep, reward_observed_fb = self.fbInds(reward_observed, weight_range, convex=fb_convex)
        I_weight_local = weight_range[:, 1] < I_thresh
        reward_observed_local = copy.deepcopy(reward_observed)
        weight_range_local = self.weight_range.copy()
        weight_range_local[I_weight_local,:] = np.inf
        reward_observed_local[I_weight_local, :] = 0
        # reward_observed_fb[I_weight_local, :] = 0
        # I_local_fb_rep = np.argmax(np.mean(reward_observed_fb, axis=1))
        I_local_fb_rep, _ =  self.fbInds(reward_observed, weight_range, convex=True)
        for n in range(reward_observed.shape[1]):
            #Get indices
            if use_fit:
                I_healthy.append(self.calcOptimalEI(self.noise_levels[n]))
                I_local.append(self.calcOptimalEI(self.noise_levels[n],weight_range=weight_range_local))
            else:
                I_healthy.append(np.argmax(reward_observed[:, n]))
                I_local.append(np.argmax(reward_observed_local[:, n]))
            I_fb.append(I_fb_rep)
            I_local_fb.append(I_local_fb_rep)
        return I_healthy, I_fb, I_local, I_local_fb

    def simulateSession(self,noise_block=[], trials_per_block=0, d_inds=[0,1,2]):
        """Simulates an experimental session"""
        if len(noise_block) > 0: self.P['noise_blocks'] = noise_block
        if trials_per_block > 0: self.P['trials_per_block'] = trials_per_block
        self.P['n_trials_total'] = self.P['trials_per_block'] * len(self.P['noise_blocks'])
        self.reward, self.max_poss, self.min_poss = np.zeros((4, self.P['n_trials_total'])), \
                                np.zeros((self.P['n_trials_total'])), np.zeros((self.P['n_trials_total']))
        #Loop through trial blocks
        I_strt = 0
        for b in range(len(self.P['noise_blocks'])):
            print("*******" + self.P['noise_blocks'][b])
            I = np.random.choice(len(self.bundles), self.P['trials_per_block'])
            bunds = self.bundles[I, :]
            bundle_val = np.concatenate((np.sum(bunds[:, [0, 2]], axis=1).reshape(len(bunds), 1),
                                         np.sum(bunds[:, [1, 3]], axis=1).reshape(len(bunds), 1)), axis=1)
            if self.P['noise_blocks'][b] == 'none': d_ind = d_inds[0]
            if self.P['noise_blocks'][b] == 'medium': d_ind = d_inds[1]
            if self.P['noise_blocks'][b] == 'high': d_ind = d_inds[2]

            if b < 3: # Display the weights used
                print('Healthy')
                print(self.weight_range[self.I_healthy[self.P['diff_inds'][d_ind]], :])
                print('Local Restricted')
                print(self.weight_range[self.I_local[self.P['diff_inds'][d_ind]], :])
                print('FB Restricted')
                print(self.weight_range[self.I_fb[self.P['diff_inds'][d_ind]], :])
                print('Local FB Restricted')
                print(self.weight_range[self.I_local_fb[self.P['diff_inds'][d_ind]], :])

            # Healthy
            self.reward[0, I_strt:(I_strt +  self.P['trials_per_block'])] = \
                bundle_val[:, 0] * self.p_A[I, self.I_healthy[self.P['diff_inds'][d_ind]], self.P['diff_inds'][d_ind]] + \
                bundle_val[:, 1] * (1 - self.p_A[I, self.I_healthy[self.P['diff_inds'][d_ind]], self.P['diff_inds'][d_ind]])
            # Local Restricted
            self.reward[1, I_strt:(I_strt +  self.P['trials_per_block'])] = \
                bundle_val[:, 0] * self.p_A[I, self.I_local[self.P['diff_inds'][d_ind]], self.P['diff_inds'][d_ind]] + \
                bundle_val[:, 1] * (1 - self.p_A[I, self.I_local[self.P['diff_inds'][d_ind]], self.P['diff_inds'][d_ind]])
            # FB Restricted
            self.reward[2, I_strt:(I_strt +  self.P['trials_per_block'])] = \
                bundle_val[:, 0] * self.p_A[I, self.I_fb[self.P['diff_inds'][d_ind]], self.P['diff_inds'][d_ind]] + \
                bundle_val[:, 1] * (1 - self.p_A[I, self.I_fb[self.P['diff_inds'][d_ind]], self.P['diff_inds'][d_ind]])
            # Local FB Restricted
            self.reward[3, I_strt:(I_strt +  self.P['trials_per_block'])] = \
                bundle_val[:, 0] * self.p_A[I, self.I_local_fb[self.P['diff_inds'][d_ind]], self.P['diff_inds'][d_ind]] + \
                bundle_val[:, 1] * (1 - self.p_A[I, self.I_local_fb[self.P['diff_inds'][d_ind]], self.P['diff_inds'][d_ind]])
            # Maximum and Minimum Possible
            self.max_poss[I_strt:(I_strt +  self.P['trials_per_block'])] = np.max(bundle_val, axis=1)
            self.min_poss[I_strt:(I_strt +  self.P['trials_per_block'])] = np.min(bundle_val, axis=1)
            I_strt +=  self.P['trials_per_block']

        reward, max_poss = (self.reward - self.min_poss), (self.max_poss - self.min_poss)
        self.reward_norm = reward / max_poss
        print("Done Simulating!")

    def simulateAll(self,paradigm='all_bundles'):
        """Takes all trials at those difficulty levels"""
        if paradigm == 'all_bundles':
            self.simulateAllBundles()
        elif paradigm == 'sessions':
            self.simulateAllSessions()

    def simulateAllSessions(self):
        self.reward = np.zeros((4, self.P['n_sessions'], self.p_A.shape[2]))
        self.max_poss = np.zeros((self.P['n_sessions'], self.p_A.shape[2]))
        self.min_poss, self.reward_norm = copy.deepcopy(self.max_poss), copy.deepcopy(self.reward)
        for d_ind in range(self.p_A.shape[2]):
            # rwrd, mx_pss = np.zeros((4, self.P['n_sessions'])), np.zeros((self.P['n_sessions']))
            # mn_pss = mx_pss.copy()
            for s in range(self.P['n_sessions']):
                I = np.random.choice(len(self.bundles), self.P['n_trials_total'])
                bunds = self.bundles[I, :]
                bundle_val = np.concatenate((np.sum(bunds[:, [0, 2]], axis=1).reshape(len(bunds), 1),
                                             np.sum(bunds[:, [1, 3]], axis=1).reshape(len(bunds), 1)), axis=1)
                # Healthy
                self.reward[0, s, d_ind] = np.mean(bundle_val[:, 0] * self.p_A[I, self.I_healthy[d_ind], d_ind] + \
                    bundle_val[:, 1] * (1 - self.p_A[I, self.I_healthy[d_ind], d_ind]))
                # Local Restricted
                self.reward[1, s, d_ind] = np.mean(bundle_val[:, 0] * self.p_A[I, self.I_local[d_ind], d_ind] + \
                    bundle_val[:, 1] * (1 - self.p_A[I, self.I_local[d_ind], d_ind]))
                # FB Restricted
                self.reward[2, s, d_ind] = np.mean(bundle_val[:, 0] * self.p_A[I, self.I_fb[d_ind],
                    d_ind] + bundle_val[:, 1] * (1 - self.p_A[I, self.I_fb[d_ind], d_ind]))
                # Local FB Restricted
                self.reward[3, s, d_ind] = np.mean(bundle_val[:, 0] * self.p_A[I, self.I_local_fb[d_ind], d_ind] + \
                    bundle_val[:, 1] * (1 - self.p_A[I, self.I_local_fb[d_ind], d_ind]))
                # Min and max possible
                self.max_poss[s, d_ind], self.min_poss[s, d_ind] = \
                    np.max(bundle_val,axis=1).mean(), np.min(bundle_val,axis=1).mean()
        reward, max_poss = (self.reward - self.min_poss), (self.max_poss - self.min_poss)
        self.reward_norm = reward / max_poss

    def simulateAllBundles(self):
        """Simulates all bundles at each variability level"""
        bundle_val = np.concatenate((np.sum(self.bundles[:, [0, 2]], axis=1).reshape(len(self.bundles), 1),
                                     np.sum(self.bundles[:, [1, 3]], axis=1).reshape(len(self.bundles), 1)), axis=1)
        self.reward = np.zeros((4, len(bundle_val), self.p_A.shape[2]))
        self.max_poss = np.max(bundle_val, axis=1)
        self.min_poss = np.min(bundle_val, axis=1)
        self.reward_norm = copy.deepcopy(self.reward)
        # Loop through the difficulty levels
        for d_ind in range(self.p_A.shape[2]):
            rwrd = np.zeros((4, len(self.bundles)))
            rwrd[0, :] = \
                bundle_val[:, 0] * self.p_A[:, self.I_healthy[d_ind], d_ind] + \
                bundle_val[:, 1] * (1 - self.p_A[:, self.I_healthy[d_ind], d_ind])
            # Local Restricted
            rwrd[1, :] = \
                bundle_val[:, 0] * self.p_A[:, self.I_local[d_ind], d_ind] + \
                bundle_val[:, 1] * (1 - self.p_A[:, self.I_local[d_ind], d_ind])
            # FB Restricted
            rwrd[2, :] = \
                bundle_val[:, 0] * self.p_A[:, self.I_fb[d_ind], d_ind] + \
                bundle_val[:, 1] * (1 - self.p_A[:, self.I_fb[d_ind], d_ind])
            # Local FB Restricted
            rwrd[3, :] = \
                bundle_val[:, 0] * self.p_A[:, self.I_local_fb[d_ind], d_ind] + \
                bundle_val[:, 1] * (1 - self.p_A[:, self.I_local_fb[d_ind], d_ind])
            # Store raw reward values
            self.reward[:, :, d_ind] = rwrd
            # Store normalized reward
            rwrd_sub, mx_ps = (rwrd - self.min_poss), (self.max_poss - self.min_poss)
            self.reward_norm[:, :, d_ind] = rwrd_sub / mx_ps

    def plotSessionReward(self, fig_name='ei_block_simulations', fmt='png', models=['healthy','local','FB'],
                          fontsize=10,vals='reward_observed',colors=['saddlebrown','darkorange','slategrey','rebeccapurple']):
        if (len(self.reward) == 0) or self.reward.ndim == 3:
            self.simulateSession()
        fig, ax = plt.subplots()
        if vals=='reward_norm':
            vals, y_label, ttl_loc = self.reward_norm, 'P(Larger Chosen)', 1.03
        elif vals == 'reward_observed':
            vals, y_label, ttl_loc = self.reward, 'Reward', 36
        else:
            raise ValueError('vals must be reward_norm or reward_observed')
        # Plot constant trajectory
        x, lines = np.arange(1, self.reward_norm.shape[1] + 1, 1), ['-',':','-.','--']
        if 'healthy' in models:
            ax.plot(x, smooth(vals[0, :], window='flat'), ('b'+lines[0]), label='Healthy', color=colors[0])
        if 'local' in models:
            ax.plot(x, smooth(vals[1, :], window='flat'), ('r'+lines[1]), label='Restricted Local', color=colors[1])
        if 'FB' in models:
            ax.plot(x, smooth(vals[2, :], window='flat'), ('g'+lines[2]), label='Restricted FB', color=colors[2])
        if 'local_FB' in models:
            ax.plot(x, smooth(vals[3, :], window='flat'), ('r'+lines[3]), label='Convex & Restricted FB', color=colors[3])
        I_strt = 0
        for b in range(len(self.P['noise_blocks'])):
            ax.axvline(x=I_strt, color='k')
            plt.text(I_strt + round(self.P['trials_per_block'] / 4), ttl_loc, self.P['noise_blocks'][b])
            I_strt += self.P['trials_per_block']
        ax.set_xlabel('Trials',fontsize=fontsize)
        ax.set_ylabel(y_label,fontsize=fontsize)
        ax.set_xlim(0, x.max())
        ax.legend(fontsize=fontsize)
        plt.setp(ax.get_xticklabels(), rotation=45, fontsize=fontsize-2)
        plt.setp(ax.get_yticklabels(), fontsize=fontsize-2)
        plt.tight_layout()
        if len(fig_name) > 0:
            plt.savefig(fig_name + '.' + fmt, format=fmt, dpi=300)

    def plotOverallCurvature(self,fig_name='ei_block_curvature', fmt='png', models=['healthy','local','FB'],
                             ylim=[1, 4], fontsize=10, fig_dir='output_figures/',linewidth=2,norm_noise_x=False):
        if (len(self.reward) == 0) or self.reward.ndim == 2:
            self.simulateAll()
        # Index into the models to be plotted
        optimal_inds = np.vstack((self.I_healthy, self.I_local, self.I_fb, self.I_local_fb))
        model_inds, lines = [], ['-', '--', '-.', ':']
        if 'healthy' in models: model_inds.append(0)
        if 'local' in models: model_inds.append(1)
        if 'FB' in models: model_inds.append(2)
        if 'local_FB' in models: model_inds.append(3)
        x_labels = ['Healthy', 'Restricted Local Inhibition', 'Restricted FB', 'Convex & Restricted FB']
        if norm_noise_x:
            x, x_lab = self.noise_levels / self.bundles[:].max(), 'Normalized Noise Magnitude'
        else: x, x_lab = self.noise_levels, 'Noise Magnitude'
        fig, ax = plt.subplots()
        for m in range(len(models)):
            ax.plot(x, self.ic_curvature[optimal_inds[model_inds[m], :]], label=x_labels[model_inds[m]],
                    linestyle=lines[m],linewidth=linewidth,marker='o')
        pass
        ax.legend(fontsize=fontsize-2)
        plt.setp(ax.get_xticklabels(), rotation=45, fontsize=fontsize - 2)
        plt.setp(ax.get_yticklabels(), fontsize=fontsize - 2)
        ax.set_xlabel(x_lab, fontsize=fontsize)
        ax.set_ylabel('IC Curvature', fontsize=fontsize)
        ax.set_ylim(ylim)
        ax.set_title('Model Performance', fontsize=fontsize)
        plt.tight_layout()
        plt.savefig((fig_dir + fig_name + '.' + fmt), format=fmt, dpi=300)

    def plotOverallReward(self, typ='bar', fig_name='ei_block_simulations', fmt='png', models=['healthy','local','FB'],
                    ylim=[30.5,33],fontsize=10, fig_dir='output_figures/',norm_noise_x=False,vals='reward_observed',
                          colors=['saddlebrown','darkorange','slategrey','rebeccapurple']):
        if (len(self.reward) == 0) or self.reward.ndim == 2:
            self.simulateAll()
        #Determine the value to plots
        if vals=='reward_norm':
            vals, y_label = self.reward_norm, 'P(Larger Chosen)'
        elif vals == 'reward_observed':
            vals, y_label = self.reward, 'Reward Per Trial'
        else:
            raise ValueError('vals must be reward_norm or observed_reward')
        #Index into the models to be plotted
        model_inds, lines = [], ['-',':','-.','--']
        if 'healthy' in models: model_inds.append(0)
        if 'local' in models: model_inds.append(1)
        if 'FB' in models: model_inds.append(2)
        if 'local_FB' in models: model_inds.append(3)
        x_labels = ['Healthy','Restricted Local Inhibition','Restricted FB','Convex & Restricted FB']
        if typ == 'bar':
            noise_labels = ['None','Medium','High']
            # Loop through the noise blocks
            x_loc = np.arange(1,len(model_inds)+1,1)
            for d_ind in range(len(self.P['diff_inds'])):
                mn, std = np.mean(vals[model_inds, :,  self.P['diff_inds'][d_ind]], axis=1), \
                          np.std(vals[model_inds, :,  self.P['diff_inds'][d_ind]], axis=1)
                fig, ax = plt.subplots()
                ax.bar(x_loc, mn, color='b', yerr=std)
                ax.set_ylabel(y_label,fontsize=fontsize)
                ax.set_xticks(x_loc)
                ax.set_xticklabels([x_labels[i] for i in model_inds],fontsize=fontsize)
                ax.set_title(noise_labels[d_ind] + ' Trial to Trail Variability',fontsize=fontsize)
                ax.set_ylim([0,1.1])
                plt.setp(ax.get_xticklabels(), rotation=45, fontsize=fontsize-2)
                plt.setp(ax.get_yticklabels(), fontsize=fontsize-2)
                plt.tight_layout()
                plt.savefig((fig_name + '_' + noise_labels[d_ind] + '.' + fmt), format=fmt, dpi=300)
        else:
            mn, std = np.mean(vals[model_inds, :, :], axis=1), \
                      np.std(vals[model_inds, :, :], axis=1)
            if norm_noise_x:
                x, x_lab = self.noise_levels / self.bundles[:].max(), 'Normalized Noise Magnitude'
            else:
                x, x_lab = self.noise_levels, '$\sigma_{\eta_I}$'
            fig, ax = plt.subplots()
            for m in range(len(models)):
                ax.errorbar(x, mn[model_inds[m], :], yerr=std[model_inds[m], :],
                            label=x_labels[model_inds[m]], color=colors[m], fmt=(lines[m] + 'o'))
            ax.legend(fontsize=fontsize-2)
            plt.setp(ax.get_xticklabels(), rotation=45, fontsize=fontsize-2)
            plt.setp(ax.get_yticklabels(), fontsize=fontsize-2)
            ax.set_xlabel(x_lab,fontsize=fontsize)
            ax.set_ylabel(y_label,fontsize=fontsize)
            ax.spines['right'].set_visible(False)
            ax.spines['top'].set_visible(False)
            ax.set_ylim(ylim)
            ax.set_title('Model Performance',fontsize=fontsize)
            plt.tight_layout()
            plt.savefig((fig_dir + fig_name + '_line' + '.' + fmt), format=fmt, dpi=300)

    def overallStats(self, models=['healthy','local','FB'],vals='reward_observed'):
        """NOT COMPLETE"""
        # Determine the value to plots
        if vals == 'reward_norm':
            vals, y_label = self.reward_norm, 'Percent Max Possible'
        elif vals == 'reward_observed':
            vals, y_label = self.reward, 'Reward Per Trial'
        else:
            raise ValueError('vals must be reward_norm or observed_reward')
        if (len(self.reward) == 0) or self.reward.ndim == 2:
            self.simulateAll()
        model_inds, lines = [], ['-', ':', '-.', '--']
        if 'healthy' in models: model_inds.append(0)
        if 'local' in models: model_inds.append(1)
        if 'FB' in models: model_inds.append(2)
        if 'local_FB' in models: model_inds.append(3)
        fig, ax = plt.subplots()
        for d_ind in range(len(self.P['diff_inds'])):
            mn, std = np.mean(vals[model_inds, :, :], axis=1), \
                      np.std(vals[model_inds, :, :], axis=1)
            for m in range(len(models)):
                ax.plot(self.P)



if __name__ == '__main__':
    #Parameters
    grid_f_name = 'grid_vary_0_40_2_E_.3_.4_I_.0_.1_0.005_ellipseFit.pickle'
    # reward_f_name = 'hd_reward_rate_vals_0_2_20_all_signal_noise_0_v_2_intrnl_noise_0.003'
    # reward_f_name = 'hd_reward_rate_bund_meth_quad_10_0.5_20_signal_noise_0_v_2_intrnl_noise_0.002.pickle'
    reward_f_name = 'hd_reward_rate_bund-meth_full_10_2_20_hard_False_signal-noise_0.00-v-2.00_intrnl-noise_0.003_long-dur_True_truncate-trial_True.pickle'

    fmt='eps'
    fontsize = 15
    models = ['healthy', 'local']
    restrict_bund_df_thresh = np.nan #10
    restrict_bund_mn_thresh = np.nan #10
    I_thresh = -0.01
    ylim = [22, 25] # [0, 1.1]
    diff_inds = [0, 5, 9]

    #Create Simulation
    ei_sim = eiSim(fb_convex=False,n_trials_total=800,reward_f_name=reward_f_name,grid_f_name=grid_f_name,
                     diff_inds=diff_inds)

    # ei_sim.plotOptimalEI()
    ei_sim.plotOptimalCurvature()

    ei_sim.simulateSession()
    ei_sim.plotSessionReward(fig_name='',models=models,
                    fmt=fmt,fontsize=fontsize,fit_vals='reward_norm')

    ei_sim.simulateAll(paradigm = 'sessions')
    ei_sim.plotOverallReward(typ='line',fit_vals='reward_observed',ylim=ylim,models=models, fmt=fmt, fontsize=fontsize)

    ylim = [30, 31]
    ei_sim.plotOverallCurvature(models=models, ylim=[1, 2], fontsize=fontsize, fmt=fmt)

    # Create Simulation
    # I_thresh = [-0.01, -0.02]
    # diff_inds = [7, 8]
    # ei_sim = eiSimAch(fb_convex=False, n_trials_total=100, reward_f_name=reward_f_name, grid_f_name=grid_f_name,
    #                     I_thresh=I_thresh, diff_inds=diff_inds,restrict_bund_df_thresh=restrict_bund_df_thresh,
    #                     restrict_bund_mn_thresh=restrict_bund_mn_thresh)
    # ei_sim.simulateSession()
    # print(np.sum(ei_sim.reward, axis=1))

    print('Done!')